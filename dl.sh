#!/bin/bash
#style
#https://abs.traduc.org/abs-5.0-fr/ch32.html#unofficialst

#http://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html#The-Set-Builtin
#http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -u
#bash -n dl.sh
#set -eE  # same as: `set -o errexit -o errtrace`

declare -r SCRIPT_PATH=`realpath $0`;        # chemin  absolu complet du script rep + nom
declare -r SCRIPT_REP=`dirname $SCRIPT_PATH`;# repertoire absolu du script (pas de slash de fin)
declare -r VERSION="2022.04.28";

if [[ ! -f "$SCRIPT_REP/legite-lib.sh" ]]
then
    echo "$SCRIPT_REP/legite-lib.sh introuvable -> quit"
    exit 1
fi
. $SCRIPT_REP/legite-lib.sh


#############
# VARIABLES #
#   INIT    #
#############
    # --- variables des includes --- #
#


#############
# VARIABLES #
# DU PROJET #
#############

    ## COMMUNS ##
        declare -r COVER_ROOT='https://legral.fr/docufiles/covers';
        declare    collectionRoot='';
        #declare collectionRep="$CONF_REP";
        declare -a collectionsTbl;       # (array) liste des collections appellées
        declare -i collectionsTblIndex=0 # 
        #declare    collection='';        # collection en cours   # obsolete

        declare -r TITLE_LEVEL_DEFAULT='titre'
        declare -a titreLevelListe=( 'Tome' 'Album' 'Partie' 'Chapitre' 'Titre' 'Fichier' 'Musique' )
        declare    titreLevel='Titre';      # niveau a partir duquel le title sera setter
        declare    trackLevel='';           # niveau a partir duquel le track sera setter


        declare -i isListeFormats=0;
        declare -i formatNo=0;              # a renommer en ytFormatNo
        declare    formatExt='';            # a renommer en ytFormaExt
        declare    odFormat='hls-525';      # format des fichier odysee
        #declare -i isCollectionNeeded=1;   # par defaut il faut indiquer une collection
        declare -i isCodeYoutube=1;         # =1 si le parametre ne contient que le code youtube (sinon c'est l'url complete)
        declare -i isCodeOdysee=1;          # =1 si le parametre ne contient que le code odysee (sinon c'est l'url complete)
        declare -i isNoDownload=0;          # ne pas telecharger
        declare -i isSaveTags=0             # force la sauvegarde des tags même hors convertion
        declare    showTagsPath=''          # Affiche les tags du chemin (repertoire ou fichier) donné en parametre

        declare    convert='';              # format de convertion (ogg)
        declare    convertBak=''; # sauvegarde de la valeur de convert

        declare    ffmpeg_loglevel='error'  # quiet,panic,fatal,error,warning,info,verbose,debug
        declare    ffmpeg_loglevelTags=" -loglevel $ffmpeg_loglevel"  # quiet,panic,fatal,error,warning,info,verbose,debug

        declare -i isMono=0;                #
        declare    FFMpegMono='';           #  ffmpeg: " -ac 1" via setMono()
    #

    # - stats - #
        declare -A stats;
        declare -i stats['dl_total']=0; # compteur de telechargement total
        declare -i stats['dl_ok']=0;    # compteur de telechargement reussi
        declare -i stats['dl_exist']=0; # compteur de telechargement pre-existant
        declare -i stats['dl_fail']=0;  # compteur de telechargement echoué

        declare -i stats['cv_total']=0; # compteur de convertion total
        declare -i stats['cv_ok']=0;    # compteur de convertion réussis
        declare -i stats['cv_exist']=0; # compteur de convertion pre-existant
        declare -i stats['cv_fail']=0;  # compteur de convertion echoué


        declare -i stats['yt_dl_total']=0; # compteur de telechargement yt total
        declare -i stats['yt_dl_ok']=0;    # compteur de telechargement yt reussi
        declare -i stats['yt_dl_exist']=0; # compteur de telechargement yt pre-existant
        declare -i stats['yt_dl_fail']=0;  # compteur de telechargement yt echoué

        declare -i stats['dlyt_cv_total']=0; # compteur de convertion yt total
        declare -i stats['dlyt_cv_ok']=0;    # compteur de convertion yt réussis
        declare -i stats['dlyt_cv_exist']=0; # compteur de convertion yt pre-existant
        declare -i stats['dlyt_cv_fail']=0;  # compteur de convertion yt echoué


        declare -i stats['wg_dl_total']=0; # compteur de telechargement wget total
        declare -i stats['wg_dl_ok']=0;    # compteur de telechargement wget reussi
        declare -i stats['wg_dl_exist']=0; # compteur de telechargement wget pre-existant
        declare -i stats['wg_dl_fail']=0;  # compteur de telechargement wget echoué

        declare -i stats['dlwg_cv_total']=0; # compteur de convertion wget total
        declare -i stats['dlwg_cv_ok']=0;    # compteur de convertion wget réussis
        declare -i stats['dlwg_cv_exist']=0; # compteur de convertion wget pre-existant
        declare -i stats['dlwg_cv_fail']=0;  # compteur de convertion wget echoué


        declare -i stats['od_dl_total']=0; # compteur de telechargement odysee total
        declare -i stats['od_dl_ok']=0;    # compteur de telechargement odysee reussi
        declare -i stats['od_dl_exist']=0; # compteur de telechargement odysee pre-existant
        declare -i stats['od_dl_fail']=0;  # compteur de telechargement odysee echoué

        declare -i stats['dlod_cv_total']=0; # compteur de convertion odysee total
        declare -i stats['dlod_cv_ok']=0;    # compteur de convertion odysee réussis
        declare -i stats['dlod_cv_exist']=0; # compteur de convertion odysee pre-existant
        declare -i stats['dlod_cv_fail']=0;  # compteur de convertion odysee echoué
    #


    ## TAGS ##

        # - collection - #
        declare    tag_language='';
        declare    tag_artist='';  # auteurs
        declare -i tag_genreNo=0;
        declare    tag_genre='';

        # - commun - #
        declare    tag_album='';    # titre du cycle/livre/album
        declare    tag_title='';    # titre du chapitre/chanson/etc
        declare -i tag_track=0;
        declare -i tag_trackNb=0;
        declare    trackNoNbT=''; 
        declare    tlz='';
        declare    tracksTN='';     # -$glz$trackNo$trackNb

        declare -i tag_year=0;

        declare    tag_url='';

        declare    dl_prenote='';   # user
        declare    tag_note='';     # calculé par setTagNoteLivre() 
        declare    dl_postnote='';  #user

        declare    dl_date='';      #declare dl_dateTN='';
        declare    dl_voix='';      declare dl_voixNormalize='';    declare dl_voixTN='';
        declare    dl_source='';    declare dl_sourceNormalize='';  declare dl_sourceTN='';
        declare    dl_duree='';     declare dl_dureeT='';
        declare    dl_editeur='';
        declare    dl_traduction='';

        # - tags calculer (setTags) - #
        declare    vorbis_tags='';
        declare    id3v2_tags=''
        declare    ffmpeg_tags='-map 0:0';
        declare    tag_comment=''               # dl_prenote+tag_note+dl_postnote
        declare    vorbis_tag_comment=''
        declare    id3v2_tag_comment=''
        declare    tag_note_mp3=''
    #

    ## COVER ##

        # - la reference du cover dans le fichier - #
        declare -r COVER_TYPE_DEFAULT='Cover (front)';
        declare    coverTypeSelect="$COVER_TYPE_DEFAULT";           # le type en cours
        declare -a coverTypesListe=('Cover (front)' 'Cover (back)')
        declare -A coverTypesPaths; #contient les paths du fichier a intégrer
        coverTypesPaths['Cover (front)']='';
        coverTypesPaths['Cover (back)']='';

        # - la description du cover dans le fichier (delon le type)- #
        declare -A coverTypesDescs;                    # coverDescriptions["Cover (front)"]="Illustration de ..."
        coverTypesDescs['Cover (front)']='';
        coverTypesDescs['Cover (back)']='';

        # les levels de cover du + précis au + global
        declare -r COVER_LEVEL_DEFAULT='Cover (front)'
        declare    coverLevelSelect="$COVER_LEVEL_DEFAULT"
        declare -a coverLevelListe=('Cover (front)' 'yt_file' 'yt_file_WEBP' 'yt_file_JPG' 'yt_file_PNG' 'yt_page')
        declare -A coverLevelUrls;              # coverLevelUrls["Cover (front)"]="url"
        coverLevelUrls['Cover (front)']='';
        coverLevelUrls['yt_file']='';
        coverLevelUrls['yt_file_WEBP']='';      # format yt natif pour les minatures des videos
        coverLevelUrls['yt_file_JPG']='';       # dlCoverYT convertit yt_file_WEBP_path en yt_file_JPG_path
        coverLevelUrls['yt_file_PNG']='';
        coverLevelUrls['yt_page']='';
        
        declare -A coverLevelPaths;             # coverLevelPaths["Cover (front)"]="fileName"
        coverLevelPaths['Cover (front)']='';
        coverLevelPaths['yt_file']='';
        coverLevelPaths['yt_file_WEBP']='';
        coverLevelPaths['yt_file_JPG']='';
        coverLevelPaths['yt_file_PNG']='';
        coverLevelPaths['yt_page']='';

        declare -A coverLevelDescs;             # coverLevelDescs["Cover (front)"]="texte"
        coverLevelDescs['Cover (front)']='';
        coverLevelDescs['yt_file']='';
        coverLevelDescs['yt_file_WEBP']='';
        coverLevelDescs['yt_file_JPG']='';
        coverLevelDescs['yt_file_PNG']='';
        coverLevelDescs['yt_page']='';

        declare coverFrontPathTmpFile='';               # fichier temporaire pour ffmpeg pour cover Front
        declare coverFFmpegTags='';                     # chemin du cover Front (calculer part set)
        declare -i isDlCoverYT=1;                       # par defaut on telecharge le cover yt avec dlyt ()
    #

    ## DATAS ##
        #declare   titre='';                            # egal tag_title
        declare    titreNormalize=''                    # initialisé par create*
        declare    titreTN=''                           # Tiret Normalize ajoute un tiret devant titreNormalize

        #declare   auteur='';                           # egal tag_artist
        declare    auteurNormalize=''                   #
        declare    auteurTN=''                          #

        #declare   albumTitre=''                        # est tag_album
        declare    albumTitreNormalize=''
        declare    albumTitreTN=''

        declare -i fichierNb=0;                         # si>0 affiche no-nb dans le titreAuto
        declare -i fichierNo=0;                         # no/nb de fichier qui découpe un livre
        declare    fichierNoNbT=''; 
        declare    flz='';
        declare    fichierTitre='';
        declare    fichierTitreNormalize='';
        declare    fichierTitreTN='';

        ## GENERIQUE ##
        declare -i generiqueNb=0;
        declare -i generiqueNo=0;
        declare    generiqueNoNbT=''; 
        declare    glz='';
        #declare    generiqueTitre='';
        #declare    generiqueTitreNormalize='';
        #declare    generiqueTitreTN='';
        declare     generique_Glz_TitreNormalize='';
        declare     generique_date_Glz_TitreNormalize='';

        ## LIVRE ##
        declare -i tomeNb=0;                            # si>0 affiche Tno-nb dans le titreAuto
        declare -i tomeNo=0;
        declare    tomeNoNbT='';
        declare    tlz='';
        declare    tomeTitre='';
        declare    tomeTitreNormalize='';
        declare    tomeTitreTN='';

        declare -i partieNb=0;                          # si>0 affiche Pno-nb dans le titre Auto
        declare -i partieNo=0;
        declare    partieNoNbT='';
        declare    plz='';
        declare    partieTitre='';
        declare    partieTitreNormalize='';
        declare    partieTitreTN='';

        # local -a chapitreTitreTbl;                    # utilisé en local: contient les titres des chapitres
        declare -i chapitreNo=0;                        # createCollection#createLivre
        declare -i chapitreNb=0; 
        declare    chapitreNoNbT='';
        declare    clz='';                              # Chapitre Leadinbg Zero (text) "0" ''
        declare    chapitreTitre='';
        declare    chapitreTitreNormalize='';           # addChapitre
        declare    chapitreTitreTN='';                  # Prefixé et normalisé -$chapitreTitreNormalize"


        # Chaines calculé par ->setNameAuto setNameMusiqueAuto
        declare    livreTitreNormalize_Auto='';
        declare    livreTitreNormalizeTomeChapitre=''
        declare    musiqueTitreNormalize_Auto='';

        ## MUSIQUE ##
        declare -i musiqueNb=0;
        declare -i musiqueNo=0;
        declare    musiqueNoNbT='';
        declare    mlz='';
        declare    musiqueTitre='';
        declare    musiqueTitreNormalize='';
        declare    musique_Mlz_TitreNormalize='';
        declare    musiqueTitreTN='';
    #
#

# #################### #
# FONCTIONS GENERIQUES #
# #################### #
    function usageLocal(){
        if [[ $argNb -eq 0 ]]
        then
            titreUsage "$SCRIPT_FILENAME:; [options] collection - parametre_de_collection"
            echo 'options:'
            echo '--no-download: Ne pas telecharger'
            echo '--saveTags : force l écriture des tags meme si le fichier est deja telechargé'
            echo '--saveTags --convert=ogg ; pour forcer l écriture des tags sur un fichier convertit existant'
            echo '--showTags=dir|fichier. Afficher les tags du chemin (repertoire ou fichier) donné en parametre'
            echo '--mono: convertit le fichier en mono'
            exit $E_ARG_NONE;
        fi
    }


    function showVarsLocal(){
        fctIn
        displayVar "convert"            "$convert"
        displayVar 'isNoDownload'       "$isNoDownload" 'isSaveTags' "$isSaveTags"
        displayVar "isMono"            "$isMono"
        displayVar 'showTagsPath'       "$showTagsPath"
        displayVar 'collectionRoot'     "$collectionRoot"
        displayVar 'isListeFormats'     "$isListeFormats"
        displayVar 'formatExt'          "$formatExt" 'formatNo' "$formatNo"
        #displayVar 'isCollectionNeeded' "$isCollectionNeeded"
        displayVar 'isCodeYoutube'      "$isCodeYoutube"
        fctOut; return 0;
    }

    function showVarsPostLocal(){
        fctIn
        fctOut; return 0;
    }
#


################
# MISE A JOURS #
################
    # - telecharge et installe la derniere version en remplacant le script qui a lancer l'update - #
    function selfUpdateLocal (){
        if [[ $isUpdate -eq 1 ]]
        then
            fctIn
            echo "mise a jours du programme";

            cd "$SCRIPT_REP";
            updateFile "$UPDATE_HTTP/$SCRIPT_FILENAME" "./$SCRIPT_FILENAME"
            cd -        

            if [[ ! -d "$CONF_USR_ROOT" ]];then mkdir "$CONF_USR_ROOT";fi
            updateFile "$UPDATE_HTTP/$SCRIPT_FILENAME"   "/usr/local/bin/$SCRIPT_FILENAME"

            if [[ ! -d "$COLLECTIONS_REP" ]];then mkdir -p $COLLECTIONS_REP;fi

            updateFile "$UPDATE_HTTP/collections/Lovecraft.sh"    "$COLLECTIONS_REP/Lovecraft.sh"
            updateFile "$UPDATE_HTTP/collections/Monsieur_Phi.sh" "$COLLECTIONS_REP/Monsieur_Phi.sh"
            updateFile "$UPDATE_HTTP/collections/Verne.sh"        "$COLLECTIONS_REP/Verne.sh"
            fctOut; return 0;
        fi
    }
#


##########################
# GESTION DES LIBRAIRIES #
##########################
    function listeFormats(){
        fctIn
        if [[ $isListeFormats -eq 1 ]]
        then
            local url=${VIDEO_INFO_URL:-''}
            if [[ "$url" == '' ]]
            then
                erreursAddEcho "$FUNCNAME(): variable VIDEO_INFO_URL non définie."
            else
                evalCmd "youtube-dl --list-formats $VIDEO_INFO_URL"
                exit
            fi
        fi
        fctOut; return 0;
    }
#


# ################### #
# FONCTIONS DU PROJET #
# ################### #
    # isExtensionValide( 'element a tester' )
    function isExtensionValide(){
        fctIn "$*"
        if [[ $# -eq 0 ]];then erreursAddEcho "$FUNCNAME[$#/1]('extention') ($*)";fctOut; return $E_ARG_REQUIRE;fi

        local -a ExtentsonTbl=('ogg' 'mp3' 'mkv' 'png' 'jpg' 'jpeg' 'ogm');
        for element in "${ExtentsonTbl[@]}";
        do
            #echo "$element - $1"
            #https://linuxhint.com/bash_lowercase_uppercase_strings/ (bash 4)
            #${1^^} = UPERCASE $1
            #${1,,} = lower $1
            if [[ "$element" == "${1,,}" ]]; then return $E_FALSE; fi
        done
        fctOut; return 0;
    }
#


###########
# RAPPORT #
###########
    function rapportShow(){
        fctIn
        if [[ $librairiesCallIndex -eq 0 ]];then fctOut; return 0;fi
        displayVar "collections" "$CONF_REP"
        titre1 "RAPPORT    ok         existant       fail   total"
        echo -e "Telecharements ${stats['dl_ok']}\t/ ${stats['dl_exist']}\t\t/ ${stats['dl_fail']}\t/ ${stats['dl_total']}"$NORMAL
        #echo "dl_yt"
        echo -e "youtube        ${stats['yt_dl_ok']}\t/ ${stats['yt_dl_exist']}\t\t/ ${stats['yt_dl_fail']}\t/ ${stats['yt_dl_total']}"
        #echo -e "Convertions    ${stats['dlyt_cv_ok']}\t/ ${stats['dlyt_cv_exist']}\t\t/ ${stats['dlyt_cv_fail']}\t/ ${stats['dlyt_cv_total']}"
        #echo "dl_wget"
        echo -e "wget           ${stats['wg_dl_ok']}\t/ ${stats['wg_dl_exist']}\t\t/ ${stats['wg_dl_fail']}\t/ ${stats['wg_dl_total']}"
        #echo -e "Convertions    ${stats['dlwg_cv_ok']}\t/ ${stats['dlwg_cv_exist']}\t\t/ ${stats['dlwg_cv_fail']}\t/ ${stats['dlwg_cv_total']}"
        echo -e "Convertions    ${stats['cv_ok']}\t/ ${stats['cv_exist']}\t\t/ ${stats['cv_fail']}\t/ ${stats['cv_total']}"$NORMAL
        fctOut; return 0;
    }


    # Affiche les tags du repertoire|fichier donné ($1|showTagsPath)
    function showTagsShow(){
        fctIn "$*"

        local pathRoot=${1:-"$showTagsPath"}
        if [[ "$pathRoot" == ''  ]];then fctOut; return 0;fi # sortie normal. La fonction est optionnel
        if [[ ! -d  "$pathRoot"  ]];then erreursAddEcho "$FUNCNAME()  '$showTagsPath' n'est pas un repertoire'";fctOut; return $E_INODE_NOT_EXIST;fi

        titre2 "Rapport des tags($pathRoot)";

        function showTagsShowScan(){
            fctIn "$*"
            if [[ $isTrapCAsk -eq 1 ]]; then return $E_CTRL_C;fi
            if [[ $# -eq 0 ]];then return $E_ARG_REQUIRE;fi
            echo ''
            local path="$1"

            if [[ ! -r "$path" ]]
            then
                erreurAddEcho "$FUNCNAME($*): Le chemin \"$path\" n'est pas lisible."
                fctOut; return $E_INODE_NOT_EXIST;
            fi

            if [[ -d "$path" ]]
            then
                displayDebug "$LINENO: le path \"$path\" est un repertoire";
                for inode in $path/*
                do
                    echo ''
                    if [[ -d "$inode" ]]
                    then
                        displayDebug "$LINENO: L inode $inode est un repertoire";
                        showTagsShowScan "$inode"
                    elif [[ -f "$inode" ]]
                    then
                        displayDebug "$LINENO: $inode est un fichier";
                        showTagsShowFile "$inode"
                    fi
                done
            elif [[ -f "$path" ]]
            then
                displayDebug "$LINENO: $path est un fichier";
                showTagsShowFile "$path"
            fi
        }
        showTagsShowScan "$pathRoot"

        fctOut; return 0;
    }


    # Affiche les tags du repertoire|fichier donné ($1 ou showTagsPath)
    function showTagsShowFile(){
        fctIn "$*"

        if [[ ! -r "$showTagsPath" ]]
        then
            erreursAddEcho "$FUNCNAME($*): Le chemin $showTagsPath n'est pas lisible."
            fctOut; return $E_INODE_NOT_EXIST;
        fi
        local path=${1:-"$showTagsPath"}
        filename=${path##*/};  # nom du fichier uniquement (sans le chemin)

        local extension=${filename##*.}
        #displayVar "path"      "$path"
        #displayVar "filename"  "$filename"
        #displayVar "extension" "$extension"
        case "$extension" in 
            #format valide 
            #vorbiscomment: https://www.xiph.org/vorbis/doc/v-comment.html
            "ogg" | "ogm" | "flac" )
                evalCmd "vorbiscomment --list $path"
                ;;
            "mp3")
                evalCmd "id3v2 --list \"$path\""
                ;;
            #format non compatible
            * )
                erreursAddEcho "$FUNCNAME($*): Format '$extension' non compatible id3";
                ;;
        esac;
        fctOut; return 0;
    }


    #affiche les differentes variables calculés
    function showDatas(){
        fctIn
        titre4 'TOUTES LES DATAS';

        showDatasGenerique
        showDatasMusique
        showDatasLivre
        showCovers
        showTags
        showTagsProg

        fctOut; return 0;
    }

    function showDatasGenerique(){
        fctIn

        echo '## GENERIQUE ##'
        displayVar 'generiqueNo' "$generiqueNo" '' "$generiqueNb" 'glz' "$glz";
        displayVar 'tag_track  ' "$tag_track"   '' "$tag_trackNb" 'glz' "$glz" 'tracksTN' "$tracksTN"

        displayVar 'fichierNo  ' "$fichierNo" '' "$fichierNb" 'flz' "$flz";
        if [[ "$fichierTitre" != '' ]]
        then
            displayVar 'fichierTitre         ' "$fichierTitre"
            displayVar 'fichierTitreNormalize' "$fichierTitreNormalize";
            displayVar 'fichierTitreTN      '  "$fichierTitreTN";
        fi

        fctOut; return 0;
    }

    function showDatasLivre(){
        fctIn

        echo '## LIVRE ##'
        displayVar 'tomeNo     ' "$tomeNo" '' "$tomeNb" 'tlz' "$tlz";
        if [[ "$tomeTitre" != '' ]]
        then
            displayVar 'tomeTitre           ' "$tomeTitre"
            displayVar 'tomeTitreNormalize  ' "$tomeTitreNormalize";
            displayVar 'tomeTitreTN        '  "$tomeTitreTN";
        fi
        displayVar 'partieNo   ' "$partieNo" '' "$partieNb" 'plz' "$plz";
        if [[ "$partieTitre" != '' ]]
        then
            displayVar 'partieTitre         ' "$partieTitre"
            displayVar 'partieTitreNormalize'  "$partieTitreNormalize" 
            displayVar 'partieTitreTN      '  "$partieTitreTN" 
        fi

        displayVar 'chapitreNo ' "$chapitreNo" '' "$chapitreNb" 'clz' "$clz";
        # local -a chapitreTitreTbl;                    # utilisé en local: contient les titres des chapitres
        if [[ "$chapitreTitre" != '' ]]
        then
            displayVar 'chapitreTitre         ' "$chapitreTitre"
            displayVar 'chapitreTitreNormalize' "$chapitreTitreNormalize";
            displayVar 'chapitreTitreTN      '  "$chapitreTitreTN";
        fi

        displayVarNotNull 'generique_Glz_TitreNormalize       ' "$generique_Glz_TitreNormalize"
        displayVarNotNull 'generique_date_Glz_TitreNormalize  ' "$generique_date_Glz_TitreNormalize"

        if [[ "$titreNormalize" != '' ]]
        then
            displayVar 'livreTitreNormalize_Auto           ' "$livreTitreNormalize_Auto"
        fi
        #displayVar 'livreTitreNormalize_Album_Fichier ' "$livreTitreNormalize_Album_Fichier"
        #displayVar 'livreTitreNormalize_Album_Chapitre' "$livreTitreNormalize_Album_Chapitre"

        titre3 'MUSIQUE'
        displayVar 'musiqueNo  ' "$musiqueNo" '' "$musiqueNb" 'mlz' "$mlz";
        if [[ "$musiqueTitre" != '' ]]
        then
            displayVar 'musiqueTitre               ' "$musiqueTitre";
            displayVar 'musiqueTitreNormalize      ' "$musiqueTitreNormalize";
            displayVar 'musiqueTitreTN             ' "$musiqueTitreTN";
            displayVar 'musique_Mlz_TitreNormalize'  "$musique_Mlz_TitreNormalize";
        fi
        fctOut; return 0;
    }

    function showDatasMusique(){
        fctIn

        echo '## MUSIQUE ##'
        displayVar 'musiqueNo  ' "$musiqueNo" '' "$musiqueNb" 'mlz' "$mlz";
        if [[ "$musiqueTitre" != '' ]]
        then
            displayVar 'musiqueTitre               ' "$musiqueTitre";
            displayVar 'musiqueTitreNormalize      ' "$musiqueTitreNormalize";
            displayVar 'musiqueTitreTN             ' "$musiqueTitreTN";
            displayVar 'musique_Mlz_TitreNormalize'  "$musique_Mlz_TitreNormalize";
        fi
        fctOut; return 0;
    }
#


# #################### #
# parametres de ffmpeg #
# #################### #
    function setFfmpeg_loglevelTags(){
        fctIn "$*"
        if [[ $isTrapCAsk -eq 1 ]];then break;fi
        local levelNu=${1:-$verbosity}

        # quiet,panic,fatal,error,warning,info,verbose,debug
        case $levelNu in
        0)
            ffmpeg_loglevel='quiet';
            ffmpeg_loglevelTags=" -loglevel $ffmpeg_loglevel"
            ;;
        1)
            ffmpeg_loglevel='error';
            ffmpeg_loglevelTags=" -loglevel $ffmpeg_loglevel"
            ;;
        esac
        fctOut; return 0;
    }
#


# ################# #
# parametres de ytd #
# ################# #
    declare YTDL_PARAM_GLOBAL='';
    YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --ignore-config --restrict-filenames" #ignore les fichiers de conf
    YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --prefer-free-formats" 
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --merge-output-format" 
    YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --prefer-ffmpeg"
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} ---prefer-avconv"
 
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --recode-video" 
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --get-format" # 
    #Video Format Options:
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --list-formats" #liste les formats videos disponibles


    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --list-thumbnails" #liste les miniatures du lien
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --get-thumbnail"

    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --quiet"
    YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --console-title"
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --all-formats" #telecharge tous les formats possibles 

    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --simulate"
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --skip-download"

    declare PARAM_TITRE='';   # contient les parametres pour le titre en cours
    #PARAM_TITRE="${PARAM_TITRE} --format FORMAT SELECTION" 
    #PARAM_TITRE="${PARAM_TITRE} --format 43"  #webm medium


#


################
##   FORMAT   ##
################
    function setFormat(){
        fctIn "$*"
        if [[ $# -eq 0 ]]
        then
            echo $WARN"Pas de parametre! setFormat formatNo formatExt$NORMAL"
            fctOut; return $E_ARG_REQUIRE;
        fi

        if [[ $# -ge 1 ]]
        then
            formatNoOld=$formatNo;
            formatNo=$1;
        fi

        # - surcharge de l'extension du fichier si defini- #
        if [[ $# -ge 2 ]]
        then
            formatExtOld=$formatExt;
            formatExt=$2;
        fi
        fctOut; return 0;
    }

    #format code  extension  resolution note
    #249          webm       audio only DASH audio   51k , opus @ 50k (48000Hz), 4.29MiB
    #250          webm       audio only DASH audio   68k , opus @ 70k (48000Hz), 5.07MiB
    #171          webm       audio only DASH audio   98k , vorbis@128k (44100Hz), 7.45MiB
    #140          m4a        audio only DASH audio  128k , m4a_dash container, mp4a.40.2@128k (44100Hz), 10.96MiB
    #251          webm       audio only DASH audio  140k , opus @160k (48000Hz), 9.11MiB
    #278          webm       256x144    DASH video   79k , webm container, vp9, 13fps, video only, 4.59MiB
    #160          mp4        256x144    DASH video  123k , avc1.4d400c, 13fps, video only, 8.58MiB
    #242          webm       426x240    DASH video  131k , vp9, 25fps, video only, 7.08MiB
    #243          webm       640x360    DASH video  247k , vp9, 25fps, video only, 13.18MiB
    #134          mp4        640x360    DASH video  273k , avc1.4d401e, 25fps, video only, 15.21MiB
    #133          mp4        426x240    DASH video  315k , avc1.4d4015, 25fps, video only, 19.32MiB
    #244          webm       854x480    DASH video  404k , vp9, 25fps, video only, 21.08MiB
    #135          mp4        854x480    DASH video  568k , avc1.4d401e, 25fps, video only, 31.03MiB
    #247          webm       1280x720   DASH video  817k , vp9, 25fps, video only, 41.24MiB
    #136          mp4        1280x720   DASH video 1147k , avc1.4d401f, 25fps, video only, 59.52MiB
    #248          webm       1920x1080  DASH video 1866k , vp9, 25fps, video only, 88.88MiB
    #137          mp4        1920x1080  DASH video 2236k , avc1.640028, 25fps, video only, 111.80MiB
    #36           3gp        ?x320      small , mp4v.20.3,  mp4a.40.2
    #17           3gp        176x144    small , mp4v.20.3,  mp4a.40.2@ 24k
    #5            flv        400x240    small , h263, mp3  @ 64k
    #43           webm       640x360    medium , vp8.0,  vorbis@128k
    #18           mp4        640x360    medium , avc1.42001E,  mp4a.40.2@ 96k
    #22           mp4        1280x720   hd720 , avc1.64001F,  mp4a.40.2@192k (best)
    function setYoutubeFormat(){
        fctIn "$*"
        if [[ $# -eq 0 ]]
        then
            echo $WARN"$FUNCNAME:Nombre de parametre requis insuffisant ($#/1)!"$NORMAL
            fctOut; return $E_ARG_REQUIRE;
        fi

        case $1 in
            43 | 171 | 242 | 243 | 244 | 247 | 248 | 249 | 250 | 251 | 278 )
                saveFormatToOld
                formatNo=$1;
                formatExt="webm"
                ;;


            18 | 22 | 133 | 134 | 135 | 136 | 137 | 160 )
                saveFormatToOld
                formatNo=$1;
                formatExt="mp4"
                ;;

            140 )
                saveFormatToOld
                formatNo=$1;
                formatExt="m4a"
                ;;

            *)
                erreursAddEcho "$FUNCNAME($*): Ce Numéro de format est inconnu!"
                ;;
        esac
        displayVarDebug "formatNo" ":$formatNo" "formatExt" "$formatExt"
        fctOut;return 0;
    }

    function saveFormatToOld(){
        fctIn
        formatNoOld=$formatNo;
        formatExtOld=$formatExt;
        displayVarDebug 'formatNoOld' "$formatNoOld" 'formatExtOld' "$formatExtOld"
        fctOut; return 0;
    }

    function restaureFormat(){
        fctIn
        formatNo=$formatNoOld;
        formatExt=$formatExtOld;
        fctOut; return 0;
    }

    function setYTF_convert(){
        fctIn "$*"
        if [[ $# -lt 2 ]]
        then
            echo $WARN"$FUNCNAME:Nombre de parametre requis insuffisant ($#/2)!"$NORMAL
            fctOut; return $E_ARG_REQUIRE;
        fi
        setYoutubeFormat "$1"
        setConvert "$2"
        fctOut; return 0;
    }

    function restaureAll(){
        fctIn "$*"
        restaureFormat
        restaureConvert
        fctOut; return 0;
    }
#


################
##   LEVEL    ##
################
    function setLevels(){
        fctIn "$*"        # laisser echo information importante a preciser
        local _level=${1:-''}
        setTitreLevel "$_level"
        setTrackLevel "$_level"
        fctOut; return 0;
    }

    #setTitreLevel( 'level')
    # en cas d'erreur titreLevel="$TITLE_LEVEL_DEFAULT"
    function setTitreLevel(){
        fctIn "$*"
        local _levelAsk=${1:-"$TITLE_LEVEL_DEFAULT"};
        if [[ "$_levelAsk" == '' ]];then _levelAsk="$TITLE_LEVEL_DEFAULT";fi
        local _level="$TITLE_LEVEL_DEFAULT"
        #displayVar '_levelAsk' "$_levelAsk"
        for element in "${titreLevelListe[@]}";
        do
            #displayVar  '_levelAsk' "$_levelAsk" 'element' "$element"
            if [[ "$_levelAsk" == "$element" ]]; then _level="$element";break; fi
        done
        titreLevel="$_level";
        fctOut; return 0;
    }

    function setTrackLevel(){
        fctIn "$*"
        local _levelAsk=${1:-"$TITLE_LEVEL_DEFAULT"};
        if [[ "$_levelAsk" == '' ]];then _levelAsk="$TITLE_LEVEL_DEFAULT";fi
        local _level="$TITLE_LEVEL_DEFAULT"
        #displayVar '_levelAsk' "$_levelAsk"
        for element in "${titreLevelListe[@]}";
        do
            #displayVar  '_levelAsk' "$_levelAsk" 'element' "$element"
            if [[ "$_levelAsk" == "$element" ]]; then _level="$element";break; fi
        done
        trackLevel="$_level";
        fctOut; return 0;
}
#


################
## Set....... ##
################
    ## SETTING DES tag, dl_ ##

    function clearTPCF(){
        fctIn
        initTome
        initPartie
        initChapitre
        initFichier
        initMusique
        fctOut; return 0;
    }

    function clearTags(){
        fctIn
        # if use "declare" -> local
        tag_language='fr'
        # - collection - #
        setArtist ''  ;  # auteurs
        tag_genreNo=0;  tag_genre='';

        # - livre - #
        setAlbumTitre '';   # titre du cycle
        setTitle '';   # titre du livre/chanson/etc
        setTrack;
        setYear 0;

        tag_url='';

        dl_prenote=''; tag_note=''; dl_postnote='';

        setDl_date '';
        setDl_voix '';
        setDl_source '';
        setDl_duree '';
        dl_editeur='';

        initCovers
        fctOut; return 0;
    }
    #setArtist( "$tag_title" ) si '' = ''


    function setArtist(){
        fctIn "$*"
        tag_artist=${1:-''};
        auteurNormalize='';
        auteurTN='';
        if [[ "$tag_artist" != '' ]]
        then
            getNormalizeText "$tag_artist"; auteurNormalize="$normalizeText";
            auteurTN="-$auteurNormalize"
        fi
        fctOut; return 0;
    }

    #setTitle( "$tag_title" )
    function setTitle(){
        fctIn "$*"
        tag_title=${1:-''};
        titreNormalize='';
        titreTN='';
        if [[ "$tag_title" != '' ]]
        then
            getNormalizeText "$tag_title";  titreNormalize="$normalizeText";
            titreTN="-$titreNormalize";
        fi
        setNameAuto
        fctOut; return 0;
    }

    #setYear( tag_year )
    function setYear(){
        fctIn "$*"
        if [[ $# -eq 0 ]]
        then
            tag_year=0;
            dl_date=''
        else
            if [[ $1 -ne 0 ]]
            then
                tag_year=$1;
                dl_date="$tag_year"
            fi
        fi
        #echo $LINENO;displayVar 'tag_year' "$tag_year" 'dl_date' "$dl_date"
        fctOut; return 0;
    }


    function setTrackNb(){
        fctIn "$*"
        tag_trackNb=$1
        fctOut; return 0;
    }

    #incTrackNb
    function incTrackNb(){
        fctIn
        ((tag_trackNb++));
        fctOut; return 0;
    }

    #incTrack 
    function incTrack(){
        fctIn
        ((tag_track++));
        if [[ $tag_track -gt $tag_trackNb ]]
        then
            erreursAddEcho "Track $tag_track/$tag_trackNb"
        fi
        fctOut; return 0;
    }

    #setTrack trackNo [trackNb]
    function setTrack(){
        fctIn "$*"
        if [[ $# -eq 0 ]]
        then
            tag_track=0;
            tag_trackNb=0;
            fctOut; return $E_ARG_NONE;
        fi
        tag_track=$1;

        if [[ $# -eq 2 ]]; then setTrackNb $2; fi
        fctOut; return 0;
    }


    #setDl_date( 'mm.jj' [year] )
    function setDl_date(){
        fctIn "$*"
        local mmjj=${1:-''}
        if [[ $# -eq 2 ]];then tag_year=$2; fi
        dl_date='';
        if [[ $tag_year -gt 0 ]];then   dl_date="$tag_year";fi
        if [ "$dl_date" != '' -a "$mmjj" != '' ]; then dl_date="$dl_date.$mmjj";fi
        #displayVar $LINENO':tag_year' "$tag_year" 'dl_date' "$dl_date"
        fctOut; return 0;
    }


    function setDl_voix(){
        fctIn "$*"
        dl_voix=${1:-''}
        dl_voixNormalize='';
        dl_voixTN='';
        if [[ "$dl_voix" != '' ]]
        then
            getNormalizeText "$dl_voix";  dl_voixNormalize="$normalizeText";
            dl_voixTN="-$dl_voixNormalize";
        fi
        fctOut; return 0;
    }

    function setDl_source(){
        fctIn "$*"
        dl_source=${1:-''}
        dl_sourceNormalize=''
        dl_sourceTN='';
        if [[ "$dl_source" != '' ]]
        then
            getNormalizeText "$dl_source";  dl_sourceNormalize="$normalizeText";
            dl_sourceTN="-$dl_sourceNormalize"
        fi
        fctOut; return 0;
    }

    function setDl_duree(){
        fctIn "$*"
        dl_duree=${1:-''}
        dl_dureeT='';
        if [[ "$dl_duree" != '' ]];then     dl_dureeT="-$dl_duree";fi
        setNameAuto
        fctOut; return 0;
    }

    #setDureeYT( codeYoutube)
    # recupere la duree de la video YT
    function setDureeYT(){
        fctIn "$*"
        if [[ $# -ne 1 ]]
        then
            erreursAddEcho "$FUNCNAME() n'a pas le bon nombre de parametre($#/1). $FUNCNAME(\"codeYoutube\")"
            fctOut; return $E_ARG_REQUIRE;
        fi

        local referenceVideoYoutube="$1"
        dl_duree='';
        #spliter la duree
        local _duration=$(youtube-dl --get-duration "https://www.youtube.com/watch?v=$referenceVideoYoutube");
        if [[ $? -ne 0 ]];then return $E_INODE_NOT_EXIST;fi
        local _dureeTbl=($(echo $_duration | tr ":" "\n"))
        local _dureeTblNb=${#_dureeTbl[@]}

        local -i h=0
        local -i m=0
        local -i s=0

        #displayVar '_duration' "$_duration"
        #displayVar '_dureeTblNb' "$_dureeTblNb"
        #echo '_dureeTbl[@]' "${_dureeTbl[@]}"
        if [[ $_dureeTblNb -eq 1 ]]
        then
            #s=${_dureeTbl[0]}
            #displayVar 's0' "$s"
            fctOut; $E_ARG_BAD; # il y a que des secondes dl_duree=''
        fi

        if [[ $_dureeTblNb -eq 2 ]]
        then
            m=${_dureeTbl[0]};s=${_dureeTbl[1]}
            #displayVar 'm0' "$m" 's1' "$s"
        fi

        if [[ $_dureeTblNb -eq 3 ]]
        then
            h=${_dureeTbl[0]};
            #displayVar '_dureeTbl[1]' "${_dureeTbl[1]}"
            m=10#${_dureeTbl[1]};
            #s=${_dureeTbl[2]};
            #displayVar 'h0' "$h" 'm1' "$m" 's2' "$s"
        fi


        local _mlz="$m"  # 
        if [[ $m -lt 10 ]]; then _mlz="0$m"; fi
        setDl_duree "${h}h$_mlz"
        #setNameAuto #redondant
        fctOut; return 0;
    }
#


###############
## NAME AUTO ##
###############
    #clearNameAuto(){
    #    livreTitreNormalize_Auto=''
    #    livreTitreNormalize_Album_Fichier=''
    #    livreTitreNormalize_Album_Chapitre=''
    #}

    function isplayNameAuto(){
        fctIn
        displayVar 'livreTitreNormalize_Auto'       "$livreTitreNormalize_Auto"
        displayVar 'albumTitreTN'                   "$albumTitreTN";
        displayVar 'tomeNoNbT'                      "$tomeNoNbT"        'tomeTitreTN'       "$tomeTitreTN";
        displayVar 'partieNoNbT'                    "$partieNoNbT"      '$partieTitreTN'    "$partieTitreTN";
        displayVar 'titreTN'                        "$titreTN";
        displayVar 'chapitreNoNbT'                  "$chapitreNoNbT"    'chapitreTitreTN'   "$chapitreTitreTN";
        displayVar 'fichierNoNbT'                   "$fichierNoNbT"     'fichierTitreTN'    "$fichierTitreTN";
        displayVar 'auteurTN$dl_sourceTN$dl_voixTN$dl_dureeT' "$auteurTN$dl_sourceTN$dl_voixTN$dl_dureeT"
        fctOut; return 0;
    }

    # remplit automatiquement les differentes variables
    function setNameAuto(){
        fctIn

        #local dateTN='';
        #                             if [[ "$dl_date" != '' ]]; then dateTN="$dl_date";fi
        #lzTomeNo;tomeNoNbT='';       if [[ $tomeNb -gt 1 ]];    then tomeNoNbT="-T$tlz$tomeNo-$tomeNb";fi
        #lzPartieNo;partieNoNbT='';    if [[ $partieNb -gt 1 ]];  then partieNoNbT="-P$partieNo-$partieNb";fi
        #lzChapitreNo;chapitreNoNbT='';if [[ $chapitreNb -gt 1 ]];then chapitreNoNbT="-C$clz$chapitreNo-$chapitreNb";fi
        #lzChapitreNo;fichierNoNbT=''; if [[ $fichierNb -gt 1 ]]; then fichierNoNbT="-F$flz$fichierNo-$fichierNb";fi
        #dl_dureeT='';                 if [[ "$dl_duree" != '' ]];then dl_dureeT="-$dl_duree";fi

        # - generique - #
        lzGeneriqueNo;trackNoNbT='';  if [[ $tag_trackNb -gt 1 ]]; then trackNoNbT="-$tlz$tag_track-$tag_trackNb";fi 
        generique_Glz_TitreNormalize="$trackNoNbT$titreTN";
        generique_date_Glz_TitreNormalize="$dl_date$trackNoNbT$titreTN$dl_dureeT";

        # - livre - #
        local v="$dl_date";
        v="$v$albumTitreTN";
        v="$v$tomeNoNbT$tomeTitreTN";
        v="$v$partieNoNbT$partieTitreTN";
        if [ "$titreTN" == "$albumTitreTN" -o "$titreTN" == "$tomeTitreTN" -o "$titreTN" == "$partieTitreTN" -o "$titreTN" == "$chapitreTitreTN" -o "$titreTN" == "$fichierTitreTN" ]
        then
            #echo 'Le titre est egale a une categorie on ne rajoute pas le titre'
            local void=0;
        else
            #echo 'on rajoute pas le titre'
            v="$v$titreTN";
        fi
        
        v="$v$chapitreNoNbT$chapitreTitreTN";
        v="$v$fichierNoNbT$fichierTitreTN";
        v="$v$auteurTN$dl_sourceTN$dl_voixTN$dl_dureeT"
        # https://linuxhint.com/trim_string_bash/ ${v##*(-)} # ne fonctionne pas ici !

        #displayVarDebug $LINENO'dl_dureeT' "$dl_dureeT"
        #suppression des tirets de debut
        if [[ "${v:0:1}" == '-' ]];then     v="${v:1}";fi #  ${v:0:1}= First Car
        livreTitreNormalize_Auto="$v"
        setTagNoteLivre

        # - musique - #
        setNameMusiqueAuto

        #displayNameAuto
        fctOut; return 0;
    }

    function setNameMusiqueAuto(){
        fctIn

        # - generique - #
        lzGeneriqueNo;trackNoNbT='';  if [[ $tag_trackNb -gt 1 ]]; then trackNoNbT="-$tlz$tag_track-$tag_trackNb";fi 
        generique_Glz_TitreNormalize="$trackNoNbT$titreTN";
        generique_date_Glz_TitreNormalize="$dl_date$trackNoNbT$titreTN$dl_dureeT";

        # - musique - #
        local v='';
        #v="$dl_date";
        v="$v$albumTitreTN";
        v="$v$musiqueNoNbT";
        #        if [[ "$titreTN" == "$albumTitreTN" -o "$titreTN" == "$musiqueTitreTN" -o "$titreTN" == "$fichierTitreTN" ]]
        #        then
        #            #echo 'Le titre est egale a une categorie on ne rajoute pas le titre'
        #            local void=0;
        #        else
        #            v="$v$titreTN";
        #        fi

        v="$v$musiqueTitreTN";
        v="$v$fichierNoNbT$fichierTitreTN";
        v="$v$auteurTN$dl_sourceTN$dl_dureeT"
        # https://linuxhint.com/trim_string_bash/ ${v##*(-)} # ne fonctionne pas ici !

        #suppression des tirets de debut
        if [[ "${v:0:1}" == '-' ]];then     v="${v:1}";fi #  ${v:0:1}= First Car
        musiqueTitreNormalize_Auto="$v"
        #displayVarDebug 'musiqueTitreNormalize_Auto' "$musiqueTitreNormalize_Auto"
        #setTagNoteLivre
        fctOut; return 0;
    }
#


##########
## MONO ##
##########
    #function setMono [isMono=1]
    function setMono(){
        fctIn "$*"
        isMono=${1:-1}
        FFMpegMono='';
        if [[ $isMono -eq 1 ]];then FFMpegMono=' -ac 1';fi
        fctOut; return 0;
    }
#


################
## CREATE.... ##
################
    #hierarchie: collection -> auteur -> year -> album -> livre -> partie -> chapitre -> fichier
    # collection: repertoire contenant des album (chemin absolu)
    # auteur
    # year
    # tome
    # album: repertoire contenant des livres,etc
    # livre: repertoire contenant des fichiers (tome, parties,chapitres,images,etc)
    # fichier: fichier faisant partie d'une suite 


    # - creer une nouvelle collection - #
    # -- creer un repertoire (avec son chemin) du meme nom que la collection et va dans ce repertoire -- #
    # $1: nom du repertoire
    function createCollection(){
        fctIn "$*"

        if [[ $# -eq 0 ]]
        then
            erreursAddEcho 'Nom du repertoire de la collection manquant.'
            fctOut; return $E_ARG_REQUIRE;
        fi

        titre1 'Nouvelle collection:' "$1"
        clearTags
        collectionRoot="$1"
        getNormalizeText "$1"
        collectionPath="$normalizeText"; # global

        #initChapitre 1 1; #la numeratation des chapitre dans le nom de fichier ne se fait que si chapireNb>1

        if [[ ! -d "$$collectionPath" ]]
        then
            mkdir -p "$collectionPath" 
            if [[ $? -eq 0 ]]
            then
                notifsAddEcho "Création du repertoire de la collection: $collectionPath"
            else
                erreursAddEcho "Le repertoire de collection '$collectionPath' ne peut etre créé"
                fctOut; return $E_INODE_NOT_EXIST;
            fi
        fi


        cd "$collectionPath"
        if [[ $? -eq 0 ]]
        then
            notifsAddEcho "Entrer dans la collection: $collectionPath"
        else
            erreursAddEcho "Le repertoire de collection '$collectionPath' ne peut etre crée ou ouvert"
            fctOut; return $E_INODE_NOT_EXIST;
        fi


        fctOut; return 0;
    }

    function closeCollection(){
        cd ..
    }


    # - creer un nouvel auteur - #
    # -- creer un repertoire (dans le rep courant) et va dans ce repertoire -- #
    # $1: nom de de l'auteur (normalisé)
    # $2: repertoire de l'auteur (normalisé, facultatif, si absent = nom normalisé)
    # modifie la var global (ou local) auteurNormalize
    function createAuteur(){
        fctIn "$*"
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "$FUNCNAME ('auteur' ['repertoire'] )."
            fctOut; return $E_ARG_REQUIRE;
        fi

        setArtist "$1"
        
        local rep="$auteurNormalize"
        if [[ $# -ge 2 ]]
        then
            getNormalizeText "$2"
            rep="$normalizeText"
        fi

        mkdir -p "$rep" && cd "$rep"
        local cdError=$?
        if [[ $cdError -eq 0 ]]
        then
            notifsAddEcho "Création de l'auteur: $tag_artist ($PWD)"
        else
            erreursAddEcho "Le repertoire '$rep' de l'auteur '$tag_artist' ne peut etre crée ou ouvert"
            fctOut; return $E_INODE_NOT_EXIST;
        fi

        fctOut; return 0;
    }

    function closeAuteur(){
        fctIn "$*"
        setArtist '';
        cd ..
        fctOut; return 0;
    }


    function createYear(){
        fctIn "$*"
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "$FUNCNAME (YYYY)"
            fctOut; return $E_ARG_REQUIRE;
        fi
        local annee=$1
        setYear $annee
        mkdir -p "$annee" && cd "$annee"
        local cdError=$?
        if [[ $cdError -eq 0 ]]
        then 
            notifsAddEcho "Création de l'année: '$annee' ($PWD)";
        else
            erreursAddEcho "Le repertoire '$annee' ne peut etre crée ou ouvert"
            fctOut; return $E_INODE_NOT_EXIST;
        fi
        fctOut; return 0;
    }

    function closeYear(){
        fctIn "$*"
        setYear;
        #setDl_date ;
        cd ..
        fctOut; return 0;
    }
#


############
## ALBUMS ##
############
    function setAlbumTitre(){
        fctIn "$*"
        tag_album=${1:-''}
        albumTitreNormalize='';
        albumTitreTN='';
        if [[ "$tag_album" != '' ]]
        then
            getNormalizeText "$tag_album";          albumTitreNormalize="$normalizeText";
            albumTitreTN="-$albumTitreNormalize";
        fi
        if [[ "$titreLevel" == 'Album' ]]; then setTitle "albumTitre"; fi
        fctOut; return 0;
    }

    # - creer un nouvel album - #
    # -- creer un repertoire (dans le rep courant) et va dans ce repertoire -- #
    # $1: nom de de l'album (normalisé)
    # $2: repertoire de l'album (normalisé, facultatif, si absent = nom normalisé)
    # modifie la var global (ou local) albumTitreNormalize
    function createAlbum(){
        fctIn "$*"
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "$FUNCNAME ('album' ['repertoire'] )."
            fctOut; return $E_ARG_REQUIRE;
        fi
        titre1 "Création d'un auteur: $1"


        local nom="$1"
        setAlbumTitre "$nom"
        local rep="$albumTitreNormalize"

        if [[ $# -ge 2 ]]
        then
            getNormalizeText "$2"; rep="$normalizeText"
        fi

        mkdir -p "$rep" && cd "$rep"
        local cdError=$?
        if [[ $cdError -eq 0 ]]
        then
            notifsAddEcho "Création de l'album: '$nom' ($PWD)";
        else
            erreursAddEcho "Le repertoire '$rep' de l'album '$nom' ne peut etre crée ou ouvert"
            fctOut; return E_INODE_NOT_EXIST;
        fi
        fctOut; return 0;
    }

    function closeAlbum(){
        fctIn "$*"
        setAlbumTitre ''
        cd ..
        fctOut; return 0;
    }
#


#################
## REPERTOIRES ##
#################
    #createRepertoire() -> inexistant le generique ne crait pas de repertoire
    #Creation d'un repertoire neutre (ex: chronologie)
    function createRepertoire(){
        fctIn "$*"
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "$FUNCNAME ('repertoire' )."
            fctOut; return $E_ARG_REQUIRE;
        fi

        local _rep="$1"
        #getNormalizeText "$1"; _rep="$normalizeText";
        _rep="$(echoNormalizeText "$_rep")"
        titre1 "Création d'un repertoire: '$_rep'"

        mkdir -p "$_rep" && cd "$_rep"
        local cdError=$?
        if [[ $cdError -eq 0 ]]
        then
            notifsAddEcho "Création du repertoire générique'$_rep' ($PWD)"
        else
            erreursAddEcho "Le repertoire générique '$_rep' ne peut etre crée ou ouvert"
            fctOut; return $E_INODE_NOT_EXIST;
        fi

       fctOut; return 0;
    }

    function closeRepertoire(){
        fctIn "$*"
        cd ..
        fctOut;return 0;
    }

    function setTagNoteGenerique(){
        local _editeur='';    if [[ "$dl_editeur"    != '' ]]; then _editeur="Editeur $dl_editeur." ;fi
        local _sortieLe='';   if [[ "$dl_date"       != '' ]]; then _sortieLe="Sortie le $dl_date." ;fi
        local _duree='';      if [[ "$dl_duree"      != '' ]]; then _duree="Durée $dl_duree."       ;fi
        local _source='';     if [[ "$dl_source"     != '' ]]; then _source="Source $dl_source."    ;fi
        local _traduction=''; if [[ "$dl_traduction" != '' ]]; then _traduction="Traduction de $dl_traduction." ;fi
        local _url='';        if [[ "$tag_url"       != '' ]]; then _url="$tag_url."                ;fi  # ajouté a postnote selon le format id3
        tag_note="$_editeur$_sortieLe$_duree$_source$_traduction" # $_url est ajouté par saveTags
        return 0;
    }
#


###########
## COVER ##
###########
    function initCovers(){
        fctIn "$*"
        setCoverLevelSelect
        coverLevelUrls['Cover (front)']='';
        coverLevelUrls['yt_file']='';
        coverLevelUrls['yt_file_JPG']='';
        coverLevelUrls['yt_file_PNG']='';
        coverLevelUrls['yt_page']='';

        coverLevelPaths['Cover (front)']='';
        coverLevelPaths['yt_file']='';
        coverLevelPaths['yt_file_JPG']='';
        coverLevelPaths['yt_file_PNG']='';
        coverLevelPaths['yt_page']='';

        coverLevelDescs['Cover (front)']='';
        coverLevelDescs['yt_file']='';
        coverLevelDescs['yt_file_JPG']='';
        coverLevelDescs['yt_file_PNG']='';
        coverLevelDescs['yt_page']='';
        fctOut; return 0;
    }

    function showCovers(){
        fctIn "$*"
        titre2 'LES COVERS'

        echo 'LEVELS'
        displayVar 'COVER_LEVEL_DEFAULT'  "$COVER_LEVEL_DEFAULT" 
            echo '  coverLevelListe[@]  ' "${coverLevelListe[@]}"
        displayVar 'coverLevelSelect   '  "$coverLevelSelect"

        for _coverName in "${coverLevelListe[@]}"; 
        do
            echo "cover: $_coverName";
            displayVar "coverLevelUrls"  "${coverLevelUrls[$_coverName]}"

            local _path="${coverLevelPaths[$_coverName]}";
            local _isPathOK="$_path"
            if [[ ! -f "$_path" ]];then _isPathOK="$WARN$_path$NORMAL"; fi
            displayVar "coverLevelPaths" "$_isPathOK"
        done;


        echo 'TYPES'

        displayVar 'COVER_TYPE_DEFAULT'  "$COVER_TYPE_DEFAULT" 
            echo '  coverTypesListe[@]' "${coverTypesListe[@]}"
        displayVar 'coverTypeSelect   '  "$coverTypeSelect"

        for _coverName in "${coverTypesListe[@]}"; 
        do
            _path=${coverTypesPaths[$_coverName]:-''}
            displayVar "coverTypesPaths[$_coverName]" "$_path";
            _description=${coverTypesDescs[$_coverName]:-''}
            displayVar "coverDescriptions[$_coverName]" "$_description";
        done;
        fctOut; return 0;
    }

    # setCoverTypeSelect ( [coverTypeSelect] )
    function setCoverTypeSelect(){
        fctIn "$*"
        coverTypeSelect="$COVER_TYPE_DEFAULT"
        if [[ $# -eq 1 ]]
        then
            local _type="$1"
            for level in "${coverTypesListe[@]}"; 
            do
                if [[ "$level" == "$_type" ]]
                then
                    coverTypeSelect="$_type"
                    fctOut; return $E_TRUE;
                fi
            done;
        fi
        fctOut; return 0;
    }

    # setCoverLevelSelect ( [coverLevel] )
    function setCoverLevelSelect(){
        fctIn "$*"
        coverLevelSelect="$COVER_LEVEL_DEFAULT"
        if [[ $# -eq 1 ]]
        then
            local _lvl="$1"
            for level in "${coverLevelListe[@]}";
            do
                if [[ "$level" == "$_lvl" ]]
                then
                    coverLevelSelect="$_lvl"
                    fctOut; return $E_TRUE;
                fi
            done;
        fi
        fctOut; return 0;
    }


    ## -- Telechargement de cover -- ##

    # dlCover "lien" "nomFichier[.ext]" ["description"] ["coverLevel"]
    # ajoute un cover (copie l'extension du lien source au fichier destination)
    # et l'associe a un type de cover 
    # https://unix.stackexchange.com/questions/84915/add-album-art-cover-to-mp3-ogg-file-from-command-line-in-batch-mode
    # lame --ti logo.jpg file.mp3
    # https://id3.org/id3v2.3.0#Attached_picture
    function dlCover(){
        fctIn "$*"

        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "$FUNCNAME($*) n'a pas le bon nombre de parametre($?/1-4). $FUNCNAME (lien nomFichier[.ext] [description] [coverType=front}])";
            fctOut; return $E_ARG_REQUIRE;
        fi
        local source=${1:-''}   #url
        if [[ "$source"  == '' ]];then fctOut; return $E_ARG_BAD;fi

        titre4 "Téléchargement du cover: $source"
        local sourceExt=${source##*.}

        local destination=${2:-"$titreNormalize"} #avec sou sans ext
        local destExt=${destination##*.}

        setCoverLevelSelect ${4:-"$coverLevelSelect"}

        coverLevelUrls["$coverLevelSelect"]="$source"

        coverLevelDescs["$coverLevelSelect"]=${3:-''}

        # gestion des extentions non valide
        isExtensionValide "$sourceExt"
        if [[ $? -eq 0 ]]
        then
            notifsAddEcho "Extension $destination.$sourceExt non valide."
            fctOut; return $E_ARG_BAD;
        fi

        # Si destination n'a pas d'extension (nom==ext) 
        if [[ "$destination" == "$destExt" ]]
        then
            destExt="${sourceExt,,}"; #lowercase
            destination="$destination.$destExt";
            fi

        #normalize le chemin de la destination
        local repCourant=$(pwd);
        getNormalizeText "$destination";
        coverLevelPaths["$coverLevelSelect"]="$repCourant/$normalizeText";
        local _path="${coverLevelPaths["$coverLevelSelect"]}"

        if [[ ! -f "$_path" ]]
        then
            #showCovers
            titre2 "wget \"$_path\" \"$source\""
            wget       --no-verbose --continue --show-progress --progress=bar --output-document="$_path" "$source"
            if [[ $? -ne 0 ]]
            then
                erreursAddEcho "$FUNCNAME($destination): Erreur lors du téléchargement"
            fi
        else
            echo "Cover '$destination' déja téléchargé."
        fi

        # mise a jours de type
        coverTypesPaths["$coverTypeSelect"]="${coverLevelPaths["$coverLevelSelect"]}"
        coverTypesDescs["$coverTypeSelect"]="${coverLevelDescs["$coverLevelSelect"]}"

        fctOut; return 0;
    }


    # dlCoverPageYT( URLLogo )
    # telecharge le logo (fournis en parametre) d'une page YT
    function dlCoverPageYT(){
        fctIn "$*"
        if [[ $# -ne 1 ]]
        then
            erreursAddEcho "$FUNCNAME() n'a pas le bon nombre de parametre(?#/1). $FUNCNAME ( URLLogo )";
            fctOut; return $E_ARG_REQUIRE;
        fi

        local url="$1";
        local destinationName="$auteurNormalize-logoYT"
        local destinationPath="$PWD/$destinationName.webp"; #VERIFIER SI format wep systematique!
        titre4 "Téléchargement du cover youtube: $auteurNormalize"

        coverLevelUrls['yt_page']='';
        coverLevelPaths['yt_page']='';

        if [[ -f "$destinationPath" ]]
        then
            notifsAddEcho "$FUNCNAME($*): $destinationPath déjà téléchargé."
            coverLevelUrls['yt_page']="$url";
            coverLevelPaths['yt_page']="$destinationPath";
        else
            if [[ "${url:0:4}" == 'http' ]]
            then
                coverLevelUrls['yt_page']="$url"
                echo wget --output-document="$destinationPath"  "$url";
                     wget --output-document="$destinationPath"  "$url";
                if [[ $? -eq 0 ]]
                then
                    coverLevelPaths['yt_page']="$destinationPath";
                else
                erreursAddEcho "Erreur lors du téléchargement du cover yt: $url"
                fctOut; return $E_INODE_NOT_EXIST;
                fi
            else
                erreursAddEcho "Le lien du thumbnail du cover est NI un fichier NI un http: $url"
                fctOut; return $E_ARG_BAD;
            fi
        fi

        #file "${coverLevel['yt_page_path']}"
        #displayVar 'url'                "$url"
        #displayVar 'destinationPath'    "$destinationPath"
        #showCovers

        fctOut; return 0;
    }


    # dlCoverFileYT( 'youtube-code' 'destinationPath(sans extension)' )
    #telecharge la miniature d'une video youtube
    function dlCoverFileYT(){
        fctIn "$*"
        if [[ $isDlCoverYT -eq 0 ]];then return; fi
        if [[ $# -ne 2 ]]
        then
            erreursAddEcho "$FUNCNAME() n'a pas le bon nombre de parametre($?/2). $FUNCNAME ( youtube-code destinationPath )";
            fctOut; return $E_ARG_REQUIRE;
        fi
        # on recupere l'adresse http de la miniature
        local _codeYT="$1"
        local url=$(youtube-dl --get-thumbnail https://www.youtube.com/watch?v=$_codeYT);
        local URLextension=${url##*.};    #displayVar 'URLextension' "$URLextension"
        
        local destinationExtention="$URLextension"
        local _ext="${destinationExtention:0:3}";#    _ext=${_ext,,}; {lowercase}
        #displayVar  'URLextension' "$URLextension" '_ext' "$_ext"
        if [[ "$_ext" == 'jpg' ]];then destinationExtention='jpg';fi
        local destinationName="$2-$_codeYT";
        local destinationPath="$PWD/$destinationName.$destinationExtention";

        coverLevelUrls['yt_file']='';
        coverLevelPaths['yt_file']='';

        titre4 "Téléchargement de la miniature de la video YT: $_codeYT"

        if [[ -f "$destinationPath" ]]
        then
            notifsAddEcho "$FUNCNAME($*): $destinationPath déjà téléchargé."
            coverLevelUrls['yt_file']="$url";
            coverLevelPaths['yt_file']="$destinationPath";
        else
            if [[ "${url:0:4}" == 'http' ]]
            then
                coverLevelPaths['yt_file']="$url"
                #echo wget --output-document="$destinationPath"  "$url";
                wget      --output-document="$destinationPath"  "$url";
                if [[ $? -eq 0 ]]
                then
                    coverLevelPaths['yt_file']="$destinationPath";
                else
                erreursAddEcho "$FUNCNAME($*): Erreur lors du téléchargement du cover yt_file: $url"
                fctOut; return $E_INODE_NOT_EXIST;
                fi
            else
                erreursAddEcho "Le lien du thumbnail du cover est NI un fichier NI un http: $url"
                fctOut; return $E_ARG_BAD;
            fi

            coverLevelPaths['yt_file_PNG']='';
            coverLevelPaths['yt_file_JPG']='';

            #si webp -> convertir en jpg (pour ogg)
            if [[ "$URLextension" == 'webp' ]]
            then
                local destinationJPGPath="$PWD/$destinationName.jpg";
                if [[ -f "$destinationJPGPath" ]]
                then
                    coverLevelPaths['yt_file_JPG']="$destinationJPGPath"
                else
                    evalCmd "ffmpeg -loglevel quiet -i \"$destinationPath\" \"$destinationJPGPath\";"
                    if [[ $? -eq 0 ]]
                    then
                        coverLevelPaths['yt_file_JPG']="$destinationJPGPath"
                    fi
                fi
            fi
        fi
        #showCovers

        fctOut; return 0;
    }


    ## -- COVER: GESTION DES TYPES -- ##
    # setCoverLevelFront: ([coverFrontPath])
    # defini le chemin du coverFront manuellement ou automatiquement selon un algorythme
    # du + precis au + global
    function setCoverFront(){
        fctIn "$*"

        # set manuel
        if [[ $# -eq 1 ]]
        then
            coverTypesPaths['Cover (front)']="$1"
            fctOut; return 0;
        fi

        # set automatique
        coverTypesPaths['Cover (front)']='';

        local _cover="${coverLevelPaths['yt_file_JPG']}"
        if [[ -f "$_cover" ]];then coverTypesPaths['Cover (front)']="$_cover"; fctOut; return 0;  fi

        local _cover="${coverLevelPaths['yt_file_PNG']}"
        if [[ -f "$_cover" ]];then coverTypesPaths['Cover (front)']="$_cover"; fctOut; return 0;  fi

        local _cover="${coverLevelPaths['yt_file']}"
        if [[ -f "$_cover" ]];then coverTypesPaths['Cover (front)']="$_cover"; fctOut; return 0;  fi

        local _cover="${coverLevelPaths['yt_page']}"
        if [[ -f "$_cover" ]];then coverTypesPaths['Cover (front)']="$_cover"; fctOut; return 0;  fi
        fctOut; return 0;
    }


    ## -- AJOUT DU COVER DANS LE FICHIER -- ##
    # saveCover( coverLevel [coverType] ){
    # sauvegarde la cover dans le fichier en tant que metadata
    function saveCover(){
        fctIn "$*"

        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "$LINENO: $FUNCNAME() n'a pas le bon nombre de parametre($#/1-2). $FUNCNAME (filename [coverLevel])";
            fctOut; return $E_ARG_REQUIRE;
        fi

        local _coverLevel="$1",
        local _coverType=${2:-"$COVER_TYPE_DEFAULT"}

        #getcoverLevel

        titre4 "Sauvegarde du cover comme metadatas: $_coverPath"
        if [[ "$_coverLevel" != '' ]]
        then
            local _coverPath="${coverTypesPaths[$_coverType]}";
            if [[ ! -f "$_coverPath" ]]
            then
                erreursAddEcho "$FUNCNAME($*): Erreur lors de l'ajout du cover $_coverPath"
                addOggCover "$fileName" "$_coverPath";
            fi
        else
            erreursAddEcho "$FUNCNAME($*): '$_coverLevel non définit'"
        fi
        fctOut; return 0;
    }

    # calcul du tag FFMpeg pour ajouter le cover 
    function setCoverFFmpegTags(){
        fctInEcho "$*";
        coverFFmpegTags='';
        if [[ ! -f "${coverLevelPaths["Cover (front)"]}" ]]
        then
            erreursAddEcho "$FUNCNAME: Chemin du cover invalide (${coverLevelPaths["Cover (front)"]})";
            fctOut; return $E_INODE_NOT_EXIST;
        fi

        coverFrontPathTmpFile="$fileName.tags.$extension" # $fileName.tags.$extension
        if [[ -f "$coverFrontPathTmpFile" ]];then rm "$coverFrontPathTmpFile";fi


        local _coverFrontTags=''
        #local _coverFrontPath="${coverLevelPaths["Cover (front)"]}\"
            _coverFrontTags="  -i \"${coverLevelPaths["Cover (front)"]}\" -map 1:0 -metadata:s:1 title=\"${coverTypesDescs["Cover (front)"]}\" -metadata:s:1 comment=\"Cover (front)\""
            coverFFmpegTags="$_coverFrontTags"
        displayVarDebug 'coverFrontPathTmpFile' "$coverFrontPathTmpFile"
        displayVarDebug 'coverFFmpegTags' "$coverFFmpegTags"
        fctOut; return 0;
    }

    # Ajout du cover #
    function addCoverLocal(){
        fctIn "$*";
        setCoverFFmpegTags

        local _coverFrontPath="${coverLevelPaths["Cover (front)"]}"
        titre4 "Sauvegarde du cover comme metadatas(_coverFrontPath): $_coverFrontPath"
        #evalCmd "ffmpeg$ffmpeg_loglevelTags -i \"$fileName\"$_coverFrontPath -codec copy$tagVersion $ffmpeg_tags$_coverFrontTags \"$tmpFile\""
        evalCmd "ffmpeg$ffmpeg_loglevelTags -i \"$fileName\"$_coverFrontPath -codec copy$tagVersion $coverFFmpegTags \"$coverFrontPathTmpFile\""
        if [[ $? -ne 0 ]]
        then
            erreursAddEcho "$FUNCNAME:$WARN erreur avec $coverFrontPathTmpFile ($_coverFrontPath)"
            #ls -s "$tmpFile"
        else #ajout du cover sans erreur
            echo "$FUNCNAME:Ajout du cover $coverFrontPathTmpFile ($_coverFrontPath)"
            #ls -l "$tmpFile"
            evalCmdEchoDebug "mv \"$coverFrontPathTmpFile\" \"$fileName\""
        fi
        fctOut; return 0;
    }

    #addOggCover ( fichier.ogg cover.png|cover.jpg)
    function addOggCover(){
        if [[ $isTrapCAsk -eq 1 ]];then return $E_CTRL_C;fi
        fctIn "$*";

        local oggPath="$1"
        local coverLevel="$2"

        titre4 "Sauvegarde du cover comme metadatas(_coverFrontPath): $oggPath"
        if [[ ! -f "${oggPath}" ]]
        then
            erreursAddEcho "$FUNCAME($*): fichier $oggPath introuvable."
            fctOut; return $E_INODE_NOT_EXIST;
        fi

        if [[ ! -f "${coverLevel}" ]]
        then
            erreursAddEcho "$FUNCAME($*): fichier $coverLevel introuvable."
            fctOut; return $E_INODE_NOT_EXIST;
        fi

        local imageMimeType=$(file -b --mime-type "${coverLevel}")

        if [[ "${imageMimeType}" != "image/jpeg" ]] && [ "${imageMimeType}" != "image/png" ]]
        then
            erreursAddEcho "$FUNCAME($*): Le cover doit etre au format jpg ou png."
            fctOut; return $E_ARG_BAD;
        fi

        local oggMimeType=$(file -b --mime-type "${oggPath}")

        if [[ "${oggMimeType}" != "audio/x-vorbis+ogg" ]] && [ "${oggMimeType}" != "audio/ogg" ]]
        then
            erreursAddEcho "$FUNCAME($*): fichier $oggPath n'est pas au format ogg."
            fctOut; return $E_ARG_BAD;
        fi

        # Export existing comments to file
        local commentsPath="$(mktemp -t "tmp.XXXXXXXXXX")"
        vorbiscomment --list --raw "${oggPath}" > "${commentsPath}"

        # Remove existing images
        sed -i -e '/^metadata_block_picture/d' "${commentsPath}"

        # Insert cover image from file

        # metadata_block_picture format
        # See: https://xiph.org/flac/format.html# metadata_block_picture
        local imageWithHeader="$(mktemp -t "tmp.XXXXXXXXXX")"
        local description=''

        # Reset cache file
        echo -n '' > "${imageWithHeader}"

        # Picture type <32>
        printf "0: %.8x" 3 | xxd -r -g0 >> "${imageWithHeader}"

        # Mime type length <32>
        printf "0: %.8x" $(echo -n "${imageMimeType}" | wc -c) | xxd -r -g0 >> "${imageWithHeader}"

        # Mime type (n * 8)
        echo -n "${imageMimeType}" >> "${imageWithHeader}"

        # Description length <32>
        printf "0: %.8x" $(echo -n "${description}" | wc -c) | xxd -r -g0 >> "${imageWithHeader}"

        # Description (n * 8)
        echo -n "${description}" >> "${imageWithHeader}"

        # Picture with <32>
        printf "0: %.8x" 0 | xxd -r -g0  >> "${imageWithHeader}"

        # Picture height <32>
        printf "0: %.8x" 0 | xxd -r -g0 >> "${imageWithHeader}"

        # Picture color depth <32>
        printf "0: %.8x" 0 | xxd -r -g0 >> "${imageWithHeader}"

        # Picture color count <32>
        printf "0: %.8x" 0 | xxd -r -g0 >> "${imageWithHeader}"

        # Image file size <32>
        printf "0: %.8x" $(wc -c "${coverLevel}" | cut --delimiter=' ' --fields=1) | xxd -r -g0 >> "${imageWithHeader}"

        # Image file
        cat "${coverLevel}" >> "${imageWithHeader}"

        echo "metadata_block_picture=$(base64 --wrap=0 < "${imageWithHeader}")" >> "${commentsPath}"

        # Update vorbis file comments
        vorbiscomment --write --raw --commentfile "${commentsPath}" "${oggPath}"

        # Delete temp files
        rm "${imageWithHeader}"
        rm "${commentsPath}"
        fctOut; return 0;
    }
#

###############
## GENERIQUE ##
###############
    #initGenerique generiqueNo generiqueNb artiste nom_de_l_album
    function initGenerique(){ 
        fctIn "$*"

        generiqueNo=${1:-1};
        tag_track=$generiqueNo;

        generiqueNb=${2:-$generiqueNo};
        tag_trackNb=$generiqueNb;

        if [[ $# -ge 3 ]]; then setArtist "$3";fi
        if [[ $# -eq 4 ]]; then setAlbum "$4";fi
    
        #showTags
        fctOut; return 0;
    }

    #prefixe generiqueNo avec un zero si 0..9
    function lzGeneriqueNo(){
        fctIn "$*"
        glz=''
        if [[ $generiqueNb -lt 10 ]];then fctOut; return 0;fi
        if [[ $generiqueNo -lt 10 ]];then glz="0" ;fi
        fctOut; return 0;
    }

    #Ajoute un generique sans changer la valeur de generiqueNo
    #addGenerique( 'dl_date' 'titre' ){
    function addGenerique(){
        fctIn "$*"
        if [[ $# -ne 2 ]]
        then
            erreursAddEcho "$FUNCNAME() n'a pas le bon nombre de parametre($#/2). $FUNCNAME(\"generiqueTitre\" [generiqueNo] )"
            fctOut; return $E_ARG_REQUIRE;
        fi

        incGenerique "$1" "$2" $generiqueNo;     # ne change pas la valeur actuelle (utilise)
        fctOut; return 0;
    }

    #defini le generiqueNo ou augmente generiqueNo de 1 si non defini en 2eme arg
    #defini le titre
    #incGenerique (dl_date) ("titre"|'') (generiqueNo|"+1") # titre==''->=album
    function incGenerique(){
        fctIn "$*"
        if [[ $# -lt 1 ]]
        then
            erreursAddEcho "$FUNCNAME() n'a pas le bon nombre de parametre(1). $FUNCNAME ( \"generiqueTitre\" GeneriqueNo|\"+1\" )"
            fctOut; return $E_ARG_REQUIRE;
        fi

        setDl_date "$1"
        setTitle "$2"
        local numeroAsk=${3:-'+1'};
        #displayVar 'dl_date' "$dl_date" 'tag_titile' "$tag_title" 'numeroAsk' "$numeroAsk"
        if [[ "$numeroAsk" == '+1' ]] || [ "$numeroAsk" == '' ]] # demande de forcage de l'incrementation?
        then (( generiqueNo++ ))
        #elif (( "$numeroAsk" > 0 )) #s'il est  numérique
        #then
        else
            generiqueNo=$numeroAsk
        fi
        tag_track=$generiqueNo;

        # avertissement en cas de depassement de numero de generiqueNo
        if [[ $generiqueNo -gt $generiqueNb ]]
        then
            echo "$WARN Generique actuelle $generiqueNo/$generiqueNb"
            return
        fi

        lzGeneriqueNo;
        setNameAuto
        setTagNoteGenerique
        fctOut; return 0;
    }

    function closeGenerique(){
        fctIn
        # avertissement en cas de manque d'entrée
        if [ $generiqueNb -gt 0 -a $generiqueNo -lt $generiqueNb ]
        then
            erreursAddEcho "$tag_title: Il manque une ou plusieurs entrées $generiqueNo/$generiqueNb"
        fi
        initGenerique
        fctOut; return 0;
    }
#


###########
## LIVRE ##
###########
    function createLivreAuto(){
        if [[ $isTrapCAsk -eq 1 ]];then return $E_CTRL_C;fi
        fctIn "$*"
        if [[ $# -ne 0 ]];then erreursAddEcho "$FUNCNAME (' titre' )."; fctOut;return $E_ARG_REQUIRE;fi

        createLivre "$1" "$livreTitreNormalize_Auto"
        local -i retour=$?
        fctOut; return $retour;
    }

    # - creer un nouveau livre - #
    # -- creer un repertoire (dans le rep courant) et va dans ce repertoire -- #
    # $1: nom de du livre (normalisation)
    # $2: repertoire du livre (normalisation, facultatif, si absent = nom normalisé)
    # modifie la var global (ou local) titreNormalize
    function createLivre(){
        if [[ $isTrapCAsk -eq 1 ]];then return $E_CTRL_C;fi
        fctIn "$*"
        if [[ $# -eq 0 ]];then erreursAddEcho "$FUNCNAME ('livre' ['repertoire'] ).";fctOut; return $E_ARG_REQUIRE;fi

        #tomeNo=0;  # ne pas initialisé ici car il est utilisé en amont pour la creation du reperotire-livre
        #partieNb=0; # mettre 0 pour neutraliser l'affichage dans le nom du fichier (fonction non implementé dans DeuxTroisChapitre) 
        #partieNo=0;
        #initPartie  0 0 # 0 pour neutraliser dans les valeurs auto
        initChapitre 0 0 # 0 pour neutraliser dans les valeurs auto

        local _libreNom="$1"
        setTitle "$_libreNom"

        titre4 "Création du livre: $_libreNom"

        local rep=${2:-"$titreNormalize"}
        getNormalizeText "$rep"; rep="$normalizeText"

        echo ''
        mkdir -p "$rep" && cd "$rep"
        if [[ $? -eq 0 ]]
        then
            notifsAddEcho "Création du livre: '$_libreNom' ($PWD)";
        else
            erreursAddEcho "Le repertoire '$rep' du livre '$_libreNom' ne peut etre crée ou ouvert"
            fctOut; return $E_INODE_NOT_EXIST;
        fi
        fctOut; return 0;
    }


    function setTagNoteLivre(){
        fctIn "$*"
        local _LuPar='';      if [[ "$dl_voix"       != '' ]]; then _LuPar="Lu Par $dl_voix."       ;fi
        local _editeur='';    if [[ "$dl_editeur"    != '' ]]; then _editeur="Editeur $dl_editeur." ;fi
        local _sortieLe='';   if [[ "$dl_date"       != '' ]]; then _sortieLe="Sortie le $dl_date." ;fi
        local _duree='';      if [[ "$dl_duree"      != '' ]]; then _duree="Durée $dl_duree."       ;fi
        local _source='';     if [[ "$dl_source"     != '' ]]; then _source="Source $dl_source."    ;fi
        local _traduction=''; if [[ "$dl_traduction" != '' ]]; then _traduction="Traduction de $dl_traduction." ;fi
        local _url='';        if [[ "$tag_url"       != '' ]]; then _url="$tag_url."                ;fi  # ajouté a postnote selon le format id3


        local _tomeTxt='';
        if [[ $tomeNb -gt 1 ]];then _tomeTxt="Tome $tomeNo/$tomeNb.";fi
        local _chapitreTitre='';
        if [[ "$chapitreTitre" != '' ]]
        then
            _chapitreTitre="Chapitre $chapitreNo/$chapitreNb:$chapitreTitre."
        fi
        tag_note="$_tomeTxt$_chapitreTitre$_LuPar$_editeur$_sortieLe$_duree$_source$_traduction" # $_url est ajouté par saveTags
        fctOut; return 0;
    }

    function closeLivre(){
        fctIn "$*"
        dl_prenote=''; tag_note=''; dl_postnote='';
        initPartie
        initChapitre
        initFichier
        setDl_date
        setDl_voix
        setDl_source
        dl_editeur='';
        cd ..
        fctOut; return 0;
    }
#


###########
## TOMES ##
###########
    function initTome(){
        fctIn "$*"
        setTomeNo ${1:-0};
        setTomeNb ${2:-$tomeNo};
        fctOut; return 0;
    }

    function incTomeNb(){
        displayDebug "$FUNCNAME($*)"
        ((tomeNb++))
        setTomeNb $tomeNb
    }

    function setTomeNb(){
        fctIn "$*"
        tomeNb=${1:-0};
        if [[ "$trackLevel" == 'Tome' ]];then setTrackNb $tomeNb;fi
        #lzTomeNb; #existe pas
        tomeNoNbT='';if [[ $tomeNb -gt 1 ]];then tomeNoNbT="-T$tlz$tomeNo-$tomeNb";fi
        setNameAuto
        fctOut; return 0;
    }

    function incTomeNo(){
        fctIn "$*"
        (( tomeNo++ ))
        setTomeNo $tomeNo
        fctOut; return 0;
    }

    function setTomeNo(){
        fctIn "$*"
        tomeNo=${1:-0};
        if [[ "$trackLevel" == 'Tome' ]];then tag_track=$tomeNo;fi
        lzTomeNo;
        tomeNoNbT='';if [[ $tomeNb -gt 1 ]];then tomeNoNbT="-T$tlz$tomeNo-$tomeNb";fi
        setNameAuto
        fctOut; return 0;
    }

    #prefixe tomeNo avec un zero si 0..9
    function lzTomeNo(){
        fctIn "$*"
        tlz=''
        if [[ $tomeNb -lt 10 ]];then return $E_ARG_BAD;fi
        if [[ $tomeNo -lt 10 ]];then tlz='0';fi
        fctOut; return 0;
    }

    function setTomeTitre(){ 
        fctIn "$*"
        displayVar '1' "$1"
        tomeTitre="${1:-''}";
        displayVar 'tomeTitre' "$tomeTitre"

        tomeTitreNormalize='';
        tomeTitreTN='';
        if [[ "$tomeTitre" != '' ]]
        then
            getNormalizeText "$tomeTitre"; tomeTitreNormalize="$normalizeText"; 
            tomeTitreTN="-$tomeTitreNormalize"
        fi
       if [[ "$titreLevel" == 'Tome' ]]; then setTitle "$tomeTitre"; fi
        setNameAuto
        fctOut; return 0;
    }

    #Ajoute un Tome sans changer la valeur de tomeNo
    #addTome( 'titre' ){
    function addTome(){
        fctIn "$*"
        if [[ $# -ne 1 ]]
        then
            erreursAddEcho "$FUNCNAME() n'a pas le bon nombre de parametre($#/1). $FUNCNAME( \"tomeTitre\" )"
            return $E_ARG_REQUIRE;
        fi
        incTome "$1" $tomeNo;     # ne change pas la valeur actuelle (utilise)
        fctOut; return 0;
    }

    #defini le tomeNo ou augmente tomeNo de 1 si non defini en 2eme arg
    #defini le titre du tome
    #incTome "tomeTitre" [tomeNo]
    function incTome(){
        fctIn "$*"
        setTomeTitre "${1:-''}";

        if [[ $# -eq 2 ]]
        then tomeNo=$2
        else (( tomeNo++ ));
        fi
        setTomeNo $tomeNo

        # avertissement en cas de depassement
        if [[ $tomeNo -gt $tomeNb ]]
        then
            erreursAddEcho "Tome actuel $tomeNo/$tomeNb"
            fctOut; return $E_FALSE;
        fi
        setNameAuto;
        fctOut; return 0;
    }

    function closeTome(){
        fctIn "$*"
        # avertissement en cas de manque de tome
        if [ $tomeNb -gt 0 -a  $tomeNo -lt $tomeNb ]
        then
            erreursAddEcho "Il manque un ou plusieurs tomes $tomeNo$WARN/$tomeNb$tomeTitreTN"
        fi
        initTome
        fctOut; return 0;
    }
#


#############
## PARTIES ##
#############
    #initPartie [No=0] [Nb=No]
    function initPartie(){
        fctIn "$*"
        setPartieNo ${1:-0}
        setPartieNb ${2:-$partieNo};
        fctOut; return 0;
    }

    function incPartieNb(){
        fctIn "$*"
        ((partieNb++))
        setPartieNb $partieNb
        fctOut; return 0;
    }

    function setPartieNb(){
        fctIn "$*"
        partieNb=${1:-0};
        if [[ "$trackLevel" == 'Partie' ]];then setTrackNb $partieNb;fi
        #lzPartieNb; #existe pas
        partieNoNbT='';if [[ $partieNb -gt 1 ]];then partieNoNbT="-P$plz$partieNo-$partieNb";fi
        setNameAuto
        fctOut; return 0;
    }

    function incPartieNo(){
        fctIn "$*"
        ((partieNo++))
        setPartieNo $partieNo
    }

    function setPartieNo(){
        fctIn "$*"
        partieNo=${1:-0};
        if [[ "$trackLevel" == 'Partie' ]];then tag_track=$partieNo;fi

        lzPartieNo;
        partieNoNbT='';if [[ $partieNb -gt 1 ]];then partieNoNbT="-P$plz$partieNo-$partieNb";fi
        setNameAuto
        fctOut; return 0;
    }

    function lzPartieNo(){
        fctIn
        plz=''
        if [[ $partieNb -lt 10 ]];then return $E_FALSE;fi
        if [[ $partieNo -lt 10 ]];then plz='0';fi
        fctOut; return 0;
    }

    function setPartieTitre(){
        fctIn "$*"
        partieTitre="${1:-''}";
        partieTitreNormalize='';
        partieTitreTN=''
        if [[ "$partieTitre" != '' ]]
        then
            getNormalizeText "$partieTitre"; partieTitreNormalize="$normalizeText"; 
            partieTitreTN="-$partieTitreNormalize"
        fi
        if [[ "$titreLevel" == 'Partie' ]]; then setTitle "$partieTitre"; fi
        setNameAuto
        fctOut; return 0;
    }

    #Ajoute une partie sans changer la valeur de partieNo
    #addPartie( 'titre' ){
    function addPartie(){
        fctIn "$*"
        if [[ $# -ne 1 ]]
        then
            erreursAddEcho "$FUNCNAME() n'a pas le bon nombre de parametre($#/1). $FUNCNAME( \"partieTitre\" )"
            return $E_ARG_REQUIRE;fctOut;
        fi
        incPartie "$1" $partieNo;     # ne change pas la valeur actuelle (utilise)
        fctOut; return 0;
    }

    #defini le partieNo ou augmente partieNo de 1 si non defini en 2eme arg
    #defini le titre de la partie
    #addPartie "partieTitre" [partieNo]
    function incPartie(){
        fctIn "$*"
        setPartieTitre "${1:-''}"

        if [[ $# -eq 2 ]]
        then partieNo=$2
        else (( partieNo++ ));
        fi
        setPartieNo $partieNo

        # avertissement en cas de depassement
        if [[ $partieNo -gt $partieNb ]]
        then
            erreursAddEcho "Partie actuelle $partieNo/$partieNb$partieTitreTN"
        fi
        setNameAuto;
        fctOut; return 0;
    }

    function closePartie(){
        fctIn "$FUNCNAME"
        if [ $partieNb -gt 0 -a  $partieNo -lt $partieNb ]
        then
            erreursAddEcho "Il manque un ou plusieurs parties $partieNo/$partieNb$partieTitreTN"
        fi
        initPartie
        #cd ..  #  une partie reste dans le meme repertoire
        fctOut; return 0;
    }
#


###############
## CHAPITRES ##
###############
    #initChapitre [chapitreNo=0] [chapitreNb=chapitreNo]
    function initChapitre(){
        fctIn "$*"
        setChapitreNo ${1:-0};
        setChapitreNb ${2:-$chapitreNo};
        fctOut; return 0;
    }

    function incChapitreNb(){
        fctIn "$*"
        ((chapitreNb++))
        setChapitreNb $chapitreNb
        fctOut; return 0;
    }

    function setChapitreNb(){
        fctIn "$*"
        chapitreNb=${1:-0};
        if [[ "$trackLevel" == 'Chapitre' ]];then setTrackNb $chapitreNb;fi
        #lzChapitreNb; #existe pas
        chapitreNoNbT='';if [[ $chapitreNb -gt 1 ]];then chapitreNoNbT="-C$clz$chapitreNo-$chapitreNb";fi
        setNameAuto
        fctOut; return 0;
    }

    function incChapitreNo(){
        fctIn "$*"
        ((chapitreNo++))
        setChapitreNo $chapitreNo
        fctOut; return 0;
    }

    function setChapitreNo(){
        fctIn "$*"
        chapitreNo=${1:-0};
        if [[ "$trackLevel" == 'Chapitre' ]];then tag_track=$chapitreNo;fi
        lzChapitreNo;
        chapitreNoNbT='';if [[ $chapitreNb -gt 1 ]];then chapitreNoNbT="-C$clz$chapitreNo-$chapitreNb";fi
        setNameAuto
        fctOut; return 0;
    }

    #prefixe chapitreNo avec un zero si 0..9
    function lzChapitreNo(){
        clz=''
        if [[ $chapitreNb -lt 10 ]];then return 0;fi
        if [[ $chapitreNo -lt 10 ]];then clz='0' ;fi
        return 0
    }

    function setChapitreTitre(){ 
        fctIn "$*"
        chapitreTitre="${1:-''}";
        chapitreTitreNormalize='';
        chapitreTitreTN='';
        if [[ "$chapitreTitre" != '' ]]
        then
            getNormalizeText "$chapitreTitre";          chapitreTitreNormalize="$normalizeText"; 
            chapitreTitreTN="-$chapitreTitreNormalize"
        fi
        if [[ "$titreLevel" == 'Chapitre' ]]; then setTitle "$chapitreTitre"; fi
         setNameAuto
         fctOut; return 0;
   }

    #Ajoute un Chapitre sans changer la valeur de chapitreNo
    #addChapitre( 'titre' ){
    function addChapitre(){
        fctIn "$*"
        if [[ $# -ne 1 ]]
        then
            erreursAddEcho "$FUNCNAME() n'a pas le bon nombre de parametre($#/1). $FUNCNAME( \"chapitreTitre\" )"
            return $E_ARG_REQUIRE;
        fi
        incChapitre "$1" $chapitreNo;     # ne change pas la valeur actuelle (utilise)
        fctOut; return 0;
    }

    #defini le chapitreNo ou augmente chapitreNo de 1 si non ndefini en 2eme arg
    #defini le titre du chapitre
    #incChapitre "chapitreTitre" [chapitreNo]
    function incChapitre(){
        fctIn "$*"
        setChapitreTitre "${1:-''}";

        if [[ $# -eq 2 ]]
        then chapitreNo=$2
        else (( chapitreNo++ ));
        fi
        setChapitreNo $chapitreNo

        # avertissement en cas de depassement de chapitre
        if [[ $chapitreNo -gt $chapitreNb ]]
        then
            erreursAddEcho "Chapitre actuel $chapitreNo/$chapitreNb$chapitreTitreTN"
        fi
        setNameAuto;
        fctOut; return 0;
    }

    function closeChapitre(){
        fctIn "$*"
        # avertissement en cas de manque de chapitre        
        if [ $chapitreNb -gt 0 -a  $chapitreNo -lt $chapitreNb ]
        then
            erreursAddEcho "Il manque un ou plusieurs chapitres $chapitreNo/$chapitreNb$chapitreTitreTN"
        fi
        initChapitre
        fctOut; return 0;
    }
#


##############
## FICHIERS ##
##############
    function initFichier(){
    fctIn "$*"
        setFichierNo ${1:-0};
        setFichierNb ${2:-$fichierNo};
        fctOut; return 0;
    }

    function incFichierNb(){
        fctIn "$*"
        ((fichierNb++))
        setFichierNb $fichierNb
        fctOut; return 0;
    }

    function setFichierNb(){
        fctIn "$*"
        fichierNb=${1:-0};
        if [[ "$trackLevel" == 'Fichier' ]];then setTrackNb $fichierNb;fi
        #lzTomeNb; #existe pas
        fichierNoNbT='';if [[ $fichierNb -gt 1 ]];then fichierNoNbT="-F$flz$fichierNo-$fichierNb";fi
        setNameAuto
        fctOut; return 0;
   }

    function incFichierNo(){
        fctIn "$*"
        ((fichierNo++))
        setFichierNo $fichierNo
        fctOut; return 0;
    }

    function setFichierNo(){
        fctIn "$*"
        fichierNo=${1:-0};
        if [[ "$trackLevel" == 'Fichier' ]];then tag_track=$fichierNo;fi
        lzFichierNo;
        fichierNoNbT='';if [[ $fichierNb -gt 1 ]];then fichierNoNbT="-F$flz$fichierNo-$fichierNb";fi
        setNameAuto
        fctOut; return 0;
    }

    #prefixe fichierNo avec un zero si 0..9
    function lzFichierNo(){
        fctIn "$*"
        flz=''
        if [[ $fichierNb -lt 10 ]];then return 0;fi
        if [[ $fichierNo -lt 10 ]];then flz="0" ;fi
        fctOut; return 0;
    }

    function setFichierTitre(){ 
        fctIn "$*"
        fichierTitre="${1:-''}";
        fichierTitreNormalize='';
        fichierTitreTN='';

        if [[ "$fichierTitre" != '' ]]
        then
            getNormalizeText "$fichierTitre"; fichierTitreNormalize="$normalizeText"; 
            fichierTitreTN="-$fichierTitreNormalize"
        fi
        if [[ "$titreLevel" == 'Fichier' ]]; then setTitle "$fichierTitre"; fi
        setNameAuto
        fctOut; return 0;
    }

    #Ajoute un Fichier sans changer la valeur de fichiereNo
    #addFichier( 'titre' ){
    function addFichier(){
        fctIn "$*"
        if [[ $# -ne 1 ]]
        then
            erreursAddEcho "$FUNCNAME() n'a pas le bon nombre de parametre($#/1). $FUNCNAME( \"fichierTitre\" )"
            fctOut; return $E_ARG_REQUIRE;
        fi
        incFichier "$1" $fichierNo;     # ne change pas la valeur actuelle (utilise)
        fctOut; return 0;
    }

    #defini le fichierNo ou augmente fichierNo de 1 si non defini en 2eme arg
    #defini le titre du chapitre
    #incFichier "chapitreTitre" [chapitreNo]
    function incFichier(){
        fctIn "$*"
        setFichierTitre "${1:-''}";

        if [[ $# -eq 2 ]]
        then fichierNo=$2
        else (( fichierNo++ ));
        fi
        setFichierNo $fichierNo

        echo -e "\n---Ajout du Fichier no $fichierNo:$fichierTitre---";

        # avertissement en cas de depassement de fichier
        if [[ $fichierNo -gt $fichierNb ]]
        then
            erreursAddEcho "Fichier actuel $fichierNo/$fichierNb$fichierTitreTN"
        fi

        setNameAuto;
        fctOut; return 0;
    }

    function closeFichier(){
        fctIn "$*"
        # avertissement en cas de manque de fichier        
        if [ $fichierNb -gt 0 -a  $fichierNo -lt $fichierNb ]
        then
            erreursAddEcho "Il manque un ou plusieurs fichiers $fichierNo/$fichierNb"
        fi
        fctOut; return 0;
    }
#


#############
## MUSIQUE ##
#############
    #createMusiqueAlbum "nom album" ["rep"]
    function createMusiqueAlbum(){
        if [[ $isTrapCAsk -eq 1 ]];then return $E_CTRL_C;fi
        fctIn "$*"
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "$LINENO: $FUNCNAME() n'a pas le bon nombre de parametre(1). $FUNCNAME (albumTitre [repertoire])"
            fctOut; return $E_ARG_REQUIRE;
        fi
        setLevels 'Musique'
        initMusique 1 1

        setAlbumTitre "$1";
        setTagNoteMusique

        titre4 "Création de l'album: $albumTitre"        
        local rep=${2:-"$albumTitreNormalize"} 
        getNormalizeText "$rep";    rep="$normalizeText"

        mkdir -p "$rep" && cd "$rep"
        if [[ $? -eq 0 ]]
        then
            notifsAddEcho "Création de l'album: '$tag_album' ($PWD)";
        else
            erreursAddEcho "Le repertoire '$rep' de l'album '$tag_album' ne peut etre crée ou ouvert"
            fctOut; return $E_INODE_NOT_EXIST;
        fi
        fctOut; return 0;
    }

    function closeMusiqueAlbum(){
        fctIn "$*"
        # avertissement en cas de manque de chapitre        
        if [ $musiqueNb -gt 0 -a  $musiqueNo -lt $musiqueNb ]
        then
            erreursAddEcho "Il manque une ou plusieurs musiques $musiqueNo$WARN/$musiqueNb"
        fi
        setAlbumTitre
        dl_prenote=''; tag_note=''; dl_postnote='';
        cd ..
        fctOut; return 0;
    }

    function setTagNoteMusique(){
        local _editeur='';    if [[ "$dl_editeur"    != '' ]]; then _editeur="Editeur $dl_editeur." ;fi
        local _sortieLe='';   if [[ "$dl_date"       != '' ]]; then _sortieLe="Sortie le $dl_date." ;fi
        local _duree='';      if [[ "$dl_duree"      != '' ]]; then _duree="Durée $dl_duree."       ;fi
        local _source='';     if [[ "$dl_source"     != '' ]]; then _source="Source $dl_source."    ;fi
        local _traduction=''; if [[ "$dl_traduction" != '' ]]; then _traduction="Traduction de $dl_traduction." ;fi
        local _url='';        if [[ "$tag_url"       != '' ]]; then _url="$tag_url."                ;fi  # ajouté a postnote selon le format id3

        tag_note="$_editeur$_sortieLe$_duree$_source$_traduction" # $_url est ajouté par saveTags

    }

    function initMusique(){
        fctIn "$*"
        setMusiqueNo ${1:-0};
        setMusiqueNb ${2:-$musiqueNo};
        fctOut; return 0;
    }



    function incMusiqueNb(){
        fctIn "$*"
        ((musiqueNb++))
        setMusiqueNb $musiqueNb
        fctOut; return 0;
    }

    function setMusiqueNb(){
        fctIn "$*"
        musiqueNb=${1:-0};
        if [[ "$trackLevel" == 'Musique' ]];then setTrackNb $musiqueNb;fi
        #lzMusiqueNb; #existe pas   
        musiqueNoNbT='';if [[ $musiqueNb -gt 1 ]];then musiqueNoNbT="-$mlz$musiqueNo-$musiqueNb";fi
        setNameMusiqueAuto
        fctOut; return 0;
   }


    function incMusiqueNo(){
        fctIn "$*"
        ((musiqueNo++))
        setMusiqueNo $musiqueNo
        fctOut; return 0;
    }

    function setMusiqueNo(){
        fctIn "$*"
        musiqueNo=${1:-0};
        if [[ "$trackLevel" == 'Musique' ]];then tag_track=$musiqueNo;fi
        lzMusiqueNo;
        musiqueNoNbT='';if [[ $musiqueNb -gt 1 ]];then musiqueNoNbT="-$mlz$musiqueNo-$musiqueNb";fi
        setNameMusiqueAuto
        fctOut; return 0;
    }

    #prefixe musiqueNo avec un zero si 0..9
    function lzMusiqueNo(){
        fctIn "$*"
        mlz=''
        if [[ $musiqueNb -lt 10 ]];then return 0;fi
        if [[ $musiqueNo -lt 10 ]];then mlz="0" ;fi
        fctOut; return 0;
    }

    function setMusiqueTitre(){ 
        fctIn "$*"
        musiqueTitre="${1:-''}";
        musiqueTitreNormalize='';
        musiqueTitreTN='';

        if [[ "$musiqueTitre" != '' ]]
        then
            getNormalizeText "$musiqueTitre"; musiqueTitreNormalize="$normalizeText"; 
            musiqueTitreTN="-$musiqueTitreNormalize"
        fi
        if [[ "$titreLevel" == 'Musique' ]]; then setTitle "$musiqueTitre"; fi
        setNameMusiqueAuto
        fctOut; return 0;
    }

    #Ajoute une musique sans changer la valeur de musiqueeNo
    #addMusique( 'titre' ){
    function addMusique(){
        fctIn "$*"
        if [[ $# -ne 1 ]]
        then
            erreursAddEcho "$FUNCNAME() n'a pas le bon nombre de parametre($#/1). $FUNCNAME( \"musiqueTitre\" )"
            return $E_ARG_REQUIRE;
        fi
        incMusique "$1" $musiqueNo;     # ne change pas la valeur actuelle (utilise)
        fctOut; return 0;
    }


    #defini le fichierNo ou augmente fichierNo de 1 si non defini en 2eme arg
    #defini le titre du chapitre
    #incFichier "chapitreTitre" [chapitreNo]
    function incMusique(){
        fctIn "$*"
        setMusiqueTitre "${1:-''}";

        if [[ $# -eq 2 ]]
        then musiqueNo=$2
        else (( musiqueNo++ ));
        fi
        setMusiqueNo $musiqueNo

        echo -e "\n---Ajout de la musique no $musiqueNo:$musiqueTitre---";

        # avertissement en cas de depassement de fichier
        if [[ $musiqueNo -gt $musiqueNb ]]
        then
            erreursAddEcho "Musique actuel $musiqueNo/$musiqueNb$musiqueTitreTN"
        fi

        setNameMusiqueAuto;
        fctOut; return 0;
    }
#


##########
## LIER ##
##########
    # lier cible [newTitre]
    # original: chemin du fichier ou sera pointer le lien
    function lier(){
        fctIn "$*"
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "$FUNCNAME($*): Cible du lien manquant."
        fi

        original="$1";
        newTitre="$original" #par defaut l'alias a le meme nom
        if [[ $# -ge 2 ]]
        then
            newTitre="$2"
        fi

        # suppression d'un eventuel alias prexistant (peut etre obsolete)
        unlink "$newTitre"
        evalCmd "ln -s \"$original\" \"$newTitre\""
        fctOut; return 0;
    }
#


#################
## TELECHARGER ##
#################
    # telecharge l'url dans le repertoire courant (changer par createCollection/createAlbum/createTitre)
    # usage:
    # dlwget "$url" "$destination"
    function dlwget(){
        if [[ $isTrapCAsk -eq 1 ]]; then return  ; fi
        fctIn "$*"
        echo "Téléchargement via $FUNCNAME($*)"
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho ". $FUNCNAME[$#/2](\"$url\" \"$destination\")"
            fctOut; return $E_ARG_REQUIRE;
        fi

        url="$1"
        if [[ "$url" == '' ]]
        then
            erreursAddEcho "$FUNCNAME($*): Pas d'url fournis."
            fctOut; return $E_ARG_BAD;
        fi
        ((stats['dl_total']++))
        ((stats['wg_dl_total']++))

        if [[ $# -lt 2 ]]
        then
            erreursAddEcho "$FUNCNAME($*): $url sans destination"
            ((stats['dl_fail']++))
            ((stats['wg_dl_fail']++))
            fctOut; return $E_ARG_BAD;
        fi

        local infoProgression="$INFO"[$chapitreNo/$chapitreNb]"$NORMAL";
        echo -n "$infoProgression"

        getNormalizeText "$2"
        destination="$normalizeText"

        # ajout auto de l'extension sur destination avec celle de l'url (si pas defini dans destination)
        local URLextension=${url##*.}
        local DESTextension=${destination##*.} # si pas d'extension alors égale à destination
        if [[ "$URLextension" != "$DESTextension" ]]
        then
            destination="$destination.$URLextension"
        fi

        #displayVar "URLextension" "$URLextension" "DESTextension" "$DESTextension" "destination" "$destination"
        # le fichier existe t'il (-f) ?
        if [[ -f "$destination" ]]
        then
            notifsAddEcho "$FUNCNAME($*): $url:$destination pré-existe."

            # - Force la sauvegarde des tags si demander - #
            if [[ $isSaveTags -eq 1 ]]
            then
                saveTags "$destination"
            fi

            ((stats['dl_exist']++))
            ((stats['wg_dl_exist']++))
        elif [[ $isNoDownload -eq 0 ]]
        then
            local _verbose="--no-verbose"
            if [[ $verbosity -gt 0 ]]; then _verbose='';  fi

            wget $_verbose --continue --show-progress --progress=bar --output-document="$destination" "$url"

            if [[ $? -eq 0 ]]  # wget=?
            then
                notifsAddEcho "$FUNCNAME($*): '$url': '$destination'"
                ((stats['dl_ok']++))
                ((stats['wg_dl_ok']++))
                saveTags "$destination";
            else 
                erreursAddEcho "$FUNCNAME($*):  $url:$destination"
                ((stats['dl_fail']++))
                ((stats['wg_dl_fail']++))
            fi
        fi

        convertir "$destination"
        fctOut; return 0;
    }

    #dlwgetDeuxChapitre $chapitreNu $episodeComplet
    function dlwgetUnChapitres(){
        fctIn "$*"
        if [[ $# -lt 2 ]];  then return $E_ARG_REQUIRE; fi
        local -i chapitreNu=$1;
        local url="$2"
        local __postnote=${3:-''};
        local partieTN=''
        dl_postnote=" $__postnote";
        addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
        dlwget "$url" "${livreTitreNormalize_Auto}.mp3"
        fctOut; return 0;
    }

    #dlwgetDeuxChapitre $chapitreNu $episodeComplet
    function dlwgetDeuxChapitres(){
        fctIn "$*"
        if [[ $# -lt 2 ]];  then return $E_ARG_REQUIRE;fctOut; fi
        local -i chapitreNu=$1; local chapitreNuInc1=$((chapitreNu+1));
        local url="$2"
        local __postnote=${3:-''};
        dl_postnote="et chapitre "$chapitreNuInc1" ${chapitreTitreTbl[$chapitreNuInc1]}. $__postnote";
        addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
        dlwget "$url" "${livreTitreNormalize_Auto}_et_chapitre${chapitreNuInc1}-${chapitreTitreTbl[$chapitreNuInc1]}.mp3"
        fctOut; return 0;
    }

    function dlwgetTroisChapitres(){
        fctIn "$*"
        if [[ $# -lt 2 ]];  then return $E_ARG_REQUIRE;fctOut; fi
        local -i chapitreNu=$1; local chapitreNuInc1=$((chapitreNu+1)); local chapitreNuInc2=$((chapitreNu+2));
        local url="$2"
        local __postnote=${3:-''};
        dl_postnote="et chapitre "$chapitreNuInc1" ${chapitreTitreTbl[$chapitreNuInc1]} et chapitre $chapitreNuInc2 ${chapitreTitreTbl[$chapitreNuInc2]}. $__postnote"
        addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
        dlwget "$url" "${livreTitreNormalize_Auto}_et_chapitre${chapitreNuInc1}-${chapitreTitreTbl[$chapitreNuInc1]}_et_chapitre${chapitreNuInc2}-${chapitreTitreTbl[$chapitreNuInc2]}.mp3"
        fctOut; return 0;
    }

    function dlytLivreAuto(){
        if [[ $isTrapCAsk -eq 1 ]]; then return $E_CTRL_C ; fi
        fctIn "$*"
        if [[ $# -eq 0 ]];          then fctOut; return $E_ARG_REQUIRE;fctOut; fi
        local referenceVideoYoutube="$1"
        setDureeYT "$referenceVideoYoutube"
        dlyt "$referenceVideoYoutube"  "$livreTitreNormalize_Auto"
        fctOut; return 0;
    }


    # Utilise la variable globale 'formatNo'
    # usage:
    # dlyt "$referenceVideoYoutube" "$destination"
    function dlyt(){
        if [[ $isTrapCAsk -eq 1 ]]; then return $E_CTRL_C ; fi
        fctIn "$*"

        #retour commun en cas de fail pour la fonction parent (Ne pas utiliser fctIn)
        function retourDlFail(){
            ((stats['dl_fail']++))
            ((stats['yt_dl_fail']++))
            fctOut; # LAISSER CAR ELLE EST DESTINÉ POUR LA FONCTION PARENT
            return 0;
        }


        if [[ $# -ne 2 ]];then erreursAddEcho "$FUNCNAME[$#/2: 'referenceVideo' 'destination') ($*)";fctOut; return $E_ARG_REQUIRE;fi

        titre3 "Téléchargement via $FUNCNAME($*)"
        local referenceVideoYoutube=${1:-''}
        if [[ "$referenceVideoYoutube" == '' ]]
        then
            erreursAddEcho "$FUNCNAME($*): Pas de reference YT"
            retourDlFail; return $E_ARG_REQUIRE;
        fi

        ((stats['dl_total']++))
        ((stats['yt_dl_total']++))

        if [[ $# -lt 2 ]]
        then
            erreursAddEcho "$FUNCNAME: '$referenceVideoYoutube' sans destination"
            retourDlFail; return $E_ARG_REQUIRE;
        fi
        destination="$(echoNormalizeText "$2")"
        displayVarDebug 'destination' "$destination"
            # si pas deja definit on cherche la duree de la video
            #a faire
            #if [[ "$dl_duree" == '' ]]
            #then
            #    setDureeYT "$referenceVideoYoutube";
            #    displayVar 'dl_duree' "$dl_duree" duree
            #fi

            #case "$destination" in
            #    'nomVar1') ;;
            #    'nomVar2') ;;
            #    * )
            #        getNormalizeText "$2";
            #        local destination="$normalizeText";
            #        ;;
            #esac
        
        dlCoverFileYT "$referenceVideoYoutube" "$destination"

        PARAM_TITRE='';
        PARAM_TITRE="$PARAM_TITRE --format $formatNo"

        # - Calcul du nom du fichier et de la refHHTP - #
        local _refHTTP='';
        local destinationConvertName='';
        if [[ $isCodeYoutube -eq 1 ]]
        then
            destinationFileName="$destination-$referenceVideoYoutube.$formatExt"
            destinationConvertName="$destination-$referenceVideoYoutube.$convert"
            _refHTTP="https://www.youtube.com/watch?v=$referenceVideoYoutube"
        else
            destinationFileName="$destination.$formatExt"
            destinationConvertName="$destination.$convert"
            _refHTTP="$referenceVideoYoutube"
        fi

        # - Téléchargement demandé - #
        if [[ $isNoDownload -eq 0 ]]
        then
            # Si convertion demandé, Le fichier de convertion existe t'il?
            local fileNameSansExt=${destinationFileName%.*};
            local fileNameConvert="$fileNameSansExt.$convert"
            #displayVar "convert" "$convert" "fileNameConvert" "$fileNameConvert"

            # - Le fichier est il deja convertit? - #
            if [ "$convert" != '' -a -f "$fileNameConvert" ]
            then
                notifsAddEcho "$FUNCNAME($*): '$referenceVideoYoutube' '$destinationFileName': déja convertit($convert)."
                ((stats['cv_exist']++))
                #((stats['yt_cv_exist']++))

                # - Force la sauvegarde des tags si demander - #
                if [[ $isSaveTags -eq 1 ]]; then saveTags "$fileNameConvert"; fi
                fctOut; return 0;
            fi

            # - Le fichier est-il deja téléchargé? - #
            if [[ -f "$destinationFileName" ]]
            then
                notifsAddEcho "$FUNCNAME($*): '$referenceVideoYoutube' '$destinationFileName' pré-existe."
                ((stats['dl_exist']++))
                ((stats['yt_dl_exist']++))

                # - Force la sauvegarde des tags si demandé - #
                if [[ $isSaveTags -eq 1 ]]; then saveTags "$destinationFileName"; fi

            else     ## - Telechargement - ##

                titre4 'Téléchargement'
                # - test erreur de parametre - #
                if [[ formatNo -eq 0 ]]
                then
                    erreursAddEcho "$FUNCNAME($*): '$referenceVideoYoutube' Format=0."
                    fctOut; return $E_ARG_BAD;
                fi

                # -- TELECHARGEMENT -- #
                local _param=''
                #_param="$YTDL_PARAM_GLOBAL $_param --print-json"
                _param="$YTDL_PARAM_GLOBAL $_param --newline"
                titreCmd "youtube-dl \"$destinationFileName\" \n-> \"$_refHTTP\""
                echo "Lancer 'tail -f $LOG_PATH' sur un autre termninal pour voir l'avancement du téléchargement"
                
                # Obligé de passer par eval sinon youtube-dl génére une erreur
                evalCmdEcho "youtube-dl $_param $PARAM_TITRE --output \"$destinationFileName\" \"$_refHTTP\" 1>> $LOG_PATH"
                
                # - Verifications du fichier recut - #                    
                if [[ ! -f  "$destinationFileName" ]]
                then
                    #displayVar "youtube-dl sortie" "<>0"
                    erreursAddEcho "$FUNCNAME($*): Le fichier '$destinationFileName' n'a pas été téléchargé."
                    retourDlFail;return $E_INODE_NOT_EXIST;
                fi

                # Le fichier existe: test de la taille
                local -i _tailleFichier=$(stat -c "%s" "$destinationFileName")
                displayVarDebug '_tailleFichier' "$_tailleFichier"
                if [[ $_tailleFichier -eq 0 ]]
                then
                    erreursAddEcho "$FUNCNAME($*): Le fichier '$destinationFileName' a une taille nulle."
                    retourDlFail; return $E_INODE_NOT_EXIST;
                fi

                # - Action finale - #
                file      "$destinationFileName"
                saveTags  "$destinationFileName"
                notifsAddEcho "$FUNCNAME($*):  '$referenceVideoYoutube' $destinationFileName"
                ((stats['dl_ok']++))
                ((stats['yt_dl_ok']++))
            fi
        fi

        # - Conversion vers mono - #
        ffmpeg_mono "$destinationFileName"

        ## - Convertion - ##
        convertir "$destinationFileName"


        fctOut; return 0;
    }


    # dlod "$referenceVideoOdysee" "$destination"
    function dlod(){
        if [[ $isTrapCAsk -eq 1 ]]; then return $E_CTRL_C ; fi
        fctIn "$*"
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho ". $FUNCNAME[$#/2](\"$referenceVideo\" \"$destination\")"
            fctOut; return $E_ARG_REQUIRE;
        fi

        echo -e "\nTéléchargement via yt"
        local referenceVideoOdysee="$1"
        if [[ "$referenceVideoOdysee" == '' ]]
        then
            erreursAddEcho "$FUNCNAME($*): Pas de reference Odysee"
            fctOut; return $E_ARG_BAD;
        fi

        ((stats['dl_total']++))
        ((stats['od_dl_total']++))

        if [[ $# -lt 2 ]]
        then
            erreursAddEcho "$FUNCNAME($*): $referenceVideoOdysee sans destination"
            ((stats['dl_fail']++))
            ((stats['od_dl_fail']++))
            fctOut; return $E_ARG_BAD;
        fi

        getNormalizeText "$2"
        local destination="$normalizeText"
        
        #dlCover "$referenceVideoOdysee" "$destination"

        PARAM_TITRE='';
        PARAM_TITRE="$PARAM_TITRE --format $odFormat"

        # - Calcul du nom du fichier et de la refHHTP - #
        local _refHTTP='';
        local destinationConvertName='';
        if [[ $isCodeOdysee -eq 1 ]]
        then
            #destinationFileName="$destination-$referenceVideoOdysee.$formatExt"
            #destinationConvertName="$destination-$referenceVideoOdysee.$convert"

            destinationFileName="$destination.$formatExt"
            destinationConvertName="$destination.$convert"

            _refHTTP="https://odysee.com/$referenceVideoOdysee"
        else
            destinationFileName="$destination.$formatExt"
            destinationConvertName="$destination.$convert"
            _refHTTP="$referenceVideoOdysee"
        fi

        # - Téléchargement demandé - #
        if [[ $isNoDownload -eq 0 ]]
        then
            # Si convertion demandé, Le fichier de convertion existe t'il?
            local fileNameSansExt=${destinationFileName%.*};
            local fileNameConvert="$fileNameSansExt.$convert"
            #displayVar "convert" "$convert" "fileNameConvert" "$fileNameConvert"

            # - Le fichier est il deja convertit? - #
            if [ "$convert" != '' -a -f "$fileNameConvert" ]
            then
                notifsAddEcho "$FUNCNAME($*): '$referenceVideoOdysee' '$destinationFileName': déja convertit($convert)."
                ((stats['cv_exist']++))
                #((stats['yt_cv_exist']++))

                # - Force la sauvegarde des tags si demander - #
                if [[ $isSaveTags -eq 1 ]]; then saveTags "$fileNameConvert"; fi
                return
            fi

            # - Si le fichier est deja téléchargé - #
            if [[ -f "$destinationFileName" ]]
            then
                notifsAddEcho "$FUNCNAME($*): '$referenceVideoOdysee' '$destinationFileName'; pré-existe."
                ((stats['dl_exist']++))
                ((stats['od_dl_exist']++))

                # - Force la sauvegarde des tags si demander - #
                if [[ $isSaveTags -eq 1 ]]; then saveTags "$destinationFileName"; fi

            else     ## - Telechargement - ##
                #echo $LINENO;showDatas

                local _param=''
                #_param="$YTDL_PARAM_GLOBAL $_param --print-json"
                _param="$YTDL_PARAM_GLOBAL $_param --newline"
                displayDebug  "youtube-dl $_param $PARAM_TITRE --output \"$destinationFileName\" \"$_refHTTP\""
                titreCmd "youtube-dl \"$destinationFileName\" \n --> \"$_refHTTP\""
                local ydl_erreur=$(youtube-dl $_param $PARAM_TITRE --output "$destinationFileName" "$_refHTTP" 2>&1 >/dev/null)
                if [[ "$ydl_erreur" == '' ]]
                then
                    displayVar 'youtube-dl sortie' '0'
                    file      "$destinationFileName"
                    saveTags  "$destinationFileName"
                    notifsAddEcho "$FUNCNAME($*): '$referenceVideoOdysee' '$destinationFileName'"
                    ((stats['dl_ok']++))
                    ((stats['yt_dl_ok']++))
                else 
                    displayVar "youtube-dl sortie" "<>0"
                    erreursAddEcho "$FUNCNAME($*): '$referenceVideoOdysee' '$destinationFileName' -> $ydl_erreur"
                    ((stats['dl_fail']++))
                    ((stats['od_dl_fail']++))
                fi
            fi
        fi

        ## - Convertion - ##
        convertir "$destinationFileName"
        fctOut; return 0;
    }


    # dlArchiveOrg "url "destination"
    # telecharge dans le repertoire courant
    function dlAO(){
        if [[ $isTrapCAsk -eq 1 ]]; then return  ; fi
        fctIn "$*"
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho ". $FUNCNAME[$#/2](\"$url\" \"$destination\")"
            fctOut; return $E_ARG_REQUIRE;
        fi

        echo -e "\nTéléchargement via Archive.org"
        local url="$1"
        if [[ "$url" == '' ]]
        then
            erreursAddEcho "$FUNCNAME($*): Pas d'url fournis"
            fctOut; return $E_ARG_BAD;
        fi

        #((stats['ao_total']++))
        #((stats['ao_dl_total']++))

        if [[ $# -lt 2 ]]
        then
            erreursAddEcho "$FUNCNAME: $url sans destination"
            ((stats['dl_fail']++))
            ((stats['ao_dl_fail']++))
            fctOut; return $E_ARG_BAD;
        fi

        getNormalizeText "$2"
        destinationFileName="$normalizeText"


        PARAM_TITRE='';
        #PARAM_TITRE="$PARAM_TITRE --format $formatNo"
        PARAM_TITRE="$PARAM_TITRE --format 1"

        ## - Telechargement - ##
        if [[ -f "$destinationFileName" ]]
        then
            notifsAddEcho "$FUNCNAME($*): '$url' '$destinationFileName': pré-existe."
            ((stats['dl_exist']++))
            ((stats['ao_dl_exist']++))

            # - Force la sauvegarde des tags si demander - #
            if [[ $isSaveTags -eq 1 ]]
            then
                saveTags "$destinationFileName"
            fi

        elif [[ $isNoDownload -eq 0 ]]
        then
            fileNameSansExt=${destinationFileName%.*};
            local fileNameConvert="$fileNameSansExt.$convert"
            if [ "$convert" != '' -a -f "fileNameConvert" ]
            then
                displayDebug "Deja convertit"
                notifsAddEcho "$FUNCNAME($*):  '$destinationFileName': déja convertit."
                #((stats['cv_exist']++))
                #((stats['ao_cv_exist']++))
            else
                #PARAM="$YTDL_PARAM_GLOBAL --format $formatNo"
                PARAM="$YTDL_PARAM_GLOBAL --format 1"
                titreCmd "youtube-dl \"$destinationFileName\" \n --> \"$url\""
                local ydl_erreur=$(youtube-dl ${YTDL_PARAM_GLOBAL} ${PARAM_TITRE} --output "$destinationFileName" "$url" 2>&1 >/dev/null)
                if [[ "$ydl_erreur" == '' ]]
                then
                    file      "$destinationFileName"
                    saveTags  "$destinationFileName"
                    notifsAddEcho "$FUNCNAME($*): youtube-dl '$url' '$destinationFileName'"
                    ((stats['dl_ok']++))
                    ((stats['ao_dl_ok']++))
                else 
                    erreursAddEcho "$FUNCNAME($*):  '$url' '$destinationFileName' -> youtube-dl: $ydl_erreur"
                    ((stats['dl_fail']++))
                    ((stats['ao_dl_fail']++))
                fi
            fi
        fi
        ## - Convertion - ##
        convertir "$destinationFileName"
        fctOut; return 0;
    }
#


################
## DOWNLOADHD ##
################
    # telecharge l'audio et la vidéo sséparement et les fusionnent
    # version simplifié de MERGE (dlyt_merge)
    function downloadHD(){
        if [[ $isTrapCAsk -eq 1 ]]; then return $E_CTRL_C ; fi
        fctIn "$*"
        local codeYT="$1"
        local _fileName=$(youtube-dl --get-filename https://www.youtube.com/watch?v=$codeYT)
        _fileName=${_fileName%.*}
        _fileName=$(echoNormalizeText "$_fileName")
        displayVar '_fileName' "$_fileName"
        evalCmd "youtube-dl --format 140 --output \"$_fileName.m4a\" https://www.youtube.com/watch?v=$codeYT"
        evalCmd "youtube-dl --format 136 --output \"$_fileName.mp4\" https://www.youtube.com/watch?v=$codeYT"
        evalCmd "FusionHD \"$_fileName\""
        fctOut; return 0;
    }

    function FusionHD(){
        if [[ $isTrapCAsk -eq 1 ]]; then return $E_CTRL_C ; fi
        fctIn "$*"
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "$FUNCNAME($*): Nombre de parametre insuffisant $#/1: $FUNCNAME( videoName )"
            return $E_ARG_BAD
	    fi
        local videoName="$1"
        if [[ -f "$videoName-HD.mp4" ]]
        then
            notifsAddEcho "Le fichier '$videoName' est déjà fusionné."
        else
            evalCmd "ffmpeg -i \"$videoName\".mp4 -i \"$videoName\".m4a -codec copy \"$videoName-HD\".mp4"
        fi
        fctOut; return 0;			    
    }


#################
## FFMPEG_MONO ##
#################
    # function ffmpeg_mono "sourcePath" ["$destination(avec ou sans extension)='sourceNom-mono.ext'"}
    # si la destination n'est pas definit alors elle aura les valeur de la source suffixé par -mono (mais avant l'extention)
    function ffmpeg_mono(){
        if [[ $isTrapCAsk -eq 1 ]]; then return $E_CTRL_C ; fi
        fctIn "$*"
        titre3 "$FUNCNAME($*)"

        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "$FUNCNAME($#/1: 'Chemin de la source' ['destination(avec ou sans extension)='sourceNom-mono.ext']) ($*)"
            fctOut; return $E_ARG_REQUIRE;
        fi

        if [[ $isMono -eq 0 ]];then fctOut; return 0;fi # isMono = 0 donc sortie normal

        local _sourcePath="$1"
        if [[ ! -f "$_sourcePath" ]]
        then
            erreursAddEcho "$FUNCNAME($*): Le fichier '$_sourcePath' existe pas."; fctOut; return $E_INODE_NOT_EXIST;
        fi

        local _sourceRep=${_sourcePath%/*}
        if [[ "$_sourceRep" == "$_sourcePath" ]];then _sourceRep='.';fi # s'ils sont identique alors on est dans le rep courant
        local _sourceFilename=${_sourcePath##*/}
        local _sourceName=${_sourceFilename%.*}
        local _sourceExt=${_sourceFilename##*.}
        displayVarDebug '_sourcePath' "$_sourcePath" '_sourceRep' "$_sourceRep" '_sourceFilename' "$_sourceFilename" '_sourceName' "$_sourceName" '_sourceExt' "$_sourceExt"

        local _destinationPathPSP="${2:-"$_sourcePath"}" # avec ou sans ext. Par defaut egale a la source

        local _destinationRep=${_destinationPathPSP%/*}
        # la destination a t'il un repertoire.?
        if [[ "$_destinationRep" == "$_destinationPathPSP" ]]
        then # la destination n'a pas de repertoire
            _destinationRep="$_sourceRep"
        fi
        displayVarDebug '_destinationRep' "$_destinationRep"

        local _destinationExt=${_destinationPathPSP##*.}

        # la destination a t'il une extention?
        if [[ "$_destinationExt" == "$_destinationPathPSP" ]]
        then # la destination n'a pas d'extention
            _destinationExt="$_sourceExt"
        fi
        displayVarDebug '_destinationExt' "$_destinationExt"

        local _destinationPath="$_destinationRep/$_sourceName-mono.$_destinationExt" #sans ext
        #displayVarDebugping free.fr
         '_destinationPath' "$_destinationPath"

        if [[ -f "$_destinationPath" ]]
        then

            # Le fichier existe: test de la taille
            local -i _tailleFichier=$(stat -c "%s" "$_destinationPath")
            displayVarDebug '_tailleFichier' "$_tailleFichier"
            if [[ $_tailleFichier -eq 0 ]]
            then
                notifsAddEcho "$FUNCNAME($*): Le fichier '$_destinationPath' a une taille nulle -> suppression."
                rm "$_destinationPath";
            else # le fichier existe et a une taille non nulle. Donc le fichier est considéré comme déjà convertit-> on quitte la fonction
                notifsAddEcho "Le fichier mono '$_destinationPath' existe déjà."
                fctOut; return 0;
            fi
        fi

        # - CONVERTIR EN MONO - #
        echo "Lancer 'tail -f $LOG_PATH' sur un autre termninal pour voir l'avancement de la convertion"
        #ffmpeg  -v quiet # ffmpeg -v help
        evalCmd "ffmpeg   -i \"$_sourcePath\" -c:v copy $FFMpegMono \"$_destinationPath\" 1>$LOG_PATH 2>$LOG_PATH";
        local -i _erreur=$?

        if [ $_erreur -eq 0 ]
        then
            notifsAddEcho "$FUNCNAME($*): la convertion vers la mono est bonne"
        else
            erreursAddEcho "$FUNCNAME($*): Problème lors de la convertion vers la mono (ffmpeg: $_erreur)"
        fi

        fctOut; return 0;
    }
#

###########
## MERGE ##
###########
    #function dlyt_merge $referenceVideoYoutube" "$destination(sans extension)" [videoFormat] [audioFormat]
    function dlyt_merge(){
        if [[ $isTrapCAsk -eq 1 ]]; then return $E_CTRL_C ; fi
        fctIn "$*"
        if [[ $# -lt 2 ]]
        then
            erreursAddEcho "$FUNCNAME() n'a pas le bon nombre de parametre($#/2-4). $FUNCNAME(\"$referenceVideoYoutube\" \"$destination\" [videoFormat] [audioFormat])"
            fctOut; return $E_ARG_REQUIRE;
        fi

        local codeYT="$1"
        local file_name="$2" #sans ext
        local -i videoFormat=${3:-399}
        local -i audioFormat=${4:-140}

        setTitle "$file_name";
        setDureeYT "$codeYT"
        local -i _err=$?
        
        dlyt "$codeYT" "$file_name"

        if [[ -f "$titreNormalize.mp4" ]]
        then
            fctOut; return 0;
        fi

        # dl video
        echo '# - MERGE: dl video - #'
            saveFormat $videoFormat
            dlyt "$codeYT" "$titreNormalize-$videoFormat"
            restaureFormat
        # dl audio
        echo '# - MERGE: dl audio - #'
            saveFormat $audioFormat
            dlyt "$codeYT" "$titreNormalize-$audioFormat"
            restaureFormat

        # merge
        echo '# - MERGE: fusion - #'
        #if [[ $isMono -eq 0 ]]
        #then evalCmd "ffmpeg  -i \"$titreNormalize-$videoFormat.mp4\"  -i \"$titreNormalize-$audioFormat.m4a\" -c:v copy  \"$titreNormalize.mp4\"";
        evalCmd "ffmpeg  -i \"$titreNormalize-$videoFormat.mp4\"  -i \"$titreNormalize-$audioFormat.m4a\"$FFMpegMono -c:v copy  \"$titreNormalize.mp4\"";
        #fi
        fctOut; return 0;
    }
#


##############
## SLPITTER ##
##############
    #https://sebastien-lhuillier.com/index.php/ffmpeg/item/148-extraire-une-partie-de-la-video-a-deux-temps-donnes

    #function splitter( 'sourcePath' 'destinationPath' 'debut HH:MM:SS' 'fin HH:MM:SS' ){
    function splitter(){
        if [[ $isTrapCAsk -eq 1 ]]; then return  ; fi
        fctIn "$*"
        if [[ $# -ne 4 ]]
        then
            erreursAddEcho ". $FUNCNAME[$#/4](\"sourcePath\" \"destinationPath(san ext)\" \"debut HH:MM:SS\" \"fin HH:MM:SS\")"
            fctOut; return $E_ARG_REQUIRE;
        fi

        titre4 'Découpage (split)'
        local _sourcePath="$1";
        local _srcExtension=${_sourcePath##*.}
        local _destinationPath="$2";    # sans extension
        local _debut="$3"
        local _fin="$4"
        evalCmd "ffmpeg -i $_sourcePath -ss $_debut -to $_fin -c copy  -q:a 0 -q:v 0 \"$_destinationPath.$_srcExtension\""
        if [[ $? -eq 0 ]]
        then
            notifsAddEcho "Découpage de '$_sourcePath' vers '$_destinationPath.$_srcExtension' OK"
            saveTags "$_destinationPath.$_srcExtension"
        else
            erreursAddEcho "Découpage de '$_sourcePath' vers '$_destinationPath.$_srcExtension' FAIL"
        fi
        fctOut
    }
#


#############
## CONVERT ##
#############
    # bacckup la variable convert
    function saveConvert(){
        fctIn "$*"
        #displayVar 'convert' "$convert"
        convertBak="$convert";
        fctOut
    }

    # supprime la demande de convertion et sauvegarde la valeur precedente.
    # doit etre restauté avec restaureConvert
    function noConvert(){
        fctIn "$*"
        convertBak="$convert";
        convert='';
        fctOut; return 0;
    }

    function restaureConvert(){
        fctIn "$*"
        convert="$convertBak";
        fctOut; return 0;
    }

    function setConvert(){
        fctIn "$*"

        case "$1" in
            "ogg" )
                saveConvert
                convert='ogg';
            ;;
            *)
                saveConvert
                convert='';
            ;;
        esac
        fctOut; return 0;
    }

    #convertir( fichierSource )
    function convertir(){
        if [[ $isTrapCAsk -eq 1 ]];then return $E_CTRL_C;fi
        fctIn "$FUNCNAME($*, convert=$convert)";
        # si pas de convertion demandé alors on quitte normalement
        if [[ "$convert" == '' ]]; then fctOut; return 0;fi 

        titre4 'Convertir'
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho ". $FUNCNAME[$#/2](\"$url\" \"$destination\")"
            fctOut; return $E_ARG_REQUIRE;
        fi

        ((stats['cv_total']++))

        local fileName="$1"

        local notifTxt='';
        local erreurTxt='';

        fileNameSansExt=${fileName%.*};
        local fileNameConvert="$fileNameSansExt.$convert"
        # si le fichier est deja convertit
        if [[ -f "$fileNameConvert" ]]
        then
            notifsAddEcho "$FUNCNAME($*): Le fichier '$fileNameConvert' existe deja."
            ((stats['cv_exist']++))

            if [[ $isSaveTags -eq 1 ]]
            then
            saveTags "$fileNameConvert"
            fi

            fctOut;return 0;
        fi

        # si le fichier a convertir existe pas
        if [[ ! -f "$fileName" ]]
        then
            ((stats['cv_fail']++))
            fctOut;return $E_INODE_NOT_EXIST;
        fi

        #canaux audio
        # https://trac.ffmpeg.org/wiki/AudioChannelManipulation
        # https://superuser.com/questions/702552/ffmpeg-audio-stereo-to-mono-using-only-left-channel
        # ffmpeg -i stereo.flac -ac 1 mono.flac
        # Left channel to mono:    ffmpeg -i video.mp4 -map_channel 0.1.0 -c:v copy mono.mp4
        # Left channel to stereo:  ffmpeg -i video.mp4 -map_channel 0.1.0 -map_channel 0.1.0 -c:v copy stereo.mp4
        # right channel to mono:   ffmpeg -i video.mp4 -map_channel 0.1.1 -c:v copy mono.mp4
        # right channel to stereo: ffmpeg -i video.mp4 -map_channel 0.1.1 -map_channel 0.1.1 -c:v copy stereo.mp4


        # - CONVERTION - #  
        evalCmd "ffmpeg -loglevel $ffmpeg_loglevel -i '$fileName'$FFMpegMono '$fileNameConvert'"
        if [[ $? -eq 0 ]]
        then
            saveTags "$fileNameConvert"
            notifTxt="";
            notifsAddEcho "$FUNCNAME($*): '$fileNameConvert'"
            ((stats['cv_ok']++))
        else
            ereurAddEcho "$FUNCNAME($*): '$fileNameConvert'"
            ((stats['cv_fail']++))
        fi
        fctOut;return 0;
    }
#


##########
# FFMPEG #
##########
    #Extract_musique ( "src_file_path" "dest_filePath" "time_beg(HH:MM:SS.sss)" "time_end(HH:MM:SS.sss)" )
    function ffmpeg_extract_audio(){
        fctIn "$*"
        if [[ $# -lt 3 ]]
        then
            erreursAddEcho "$FUNCNAME[$#/3] ('src_file_path' 'dest_filePath' 'time_beg(HH:MM:SS.sss)' 'time_end(HH:MM:SS.sss)') ($*)";
            fctOut; return $E_ARG_REQUIRE;
        fi

        local src_file_path="$1"
        local dest_filePath="$2"
        local time_beg="$3"
        local time_fin=${4:-''}
        time_fin_Param='';
        if [[ "$time_fin" != '' ]];then time_fin_Param=" -to $time_fin" ;fi
        
        if [[ ! -f "$src_file_path" ]]
        then
            erreursAddEcho "Le fichier source: $src_file_path existe pas."
            fctOut; return 0;
        fi

        if [[ ! -f "$dest_filePath" ]] # et si long = 0
        then
            evalCmd "ffmpeg -i $src_file_path -ss $time_beg$time_fin_Param -c copy -vn $dest_filePath";
            if [[ $? -eq 0 ]]
                then notifsAddEcho  "EXTRACTION de $src_file_path \nvers $dest_filePath: OK";
                else erreursAddEcho "EXTRACTION de $src_file_path \nvers $dest_filePath: FAIL";
                fctOut; return 0;
            fi
        else
            notifsAddEcho "EXTRACTION de $src_file_path \nvers $dest_filePath: déjà faite";
        fi

        #evalCmd "chDroit \"$dest_filePath\""
        fctOut; return 0;
    }


    # ffmpeg_concat ( 'destPath' 'filePath1' .. 'filePathN' )
    function ffmpeg_concat(){
        fctIn "$*"
        if [[ $# -lt 2 ]]
        then
            erreursAddEcho "$FUNCNAME ('dest_filePath' 'filePath1' .. 'filePathN' )";
            fctOut; return $E_ARG_BAD;
        fi

        local dest_filePath="$1"; shift;

        if [[ ! -f "$2" ]]
        then
            erreursAddEcho "Le fichier source: $2 existe pas."
            fctOut; return 0;
        fi

        if [[ ! -f "$dest_filePath" ]] # et si long = 0
        then
            evalCmd "ffmpeg -i "concat:$2|$3"  $dest_filePath";
            if [[ $? -eq 0 ]]
                then notifsAddEcho  "CONCATENATION vers $dest_filePath: OK";
                else erreursAddEcho "CONCATENATION vers $dest_filePath: FAIL"; fctOut; return 0;
            fi
        else
            notifsAddEcho "CONCATENATION vers $dest_filePath: déjà faite";
        fi

        #evalCmd "chDroit \"$dest_filePath\""
        fctOut; return 0;
    }


    # convertFile( 'fichierSansExt' 'extSource' 'extDest' )
    function convertFile(){
        if [[ $isTrapCAsk -eq 1 ]]; then return $E_CTRL_C ; fi
        fctIn "$*"
        if [[ $? -ne 3 ]]
        then
            erreursAddEcho "$FUNCNAME($*): Nombre de parametre insuffisant $#/3."
            fctOut; return $E_ARG_REQUIRE;
        fi
        local fichier="$1"
        local ext1="$2"
        local ext2="$3"
        if [[ ! -f "$fichier" ]]
        then
            erreursAddEcho "$FUNCNAME($*): Le fichier '$fichier' existe pas."
        fctOut; return $E_INODE_NOT_EXIST;

        fi
        evalCmd "ffmpeg -loglevel error -i \"$fichier.$ext1\" \"$fichier.$ext2\""
        fctOut;return 0
    }


    #concat
    #fmpeg -i concat:"partie1.avi|partie2.avi" -c copy film.avi

    #extractToMP4( src.srcExt [dest])
    function extractToMP4(){
        if [[ $isTrapCAsk -eq 1 ]]; then return $E_CTRL_C ; fi
        fctIn "$*"
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "$FUNCNAME($*): Nombre de parametre insuffisant $#/3. $FUNCNAME( src.srcExt [dest])"
            fctOut;return $E_ARG_REQUIRE;
        fi
        local srcPath="$1"
        local srcSExt="${srcPath%.*}"
        local destSExt="${2:-"$srcSExt"}"
        local destExt="mp4"

        displayVar "srcPath" "$srcPath"
        displayVar "srcSExt" "$srcSExt"
        displayVar "destSExt" "$destSExt"
        displayVar "destExt" "$destExt"

        if [[ ! -f "$srcPath" ]]
        then
            erreursAddEcho "$FUNCNAME($*): Le fichier '$srcPath' existe pas."
            fctOut;return E_INODE_NOT_EXIST
        fi
        evalCmd "ffmpeg -i \"$srcPath\" -codec copy -bsf:v mpeg4_unpack_bframes  \"$destSExt.$destExt\""
        fctOut;return 0
    }


    #extractToOGG( src.srcExt [dest])
    function extractToOGG(){
        if [[ $isTrapCAsk -eq 1 ]]; then return $E_CTRL_C ; fi
        fctIn "$*"
        if [[ $# -eq 0 ]]
        then
            echo $WARN"Nombre de parametre insuffisant $#/1"$NORMAL
            echo "usage: extractToOGG( src.srcExt [dest])"
            fctOut;return $E_ARG_REQUIRE
        fi
        local srcPath="$1"
        local srcSExt="${srcPath%.*}"
        local destSExt="${2:-"$srcSExt"}"
        local destExt="ogg"

        displayVar "srcPath" "$srcPath"
        displayVar "srcSExt" "$srcSExt"
        displayVar "destSExt" "$destSExt"
        displayVar "destExt" "$destExt"

        if [[ ! -f "$srcPath" ]]
        then
            erreursAddEcho "$FUNCNAME($*): Le fichier '$srcPath' existe pas."
            fctOut;return $E_INODE_NOT_EXIST;
        fi
        evalCmd "ffmpeg -i \"$srcPath\" -vn -acodec libvorbis  \"$destSExt.$destExt\""
        fctOut;return 0;
    }
#

##########
## TAGS ##
##########
    #id3tool [<options>] <filename>
    #  -t, --set-title=WORD		Sets the title to WORD
    ##  -a, --set-album=WORD		Sets the album to WORD
    #  -r, --set-artist=WORD		Sets the artist to WORD
    #  -y, --set-year=YEAR		Sets the year to YEAR [4 digits]
    #  -n, --set-note=WORD		Sets the note to WORD
    #  -g, --set-genre=INT		Sets the genre code to INT
    # http://id3.org/id3v2-00 #A.3.   Genre List
    #28.Vocal
    #  -G, --set-genre-word=WORD	Sets the genre to WORD
    #  -c, --set-track=INT		Sets the track number to INT
    #  -l, --genre-list		Shows the Genre's and their codes
    #  -v, --version			Displays the version
    #  -h, --help			Displays this message


    function showTags(){
        fctIn "$*";
        echo 'Les tags:';
        displayVar "tag_language" "$tag_language"

        displayVar 'tag_album  ' "$tag_album" 'albumTitreNormalize' "$albumTitreNormalize" 'albumTitreTN' "$albumTitreTN"
        displayVar 'tag_title  ' "$tag_title" 'titreNormalize' "$titreNormalize" 'titreTN' "$titreTN"
        displayVar 'tag_track  ' "$tag_track" 'tag_trackNb  ' "$tag_trackNb"

        displayVar 'tag_artist ' "$tag_artist"   'auteurNormalize'       "$auteurNormalize"          'auteurTN'       "$auteurTN"
        displayVar 'tag_genreNo' "$tag_genreNo" 'tag_genre'             "$tag_genre"

        displayVar 'tag_year   ' "$tag_year"     'dl_date'               "$dl_date"
        displayVar 'tag_url    ' "$tag_url"

        displayVar 'dl_voix    ' "$dl_voix"      'dl_voixNormalize'      "$dl_voixNormalize"         'dl_voixTN'      "$dl_voixTN"
        displayVar 'dl_source  ' "$dl_source"    'dl_sourceNormalize'    "$dl_sourceNormalize"       'dl_sourceTN'    "$dl_sourceTN"
        displayVar 'dl_duree   ' "$dl_duree"

        displayVar 'dl_editeur ' "$dl_editeur"
        displayVar 'dl_prenote ' "$dl_prenote"   'tag_note' "$tag_note" 'dl_postnote' "$dl_postnote"
        fctOut; return 0;
    }

    # Affiche kes tags utilisés par les programmes
    function showTagsProg(){
        fctIn "$*";
        displayVar 'vorbis_tags' "$vorbis_tags"
        displayVar 'vorbis_tag_comment' "$vorbis_tag_comment"
        displayVar 'id3v2_tags' "$id3v2_tags"
        displayVar 'id3v2_tag_comment' "$id3v2_tag_comment"
        displayVar 'ffmpeg_tags' "$ffmpeg_tags"

        displayVar 'coverFrontPathTmpFile' "$coverFrontPathTmpFile"
        displayVar 'coverFFmpegTags' "$coverFFmpegTags"
        displayVar 'ffmpeg_tags' "$ffmpeg_tags"
        displayVar 'tag_note' "$tag_note"
        displayVar 'tag_note_mp3' "$tag_note_mp3"
        displayVarDebug 'tag_comment' "$tag_comment"
        fctOut; return 0;
    }

    # addTags4Progs ('extension' 'tagname' 'value' )
    # ajoute la synatxe dans la varaible des tags du programme selon l'extension fournis
    # fonction non utilisé
    function addTags4FFMpeg(){
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "$FUNCNAME[$#/1](\"extension\")"
            fctOut; return $E_ARG_REQUIRE;
        fi

        local extension="$1";
        #case "$extension"
        #do
        #    'mp3')
        #        case ""
        #        ;;
        #    *)
        #    ereurAddEcho "$FUNCNAME($*): Extension '$extension' non prise en charge"
        #done
        fctOut; return 0;
    }


    # - Calcul les tag - #
    # entrée unique pour calculer les tagsProgs
    # https://wiki.multimedia.cx/index.php?title=FFmpeg_Metadata (les metadatas acceptés par FFMPEG selon le format)
    function setTags(){
        if [[ $isTrapCAsk -eq 1 ]];then return $E_CTRL_C;fi
        fctIn "$*";
        local _extension=${1:-''}
        echo  "Calcul des tags ($_extension)";

        vorbis_tags='';
        id3v2_tags=''
        ffmpeg_tags=' -map 0:0 ';

        if [[ "$tag_artist"   != '' ]]
        then
            vorbis_tags="$vorbis_tags --tag \"ARTIST=$tag_artist\"";
            id3v2_tags="$id3v2_tags --artist \"$tag_artist\"";
            #ffmpeg_tags="$ffmpeg_tags -metadata artist=\"$tag_artist\"";
            case "$extension" in
                'm4a'|'mp4'|'mov'|\
                'asf'|'wma'|'wmv'|'rm'|'nut')
                    ffmpeg_tags="$ffmpeg_tags-metadata author=\"$tag_artist\"";
                    ;;
                'mp3'|'avi')
                    ffmpeg_tags="$ffmpeg_tags-metadata artist=\"$tag_artist\"";
                    ;;
            esac
        fi

        if [[ $tag_year -ne 0 ]]
        then
            vorbis_tags="$vorbis_tags --tag \"DATE=$tag_year\"";
            id3v2_tags="$id3v2_tags --year $tag_year";
            #ffmpeg_tags="$ffmpeg_tags -metadata date=\"$tag_year\"";
            case "$extension" in
                'm4a'|'mp4'|'mov')
                    ffmpeg_tags="$ffmpeg_tags -metadata year=\"$tag_year\"";
                    ;;
                'mp3'|'avi')
                    ffmpeg_tags="$ffmpeg_tags -metadata date=\"$tag_year\"";
                    ;;
            esac
        fi

        if [[ "$tag_album"    != '' ]]
        then
            vorbis_tags="$vorbis_tags --tag \"ALBUM=$tag_album\"";
            id3v2_tags="$id3v2_tags --album \"$tag_album\"";
            ffmpeg_tags="$ffmpeg_tags -metadata album=\"$tag_album\"";
        fi

        if [[ "$tag_title"    != '' ]];
        then
            vorbis_tags="$vorbis_tags --tag \"TITLE=$tag_title\"";
            id3v2_tags="$id3v2_tags --song \"$tag_title\"";
            ffmpeg_tags="$ffmpeg_tags -metadata title=\"$tag_title\"";
        fi

        if [[ "$tag_genre"    != '' ]]
        then
            vorbis_tags="$vorbis_tags --tag \"GENRE=$tag_genre\"";
            id3v2_tags="$id3v2_tags --genre $tag_genreNo";
            ffmpeg_tags="$ffmpeg_tags -metadata genre=\"$tag_genre\"";
        fi

        if [[  $tag_track     -ne 0 ]]
        then
            vorbis_tags="$vorbis_tags --tag \"TRACKNUMBER=$tag_track\"";
            id3v2_tags="$id3v2_tags --track $tag_track/$tag_trackNb"; 
            #ffmpeg_tags="$ffmpeg_tags -metadata track=\"$tag_track/$tag_track\"";
            case "$extension" in
                'm4a'|'mp4'|'mov'|'avi')
                    ffmpeg_tags="$ffmpeg_tags -metadata track=\"$tag_track\"";
                    ffmpeg_tags="$ffmpeg_tags -metadata episode_id=\"$tag_track\"";
                    ;;
                'mp3')
                    ffmpeg_tags="$ffmpeg_tags -metadata track=\"$tag_track/$tag_track\"";
                    ;;
            esac
        fi

        if [[  $tag_trackNb   -ne 0 ]]
        then
            vorbis_tags="$vorbis_tags --tag \"TRACKTOTAL=$tag_trackNb\"";
            ffmpeg_tags="$ffmpeg_tags -metadata tracktotal=\"$tag_trackNb\""; #en ajout
        fi

        if [[ "$tag_language" != '' ]]
        then
            vorbis_tags="$vorbis_tags --tag \"language=$tag_language\"";
            #id3v2_tags="$id3v2_tags --language \"$tag_language\"";      # Pas reconnu par idv3
            ffmpeg_tags="$ffmpeg_tags -metadata language=\"$tag_language\"";
        fi

        # - Commentaire: preparation url - #
        if [[ "$tag_url"      != '' ]]
        then
            local tag_url_mp3="$tag_url"

            vorbis_tags="$vorbis_tags --tag \"url=$tag_url\"";

            tag_url_mp3=$(echo $tag_url_mp3 | tr -d ':');
            tag_url_mp3=$(echo $tag_url_mp3 | tr '/' '-');
            #id3v2_tags="$id3v2_tags --url \"$tag_url_mp3\"";           # Pas reconnu par idv3

            # on ajoute l'url a la note car peu de logiciel lisent le tag url
            tag_note_mp3="$tag_note $tag_url_mp3.";
            tag_note="$tag_note $tag_url.";             # doit etre apres: tag_note_mp3="$tag_note $tag_url_mp3.";
        fi

        # - Commentaire :Finalisation - #

        tag_comment="$dl_prenote$tag_note$dl_postnote"
        if [[ "$tag_comment" != '' ]]
        then
            # VORBIS
            vorbis_tag_comment=''
            #tag_comment="$dl_prenote$tag_note$dl_postnote"             # deja calculer
            vorbis_tag_comment=" --tag \"DESCRIPTION=$tag_comment\"";   # tag comment seul
            vorbis_tags=" $vorbis_tags$vorbis_tag_comment ";            # tous les tags + comment

            # MP3
            id3v2_tag_comment=' '
            tag_comment="$dl_prenote$tag_note_mp3$dl_postnote";
            id3v2_tag_comment=" --comment \"$tag_comment\"";            # tag comment seul
            id3v2_tags="$id3v2_tags$id3v2_tag_comment";                 # tous les tags + comment

            # FFMPEG
            #ffmpeg_tag_comment=" -metadata comment=\"$tag_comment\""
            case "$extension" in
                'm4a'|'mp4'|'mov'|'avi'|\
                'asf'|'wma'|'wmv'|\
                'rm' |'sox')
                    ffmpeg_tags="$ffmpeg_tags -metadata comment=\"$tag_comment\"";
                    ;;
                'mp3')  # non pris officiellement en charge
                    ffmpeg_tags="$ffmpeg_tags -metadata comment=\"$tag_comment\"";
                    ;;
                'mkv')
                    ffmpeg_tags="$ffmpeg_tags -metadata description=\"$tag_comment\"";
                ;;
            esac
        fi

        # - Cover: ajout dans les tags - #
        setCoverFFmpegTags
        if [[ "coverFFmpegTags" != '' ]]
        then
            ffmpeg_tags="$coverFFmpegTags$ffmpeg_tags";
            displayVarDebug $LINENO'ffmpeg_tags' "$ffmpeg_tags"
        fi

        showTagsProg

        fctOut; return 0;
    }

    # - ecrit les tag dans le fichier - #   
    # saveTags
    # $1: fichier a taguer
    function saveTags(){
        if [[ $isTrapCAsk -eq 1 ]];then return $E_CTRL_C;fi
        #echo "$LINENO";return 0; # pour test
        fctInEcho "$*"
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho " $FUNCNAME[$#/1]('Fichier a tagguer') ($*)"
            fctOut; return $E_ARG_REQUIRE;
        fi

        # https://id3.org/id3v2.3.0
        # https://id3.org/id3v2.3.0#Attached_picture
        # https://stackoverflow.com/questions/18710992/how-to-add-album-art-with-ffmpeg
        #   ffmpeg -i in.mp3 -i cover.png -codec copy         -map 0:0 -map 1:0 -id3v2_version 3 -metadata:s:v title="Album cover" -metadata:s:v comment="Cover (front)" out.mp3
        #   ffmpeg -i in.mp3 -i cover.png -c:a copy -c:v copy -map 0:0 -map 1:0 -id3v2_version 3 -metadata:s:v title="Album cover" -metadata:s:v comment="Cover (front)" out.mp3
        # https://superuser.com/questions/1208273/add-new-and-non-defined-metadata-to-a-mp4-file

        titre4 'Sauvegarde des tags'
        local fileName="$1";
        local extension=${fileName##*.}
        local tagVersion='';
        setTags "$extension"
        #showDatas

        case "$extension" in 
            #format valide 
            #vorbiscomment: https://www.xiph.org/vorbis/doc/v-comment.html

            'ogg' | 'ogm' | 'flac' )
                # https://wiki.xiph.org/index.php?title=VorbisComment&mobileaction=toggle_view_desktop#Cover_art
                # https://dogphilosophy.net/?page_id=66

                # - ajout des tags sauf cover - #
                if [[ "$vorbis_tags" != '' ]]
                then
                    displayDebug $LINENO': ajout des tags sauf cover'
                    ls -l "$fileName"
                    evalCmd "vorbiscomment --raw -w $vorbis_tags \"$fileName\"";
                fi

                saveCover "$fileName"
                #getcoverLevel
                #showCovers

                # - Verification - #
                #vorbiscomment --list $fileName
                ;;

            * )
                # https://help.mp3tag.de/main_tags.html (mapping des tags)

                # ecriture du cover (Avant d'ecrite les autres tags)
                #addCoverLocal

                tagVersion=' -id3v2_version 3';

                if [[ "$id3v2_tags" != '' ]]
                then
                    displayVarDebug 'LINENO' "$LINENO"
                    #showDatas
                    # - ajout des tags sauf cover (avec id3v2) - #
                    #evalCmd "id3v2 --delete-all  \"$fileName\"";
                    #evalCmd "id3v2 $id3v2_tags \"$fileName\"";
                    #evalCmd "id3v2 $id3v2_tag_comment \"$fileName\"";

                    # utilisation de ffmpeg plutot que idv3 trop limmité
                    evalCmd "ffmpeg -loglevel $ffmpeg_loglevel -i \"$fileName\" $ffmpeg_tags -codec copy \"$fileName.metadata.$extension\""
                    if [[ $? -eq 0 ]]
                    then
                        displayVarDebug 'LINENO' "$LINENO"
                        evalCmdEchoDebug "mv \"$fileName.metadata.$extension\" \"$fileName\""
                    else
                        erreursAddEcho "$FUNCNAME($*): Erreur lors de la création du fichier temporaire '$fileName.metadata.$extension'" 
                    fi

                    #echo  "Verification des tags";
                    #evalCmd "id3v2 --list \"$fileName\""

                else
                    echo 'Pas de tag de défini.'
                fi
                ;;

            "mp4")
                local tags='';
                if [[ "$tag_artist" != '' ]]; then tags="$tags -metadata Artist=\"$tag_artist\""; fi
                if [[ "$tag_year"   != '' ]]; then tags="$tags -metadata date=\"$tag_year\""; fi
                if [[ "$tag_album"  != '' ]]; then tags="$tags -metadata Album=\"$tag_album\""; fi
                if [[ "$tag_title"  != '' ]]; then tags="$tags -metadata Title=\"$tag_title\""; fi
                #if [[ $tag_genreNo  -ne 0 ]]; then tags="$tags -metadata genre=$tag_genreNo"; fi
                if [[ "$tag_genre"  != '' ]]; then tags="$tags -metadata Genre=\"$tag_genre\""; fi
                if [[ "$tag_track"  -ne 0 ]]; then tags="$tags -metadata Track=$tag_track"; fi
                #if [[  $tag_trackNb   -ne 0 ]]; then tags="$tags -metadata \"TRACKCOUNT=$tag_trackNb\""; fi
                #if [[ "$tag_language" != '' ]]; then tags="$tags -metadata \"language=$tag_language\""; fi
                #/$tag_trackNb"; fi

                local comment="$tag_note"

                # ajout de l'url en fin de tag_note
                if [[ "$tag_url" != '' ]]
                then
                    #displayDebug "$LINENO: ajout de l'url($tag_url)"
                    comment="$tag_comment URL=$tag_url";
                fi

                # filtrage du comment
                if [[ "$tag_note"   != '' ]]
                then
                    displayDebug "$LINENO:Filtrage du commentaire"
                    #comment=$(echo $tag_comment | tr -d ':');
                    #comment=$(echo $tag_comment | tr '/' '-'); #ne fonctionne pas
                fi

                # - comment final - #
                comment="$dl_prenote$tag_comment$dl_postnote"
                if [[ "$tag_comment" != '' ]]
                then
                    displayDebug "$LINENO: création du commentaire finalisé"
                    tags="$tags -metadata Comment=\"$tag_comment\"";
                fi

                evalCmd "ffmpeg -loglevel $ffmpeg_loglevel -i \"$fileName\" $tags -codec copy \"$fileName.metadata.$extension\""
                if [[ -f "$fileName.metadata.$extension" ]]
                then
                    mv "$fileName.metadata.$extension" "$fileName"
                else
                    erreursAddEcho "$FUNCNAME($*) Erreur lors de la convertion du fichier '$fileNameConvert'"
                fi
                ;;


            #format non compatible
            #* )
            #    erreursAddEcho "$FUNCNAME($*): Format $extension non compatible id3";
            #    ;;
        esac;
        fctOut; return 0;
    } #saveTags


    function saveTagsLivreAuto(){
        if [[ $isTrapCAsk -eq 1 ]];then return $E_CTRL_C;fi
        fctIn "$*";
        saveTags "$livreTitreNormalize_Auto"
        fctOut; return 0;
    }
#


################
## PARAMETRES ##
################
TEMP=$(getopt \
    --options v::dVhnS- \
    --long help,version,verbose::,debug,showVars,showCollections,update,conf::,showLibs,showLibsDetail,showDuree\
listeFormats,noDownload,convert::,saveTags,showTags,mono \
    -- "$@")
    eval set -- "$TEMP"

    while true
    do
        if [[ $isTrapCAsk -eq 1 ]];then break;fi
        argument=${1:-''}
        parametre=${2:-''}
        #displayVarDebug 'argument' "$argument" 'parametre' "$parametre"

        case "$argument" in

            # - fonctions generiques - #
            -h|--help)    usage; exit 0;                ;;
            -V|--version) echo "$VERSION"; exit 0;      ;;
            -v|--verbose) ((verbosity++));              ;;
            -d|--debug)   ((isDebug++));isShowVars=1;isShowDuree=1; ;;
            --update)           isUpdate=1;             ;;
            --showVars)         isShowVars=1;           ;;
            --showCollections)  isShowCollections=1;    ;;
            --showLibs )        ((isShowLibs++));       ;;
            --showLibsDetail)   isShowLibs=2;           ;;
            --showDuree )       isShowDuree=1;          ;;
            --conf)     CONF_PSP_PATH="$parametre";     ;;

            -S)
                isSimulation=1;
                ;;

            --) # créer pat getopt; juste ignoré ce parametre
                ;;

            -)  # indicateur des parametres pour les librairies/collections
                ((isPSPSup++));
                ;;

            #--collection)       collection="$parametre"; ;; # surement obsolete

            # - Fonctions specifiques au programme - #
            --listeFormats)     isListeFormats=1;        ;;

            --no-download)      isNoDownload=1;          ;;
            --convert)          setConvert "$2";   shift ;;
            --saveTags)         isSaveTags=1;            ;;
            --showTags)         showTagsPath="$2"; shift ;;
            --mono)             setMono 1;               ;;

            *) #default $argument,$parametre"
                if [[ -z "$argument" ]];then break;fi
                if [[ $isPSPSup -eq 0 ]]
                then
                    librairie="$argument";
                    librairiesCall[$librairiesCallIndex]="$librairie"
                    ((librairiesCallIndex++))
                elif [[ $isPSPSup -eq 1 ]] # est ce un parametre de librairie?
                then
                    addLibParams "$argument";
                fi
                ;;

            esac
        shift 1; #renvoie une valeur non null
        if [[ -z "$parametre" ]];then break;fi
    done
    unset librairie param
#


##########
## MAIN ##
##########

beginStandard
listeFormats
rapportShow;
showTagsShow;

endStandard

showFreedisk '/media/docutheques'
exit 0