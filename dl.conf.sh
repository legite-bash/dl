#!/bin/bash
set -u
# fichier de donfiguration de base de dl.sh

###############################
# - Gestion des repertoires - #
###############################
MEDIATHEQUES_REP='/media/mediatheques'
DOCUTHEQUES_REP='/media/docutheques'
MUSIQUES_REP='$MEDIATHEQUES_REP/Musiques';
MANGAS_REP='$MEDIATHEQUES_REP/Mangas'
SERIES_REP='$MEDIATHEQUES_REP/Series';
collectionRoot="/media/docutheques/Videos/Philosophies/$collection"


##########################
# - variables globales - #
##########################
listeCollections=0;                       # la liste des collections doit etre affichée
listeFormats=0;                           # la liste des formats disponible pour l'url de la video doit etre affiché


#########################################
# - variables locales a la collection - #
#########################################
collection="";                            # nom de la collection appelle

#formatNo=18;                             # numero de format de video (yt,dm)
#formatExt="mp4";                         # texte de l'extention du format
setFormat 18

SITE_URL="https://www.youtube.com/channel/UCqA8H22FwgBVcF3GJpp0MQw"

