#!/bin/bash
set -u
#set -eE  # same as: `set -o errexit -o errtrace`
#style
#https://abs.traduc.org/abs-5.0-fr/ch32.html#unofficialst
#http://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html#The-Set-Builtin

declare -r SCRIPT_PATH=`realpath $0`;        # chemin  absolu complet du script rep + nom
declare -r SCRIPT_REP=`dirname $SCRIPT_PATH`;# repertoire absolu du script (pas de slash de fin)
declare -r VERSION="v3.0.0-2021.07.17";
declare CONF_REP="$HOME/.legite/legite-scripts"; # doit etre défini avant legite-libs.sh

if [ ! -f "$SCRIPT_REP/legite-lib.sh" ]
then
    echo "$SCRIPT_REP/legite-lib.sh introuvable -> quit"
    exit 1
fi
. $SCRIPT_REP/legite-lib.sh


#############
# variables #
#    init   #
#############


#############
# VARIABLES #
# DU PROJET #
#############
# --- variables des includes --- #
declare -r UPDATE_HTTP="$UPDATE_HTTP_ROOT/$SCRIPT_NAME";
declare -i isLegiteLibsDw=0;                # la lib legite-lib est elle deja telechargé?
declare -i isPause=0;                       # L'user doit appuyer sur enter pour continuer

declare -i isReboot=0;
declare -i isEdit=0;
declare -r DNFILE_PATH="/etc/unbound/unbound.conf.d/list.conf";


declare -r DEBIAN_VERSION=$(cat /etc/debian_version)
declare -r DEBIAN_VERSION_MAJOR=${DEBIAN_VERSION%.*}


# #################### #
# FONCTIONS GENERIQUES #
# #################### #
usage (){
    if [ $argNb -eq 0 ]
    then
        echo "$SCRIPT_FILENAME [-d] [--showVars] [--showLibs] [--update] [--conf=fichier.cfg.sh] [programme]";
        echo "--update: update le programme legite-scripts.sh lui meme (via $UPDATE_HTTP)."
        echo "--conf: execute un fichier de configuration si existe. Peut etre appller plusieurs fois pour charger plusieurs fichiers."
        echo "Fichier de configurations:      (visible avec --showVars)."
        echo "programme: execute une fonction (visible avec --showLibs)."
        exit $E_ARG_NONE;
    fi
}


showVarsLocal(){
    showDebug "$FUNCNAME($@)";
}


showVarsPost(){
    showDebug "$FUNCNAME($*)"
    if [ $isShowVars -eq 1 ]
    then
        showVarsLocal
    fi
}


################
# MISE A JOURS #
################
#selfUpdateLocal(){
    # copier le modele de ./legite-lib.sh
#}

#############
# LOAD_CONF #
#############
loadConfsLocal(){
    showDebug "$FUNCNAME($*)"
    execFile "$CONF_REP/legite-push.cfg.sh"
}


# ################### #
# FONCTIONS DU PROJET #
# ################### #
#pushProg ("src" "dest")
pushBashToUpdates(){
    local src="${1:-""}";	local dest="${2:-"$src"}";
    if [ "$src" == "" ]; then return;fi
    evalCmd "scp -p /www/bash/$src pi@192.168.0.202:/www/www-sites/updates/$dest > /dev/null"
    if [ $? -ne 0 ]
    then
        echo "${WARN}Erreur avec le fichier: $dest$NORMAL"
    fi
}

#pushConfToPi ("src1" ["src2"])
pushConfToPi(){
    local src="${1:-''}";
    local dest="${2:-"$src"}";
    if [ "$src" == '' ]; then return;fi
    local srcRep='/home/pascal/.legite'
    local srcPath="$srcRep/$src"
    local destRep='pi@192.168.0.202:/home/pi/.legite'
    local destPath="$destRep/$dest"
    if [ ! -f "$srcPath" ]
    then
        echo $WARN"Le chemin '$srcPath' est inextant."$NORMAL
        return
    fi        
    evalCmd "scp -p '$srcPath' '$destPath' > /dev/null"
    if [ $? -ne 0 ]
    then
        echo $WARN"Erreur avec le fichier: '$srcPath'"$NORMAL
    fi
}

pushHomeToPi(){
    local src="${1:-''}";
    local dest="${2:-"$src"}";
    if [ "$src" == '' ]; then return;fi
    local srcRep='/home/pascal/'
    local srcPath="$srcRep/$src"
    local destRep="pi@192.168.0.202:/home/pi"
    local destPath="$destRep/$dest"
    if [ ! -f "$srcPath" ]
    then
        echo $WARN"Le chemin '$srcPath' est inextant."$NORMAL
        return
    fi        
    evalCmd "scp -p '$srcPath' '$destPath' > /dev/null"
    if [ $? -ne 0 ]
    then
        echo $WARN"Erreur avec le fichier: '$srcPath'"$NORMAL
    fi
}


reboot(){
    if [ $isReboot -eq 1 ];then
        evalCmd "service unbound stop; service unbound start; service unbound status";
    fi
}


createLink(){
    local src="${1:-""}";	local dest="${2:-"$src"}";
    if [ "$src" == "" ]; then return $E_ARG_EMPTY;fi
    #local srcRep=""
    local srcPath="$src"
    #local destRep=""
    local destPath="$dest"

    if [ ! -f "$srcPath" ]
    then
        echo $WARN"La source $srcPath est introuvable."$NORMAL
        return 1
    fi
    if [ -f "$destPath" ]
    then
        echo "$destPath existant."
        return 2
    fi
    ln -s "$srcPath" "$destPath"
    local errNo=$?
    echo -n "ln -s \"$srcPath\" \"$destPath\": "
    if [ $errNo -eq 0 ]
    then
        echo $INFO"ok"$NORMAL
    else
        echo $WARN"fail"$NORMAL
    fi
    return 0
}


createDir(){
    showDebug "$FUNCNAME"
    if [ $argNb -eq 0 ]
    then
        echo $WARN"$FUNCNAME argument requis: $?/1"$NORMAL;
        return $E_ARG_REQUIRE
    fi
    if [ -d "$1" ]
    then
        echo $INFO"Le repertoire '$1' existe déjà. "$NORMAL
    else
        evalCmd "mkdir -p $1"
    fi
    return 0
}



hitEnter(){
    showDebug "$FUNCNAME"
    if [ $isPause -eq 1 ]
    then
        local txt=${1:-"Appuyer sur une Entrée"}
        read -p "$txt"
    fi
    return 0
}

aptitude_install(){
    showDebug "$FUNCNAME($*)"
    if [ $IS_ROOT -eq 0 ];then echo $WARN"$FUNCNAME doit être lancé en root!"$NORMAL;return; fi

    
    while true
    do
        if [ $isTrapCAsk -eq 1 ];then break;fi
        paquet=${1:-""}
        shift 1;
        if [ -z "$paquet" ];then break;fi

        evalCmd "aptitude --assume-yes install $paquet"

        if [ $? -eq 0 ]
        then
            notifsAdd "Installation du Paquet: $paquet OK"
        else
            erreursAdd "Installation du Paquet: $paquet FAIL"
        fi
    done
    return 0
}




##########################
# GESTION DES LIBRAIRIES #
##########################


########
# MAIN #
########
#clear;
echo "${INFO}$SCRIPT_PATH $VERSION$NORMAL";

usage;
selfUpdate;
showVars;
loadConfs

# ########## #
# PARAMETRES #
# ########## #
TEMP=$(getopt \
    --options v::dVhnS \
    --long help,version,verbose::,debug,update,showVars,conf::,showLibs\
,none \
    -- "$@")
eval set -- "$TEMP"

while true
do
    if [ $isTrapCAsk -eq 1 ];then break;fi
    argument=${1:-""}
    parametre=${2:-""}
    #echo "argument:'$argument', parametre='$parametre'"

    case "$argument" in

        # - fonctions generiques - #
        -h|--help) usage; shift ;;
        -V|--version) echo "$VERSION"; exit 0; shift ;;
        -v|--verbose)
            case "$2" in
                "") verbosity=1; shift 2 ;;
                *)  #echo "Option c, argument \`$2'" ;
                verbosity=$2; shift 2;;
            esac
            ;;

        -d|--debug) isDebug=1;isShowVars=1; shift;  ;;
        --showVars) isShowVars=1;           shift  ;;
        --update)   isUpdate=1;             shift ;;
        --conf)
            CONF_PSP_PATH="$parametre";shift 2;
            execFile "$CONF_PSP_PATH"
            ;;
        --showLibs) isShowLibs=1;         shift ;;

        #--) # parcours des arguments supplementaires
        --)
            if [ -z "$parametre" ]
            then
                shift;
                break;
            fi
            #echo "--)$argument,$parametre"
            librairie="$parametre";
            librairiesCall[$librairiesCallIndex]="$librairie"
            ((librairiesCallIndex++))
            shift 2;
            ;;

        *)
            if [ -z "$argument" ]
            then
                shift 1;
                break;
            fi
            #echo "*)$argument,$parametre"
            librairie="$argument";
            librairiesCall[$librairiesCallIndex]="$librairie"
            ((librairiesCallIndex++))
            shift 1;
            ;;

        esac
done



showLibs;
execLibrairiesCall

#echo "Résumé: "
notifsShow;
erreursShow;

showVarsPost
if [ $isTrapCAsk -eq 1 ];then exit $E_CTRL_C;fi
exit 0