#!/bin/bash
set -u

# - Verne - #
echo ""


#SITE_URL="http://www.litteratureaudio.com/livres-audio-gratuits-mp3/tag/jules-verne"

# - references - #
#https://fr.wikipedia.org/wiki/
#http://www.litteratureaudio.com/livres-audio-gratuits-mp3/tag/jules-verne
#http://www.gutenberg.org/wiki/FR_Litt%C3%A9rature_%28Genre%29#Jules_Verne
#http://beq.ebooksgratuits.com/vents/verne.htm



Verne(){
    showDebug "\n$FUNCNAME($*)"
    #http://www.litteratureaudio.com/livres-audio-gratuits-mp3/tag/jules-verne
    createCollection "/media/docutheques/LivreAudios/Auteurs/"
    if [ $? -ne 0 ]; then return 1;fi

    createAuteur "Verne,Jules"
    if [ $? -ne 0 ]; then return 1;fi

    tag_genre="Vocal";
    tag_genreNo=28; #28.Vocal

    tag_prenote="litteratureaudio.com";

    Verne_litteratureaudio_com_chronologie
    #Verne_litteratureaudio_com_Nouvelles
    #Verne_litteratureaudio_com_Cycles
    #Verne_litteratureaudio_com_Divers

    closeAuteur
    return 0
}


###############
# Chronologie #
################
Verne_litteratureaudio_com_chronologie(){
    createRepertoire "Chronologie"
    if [ $? -ne 0 ]; then return 1;fi

    #Verne_1855_Un_hivernage_dans_les_glaces
    #Verne_1863_Cinq_semaines_en_ballon
    #Verne_1863_A_propos_du_Geant
        #Les_Aventures_du_capitaine_Hatteras
    #Verne_1864_Voyage_au_centre_de_la_Terre
    #Verne_1865_Les_Forceurs_de_blocus
    #Verne_1865_De_la_Terre_a_la_Lune
    #Verne_1867_Les_Enfants_du_Capitaine_Grant
    #Verne_1869_Vingt_mille_lieues_sous_les_mers

    #Verne_1870_Autour_de_la_Lune
    #Verne_1871_Une_ville_flottante

        ##Aventures de trois Russes et de trois Anglais dans l'Afrique australe
        ##Le Pays des fourrures
    #Verne_1872_Le_Tour_du_monde_en_80_jours
    #Verne_1873_Les_Meridiens_et_le_calendrier
    #Verne_1874_Une_fantaisie_du_Docteur_Ox
    #Verne_1874_Un_drame_dans_les_airs
    #Verne_1875_Une_ville_ideale
    #Verne_1875_L_Ile_mysterieuse
        #Le Chancellor • Martin Paz
    #Verne_1876_Michel_Strogoff
    Verne_1877_Hector_Servadac
    #Verne_1877_Les_Indes_noires
        ##Un capitaine de quinze ans
    #Verne_1879_Les_Revoltes_de_la_Bounty
    #Verne_1879_Les_Cinq_Cents_Millions_de_la_Begum
    #Verne_1879_Les_Tribulations_d_un_Chinois_en_Chine
    #Verne_1879_La_maison_a_vapeur
        #La Jangada
        # L'École des Robinsons

    #Verne_1881_L_Etoile_du_sud
    #Verne_1882_Le_Rayon_vert
        # Dix Heures en chasse
        # Kéraban-le-Têtu
        #L'Archipel en feu
        #Mathias Sandorf
        #Un billet de loterie
    #Verne_1884_Frritt_Flacc
    #Verne_1885_lepave_du_cynthia
    #Verne_1886_Aventures_de_la_famille_Raton
    #Verne_1886_Robur_le_Conquerant
        #Nord contre Sud
        #Le Chemin de France
    #Verne_1887_Gil_Braltar
    #Verne_1888_Deux_Ans_de_vacances
        #Famille-Sans-Nom
    #Verne_1889_Discours_d_inauguration_du_cirque_municipal_d_Amiens
    #Verne_1889_Sans_dessus_dessous
    #Verne_1873_09_28_Vingt_quatre_minutes_en_ballon

    #Verne_1890_Cesar_Cascabel
        #Mistress Branican
        #Claudius Bombarnac
    #Verne_1891_Ptit_Bonhomme
        #Mirifiques Aventures de maître Antifer
    #Verne_1892_Le_Chateau_des_Carpathes
    #Verne_1895_L_Ile_a_helice
        #Face au drapeau
        #Clovis Dardentor
    #Verne_1897_Le_Sphinx_des_Glaces
        #Le Superbe Orénoque
    #Verne_1899_Le_Testament_d_un_excentriquee
        #Seconde Patrie
        #Le Village aérien
    #Verne_1901_Les_Histoires_de_Jean_Marie_Cabidoulin
        #Les Frères Kip
        #Bourses de voyage
        #Un drame en Livonie
    #Verne_1898_Le_Secret_de_Wilhelm_Storitz

    ##Maitre_du_Monde
        #L'Invasion de la mer
        #Le Phare du bout du monde
        #Le Volcan d'or
        #L'Agence Thompson and Co
    
    #Verne_1901_La_Chasse_au_meteore 
        #Le Pilote du Danube
    Verne_1909_Les_Naufrages_du_Jonathan


    #Verne_1910_Le_Humbug_Moeurs_Americaines
    #Verne_1893_Monsieur_Re_Dieze_et_mademoiselle_Mi_Bemol
    #Verne_1910_La_Destinee_de_Jean_Morenas
    #Verne_1910_La_Journee_d_un_journaliste_americain_en_2890
    #Verne_1910_L_Eternel_Adam
    #Verne_1904_Maitre_du_Monde
    #hier_et_demain
        #L'Étonnante Aventure de la mission Barsac
        #Voyage d'études
  
    #Verne_litteratureaudio_com_Nouvelles

    #  L'Île à hélice • Face au drapeau • Clovis Dardentor
    #  Le Sphinx des glaces  • Le Superbe Orénoque • Le Testament d'un excentrique • Seconde Patrie • Le Village aérien
    #  • Les Histoires de Jean-Marie Cabidoulin • Les Frères Kip • Bourses de voyage • Un drame en Livonie
    #  Maître du monde • L'Invasion de la mer • Le Phare du bout du monde • Le Volcan d'or • L'Agence Thompson and Co
    #  La Chasse au météore • Le Pilote du Danube • Les Naufragés du « Jonathan » • Le Secret de Wilhelm Storitz • Hier et demain • L'Étonnante Aventure de la mission Barsac • Voyage d'études

    closeRepertoire #chronologie
}


#################
# - les livres- #
#################

Verne_1855_Un_hivernage_dans_les_glaces(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                         tag_genre="Nouvelle";
    tag_album="Cinq semaines en ballon";    tag_title="$tag_album";
    tag_year=1855;                          dl_date="$tag_year.03";
    dl_editeur="Musée des familles"
	dl_voix="Bernard";                      dl_source="www.litteratureaudio.com";
    dl_duree="2h25";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-un-hivernage-dans-les-glaces.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    #livreContent
        # https://fr.wikipedia.org/wiki/Un_hivernage_dans_les_glaces
        # https://fr.wikisource.org/wiki/Un_Hivernage_dans_les_glaces
    
        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/Arrivee_Projectile_DTAL.jpg/250px-Arrivee_Projectile_DTAL.jpg"

        # - audios - #
        initChapitre 1 16
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Un_Hivernage_dans_les_glaces_Chap"

        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Le pavillon noir"
        chapitreTitreTbl[2]="Le projet de Jean Cornbutte"
        chapitreTitreTbl[3]="Lueur d’espoir"
        chapitreTitreTbl[4]="Dans les passes"
        chapitreTitreTbl[5]="L’île Liverpool"
        chapitreTitreTbl[6]="Le tremblement de glaces"
        chapitreTitreTbl[7]="Les installations de l’hivernage"
        chapitreTitreTbl[8]="Plan d’explorations"
        chapitreTitreTbl[9]="La maison de neige"

        chapitreTitreTbl[10]="Enterrés vivants"
        chapitreTitreTbl[11]="Un nuage de fumée"
        chapitreTitreTbl[12]="Retour au navire"
        chapitreTitreTbl[13]="Les deux rivaux"
        chapitreTitreTbl[14]="Détresse"
        chapitreTitreTbl[15]="Les ours blancs"
        chapitreTitreTbl[16]="Conclusion"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done

    closeLivre
}

Verne_1863_Cinq_semaines_en_ballon(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                         tag_genre="Roman";
    tag_album="Cinq semaines en ballon";    tag_title="$tag_album";
    tag_year=1863;                          dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                     dl_source="www.litteratureaudio.com";
    dl_duree="9h24";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-cinq-semaines-en-ballon.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Cinq_Semaines_en_ballon
        # https://beq.ebooksgratuits.com/vents/Verne-ballon.pdf

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Cinq_Semaines_en_ballon_001.png/250px-Cinq_Semaines_en_ballon_001.png"

        # - chapitres - #
        initChapitre 1 44
        # pas de titre aux chapitres
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Cinq_semaines_en_ballon_Chap"
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1863_A_propos_du_Geant(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                         tag_genre="Roman";
    tag_album="A propos du Géant";          tag_title="$tag_album";
    tag_year=1863;                          dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                     dl_source="www.litteratureaudio.com";
    dl_duree="0h08";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-a-propos-du-geant.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        #http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-a-propos-du-geant.html

        # - cover - #
        dlCover ""

        # - audios - #
        initChapitre 1 1
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_A_propos_du_geant.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent

    closeLivre
}

Verne_1864_Voyage_au_centre_de_la_Terre(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                           tag_genre="Roman";
    tag_album="Voyage au centre de la Terre"; tag_title="$tag_album";
    tag_year=1864;                            dl_date="$tag_year.11.24";
    dl_editeur="Hetzel"
	dl_voix="Damien Genevois";                dl_source="www.litteratureaudio.com";
    dl_duree="6h44";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/jules-verne-voyage-au-centre-de-la-terre.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Voyage_au_centre_de_la_Terre

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/%27Journey_to_the_Center_of_the_Earth%27_by_%C3%89douard_Riou_38.jpg/500px-%27Journey_to_the_Center_of_the_Earth%27_by_%C3%89douard_Riou_38.jpg"

        # - audios - #
        initChapitre 1 45
        local episodeUrl="http://www.litteratureaudio.net/mp3/Jules_Verne_-_Voyage_au_centre_de_la_Terre_Chap"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1865_De_la_Terre_a_la_Lune(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="De_la_Terre_a_la_Lune";          tag_title="$tag_album";
    tag_year=1865;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="6h05";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-de-la-terre-a-la-lune.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi
    
    livreContent(){
        # https://fr.wikipedia.org/wiki/De_la_Terre_%C3%A0_la_Lune
    
        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/Arrivee_Projectile_DTAL.jpg/250px-Arrivee_Projectile_DTAL.jpg"

        # - audios - #
        initChapitre 0 28 # ***** 1er chapitre=0 *****
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_De_la_Terre_a_la_Lune_Chap"

        local -a chapitreTitreTbl;
        chapitreTitreTbl[0]="preliminaire"
        chapitreTitreTbl[1]="Le_gun_club"
        chapitreTitreTbl[2]="Communication_du_president_Barbican"
        chapitreTitreTbl[3]="Effet_de_la_communication_Barbican"
        chapitreTitreTbl[4]="Réponse_de_l_observatoire_de_Cambridge"
        chapitreTitreTbl[5]="Le_roman_de_la_Lune"
        chapitreTitreTbl[6]="Ce_qui_n_est_pas_possible_d_ignorer_et_ce_qui_n_est_plus_permis_de_croire_dans_les_états-unis"
        chapitreTitreTbl[7]="L_hymne_du_boulet"
        chapitreTitreTbl[8]="L_histoire_du_canon"
        chapitreTitreTbl[9]="La_question_des_poudres"

        chapitreTitreTbl[10]="Un_ennemi_sur_25_millions_d_années"
        chapitreTitreTbl[11]="Floride_et_Texas"
        chapitreTitreTbl[12]="Orbis_et_Torbis"
        chapitreTitreTbl[13]="Stones_hill"
        chapitreTitreTbl[14]="Pioche_et_truelle"
        chapitreTitreTbl[15]="La_fete_de_la_fonte"
        chapitreTitreTbl[16]="La_Colombiade"
        chapitreTitreTbl[17]="Une_depeche_telegraphique"
        chapitreTitreTbl[18]="Le_passager_de_l_Atlanta"
        chapitreTitreTbl[19]="Un_meeting"

        chapitreTitreTbl[20]="Attaque_et_Riposte"
        chapitreTitreTbl[21]="Comment_un_francais_arrange_une_affaire"
        chapitreTitreTbl[22]="Le_nouveau_citoyen_des_etats"
        chapitreTitreTbl[23]="Le_wagon_projectile"
        chapitreTitreTbl[24]="Le_telecsope_des_montagnes_rocheuses"
        chapitreTitreTbl[25]="derniers_details"
        chapitreTitreTbl[26]="Feu"
        chapitreTitreTbl[27]="Temps_couvert"
        chapitreTitreTbl[28]="Un_nouvel_astre"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
        return 
    }
    livreContent
    closeLivre
}

Verne_1867_Les_Enfants_du_Capitaine_Grant(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Les enfants du Capitaine Grant"; tag_title="$tag_album";
    tag_year=1867;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="23h27";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-les-enfants-du-capitaine-grant.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Voyage_au_centre_de_la_Terre
        # https://www.ebooksgratuits.com/pdf/verne_enfants_capitaine_grant.pdf 

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/%27Journey_to_the_Center_of_the_Earth%27_by_%C3%89douard_Riou_38.jpg/250px-%27Journey_to_the_Center_of_the_Earth%27_by_%C3%89douard_Riou_38.jpg"

        # - audios - #
        partieNb=2;

        # - partie 1- #
        partieNo=1;
        initChapitre 1 26
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_enfants_du_Capitaine_Grant_Partie${partieNo}_Chap"
    
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Balance-fish"
        chapitreTitreTbl[2]="Les trois documents"
        chapitreTitreTbl[3]="Malcolm-Castle"
        chapitreTitreTbl[4]="Une proposition de lady Glenarvan"
        chapitreTitreTbl[5]="Le départ du « Duncan »"
        chapitreTitreTbl[6]="Le passager de la cabine numéro six"
        chapitreTitreTbl[7]="D’où vient et où va Jacques Paganel"
        chapitreTitreTbl[8]="Un brave homme de plus à bord du « Duncan »"
        chapitreTitreTbl[9]="Le détroit de Magellan"

        chapitreTitreTbl[10]="Le trente-septième parallèle"
        chapitreTitreTbl[11]="Traversée du Chili"
        chapitreTitreTbl[12]="À douze mille pieds dans les airs"
        chapitreTitreTbl[13]="Descente de la cordillère"
        chapitreTitreTbl[14]="Le coup de fusil de la providence"
        chapitreTitreTbl[15]="L’espagnol de Jacques Paganel"
        chapitreTitreTbl[16]="Le rio-Colorado"
        chapitreTitreTbl[17]="Les pampas"
        chapitreTitreTbl[18]="À la recherche d’une aiguade"
        chapitreTitreTbl[19]="Les loups rouges"

        chapitreTitreTbl[20]="Les plaines argentines"
        chapitreTitreTbl[21]="Le fort Indépendance"
        chapitreTitreTbl[22]="La crue"
        chapitreTitreTbl[23]="Où l’on mène la vie des oiseaux"
        chapitreTitreTbl[24]="Où l’on continue de mener la vie des oiseaux"
        chapitreTitreTbl[25]="Entre le feu et l’eau"
        chapitreTitreTbl[26]="L'Atlantique"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

        # - partie 2 - #
        partieNo=2;
        initChapitre 1 22
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_enfants_du_Capitaine_Grant_Partie${partieNo}_Chap"

        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Le retour à bord"
        chapitreTitreTbl[2]="Tristan d’Acunha"
        chapitreTitreTbl[3]="L’île Amsterdam"
        chapitreTitreTbl[4]="Les paris de Jacques Paganel et du major Mac Nabbs"
        chapitreTitreTbl[5]="Les colères de l’océan Indien"
        chapitreTitreTbl[6]="Le cap Bernouilli"
        chapitreTitreTbl[7]="Ayrton"
        chapitreTitreTbl[8]="Le départ"
        chapitreTitreTbl[9]="La province de Victoria"

        chapitreTitreTbl[10]="Wimerra river"
        chapitreTitreTbl[11]="Burke et Stuart"
        chapitreTitreTbl[12]="Le railway de Melbourne à Sandhurst"
        chapitreTitreTbl[13]="Un premier prix de géographie"
        chapitreTitreTbl[14]="Les mines du mont Alexandre"
        chapitreTitreTbl[15]="« Australian and New Zealand gazette »"
        chapitreTitreTbl[16]="Où le major soutient que ce sont des singes"
        chapitreTitreTbl[17]="Les éleveurs millionnaires"
        chapitreTitreTbl[18]="Les alpes australiennes"
        chapitreTitreTbl[19]="Un coup de théâtre"

        chapitreTitreTbl[20]="Aland ! Zealand !"
        chapitreTitreTbl[21]="Quatre jours d’angoisse"
        chapitreTitreTbl[22]="Eden"


        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

        # - partie 3 - #
        partieNo=3;
        initChapitre 1 22
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_enfants_du_Capitaine_Grant_Partie${partieNo}_Chap"

        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Le macquarie"
        chapitreTitreTbl[2]="Le passé du pays où l’on va"
        chapitreTitreTbl[3]="Les massacres de la Nouvelle-Zélande"
        chapitreTitreTbl[4]="Les brisants"
        chapitreTitreTbl[5]="Les matelots improvisés"
        chapitreTitreTbl[6]="Où le cannibalisme est traité théoriquement"
        chapitreTitreTbl[7]="Où l’on accoste enfin une terre qu’il faudrait éviter"
        chapitreTitreTbl[8]="Le présent du pays où l’on est"
        chapitreTitreTbl[9]="Trente milles au nord"

        chapitreTitreTbl[10]="Le fleuve national"
        chapitreTitreTbl[11]="Le lac Taupo"
        chapitreTitreTbl[12]="Les funérailles d’un chef maori"
        chapitreTitreTbl[13]="Les dernières heures"
        chapitreTitreTbl[14]="La montagne tabou"
        chapitreTitreTbl[15]="Les grands moyens de Paganel"
        chapitreTitreTbl[16]="Entre deux feux"
        chapitreTitreTbl[17]="Pourquoi le « Duncan » croisait sur la côte est de la Nouvelle-Zélande"
        chapitreTitreTbl[18]="Ayrton ou Ben Joyce "
        chapitreTitreTbl[19]="Une transaction"

        chapitreTitreTbl[20]="Un cri dans la nuit"
        chapitreTitreTbl[21]="L’île Tabor"
        chapitreTitreTbl[22]="La dernière distraction de Jacques Paganel"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1869_Vingt_mille_lieues_sous_les_mers(){
	showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                 tag_genre="Roman";
    tag_album="Vingt mille lieues sous les mers";   tag_title="$tag_album";
    tag_year=1869;                                  dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Damien Genevois";                      dl_source="www.litteratureaudio.com";
    dl_duree="15h02";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-vingt-mille-lieues-sous-les-mers.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Vingt_Mille_Lieues_sous_les_mers

        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/170x220xJules_Verne_-_20000_lieues_sous_les_mers.jpg.pagespeed.ic.FmbAhzQhrV.jpg"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/1/10/Houghton_FC8_V5946_869ve_-_Verne%2C_frontispiece.jpg/250px-Houghton_FC8_V5946_869ve_-_Verne%2C_frontispiece.jpg" "$livreTitreNormalize-wiki"

        # - audios - #
        partieNb=2

        # - Partie 1 - #
        partieNo=1;
        initChapitre 1 24
        local episodeUrl="http://www.litteratureaudio.net/mp3/Jules_Verne_-_20000_lieues_sous_les_mers_L${partieNo}_Chap"
                         #http://www.litteratureaudio.net/mp3/Jules_Verne_-_20000_lieues_sous_les_mers_L1_Chap01.mp3 

        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Un_écueil_fuyant"
        chapitreTitreTbl[2]="Le_pour_et_le_contre"
        chapitreTitreTbl[3]="Comme_il_plaira_à_monsieur"
        chapitreTitreTbl[4]="Ned_Land"
        chapitreTitreTbl[5]="À_l_aventure"
        chapitreTitreTbl[6]="À_toute_vapeur"
        chapitreTitreTbl[7]="Une_baleine_d_espèce_inconnue"
        chapitreTitreTbl[8]="Mobilis_in_mobile"
        chapitreTitreTbl[9]="Les_colères_de_Ned_Land"

        chapitreTitreTbl[10]="L_homme_des_eaux"
        chapitreTitreTbl[11]="Le_Nautilus"
        chapitreTitreTbl[12]="Tout_par_l_électricité"
        chapitreTitreTbl[13]="Quelques_chiffres"
        chapitreTitreTbl[14]="Le_Fleuve Noir"
        chapitreTitreTbl[15]="Une_invitation_par_lettre"
        chapitreTitreTbl[16]="Promenade_en_plaine"
        chapitreTitreTbl[17]="Une_forêt_sous-marine"
        chapitreTitreTbl[18]="Quatre_milles_lieues_sous_le_Pacifique"
        chapitreTitreTbl[19]="Vanikoro"

        chapitreTitreTbl[20]="Le_détroit_de_Torrès"
        chapitreTitreTbl[21]="Quelques_jours_à_terre"
        chapitreTitreTbl[22]="La_foudre_du_capitaine_Nemo"
        chapitreTitreTbl[23]="agri_somnia"
        chapitreTitreTbl[24]="Le_royaume_du_corail"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done


        # - Partie 2 - #
        partieNo=2;
        initChapitre 1 23

        local episodeUrl="http://www.litteratureaudio.net/mp3/Jules_Verne_-_20000_lieues_sous_les_mers_L${partieNo}_Chap"
                          #http://www.litteratureaudio.net/mp3/Jules_Verne_-_20000_lieues_sous_les_mers_L1_Chap01.mp3

        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="L_océan_Indien"
        chapitreTitreTbl[2]="Une_nouvelle_proposition_du_capitaine_Nemo"
        chapitreTitreTbl[3]="Une_perle_de_dix_millions"
        chapitreTitreTbl[4]="La_mer_Rouge"
        chapitreTitreTbl[5]="Arabian-Tunnel"
        chapitreTitreTbl[6]="L_Archipel_grec"
        chapitreTitreTbl[7]="La_Méditerranée_en_quarante-huit_heures"
        chapitreTitreTbl[8]="La_baie_de_Vigo"
        chapitreTitreTbl[9]="Un_continent_disparu"

        chapitreTitreTbl[10]="Les_houillères_sous-marines"
        chapitreTitreTbl[11]="La_mer_de_Sargasses"
        chapitreTitreTbl[12]="Cachalots_et_baleines"
        chapitreTitreTbl[13]="La_banquise"
        chapitreTitreTbl[14]="Le_pôle_Sud"
        chapitreTitreTbl[15]="Accident_ou_incident"
        chapitreTitreTbl[16]="Faute_d_air"
        chapitreTitreTbl[17]="Du_cap_Horn_à_l_Amazone"
        chapitreTitreTbl[18]="Les_poulpes"
        chapitreTitreTbl[19]="Le_Gulf-Stream"

        chapitreTitreTbl[20]="Par_47°24′_de_latitude_et_de_17°28′_de_longitude"
        chapitreTitreTbl[21]="Une_hécatombe"
        chapitreTitreTbl[22]="Les_dernières_paroles_du_capitaine_Nemo"
        chapitreTitreTbl[23]="Conclusion"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done
        }
    livreContent
    closeLivre
}

Verne_1870_Autour_de_la_Lune(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Autour de la Lune";              tag_title="$tag_album";
    tag_year=1870;                              dl_date="$tag_year.11.04";
    dl_editeur="Journal des débats"
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="23h27";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-autour-de-la-lune.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Autour_de_la_Lune
    
        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/%27Around_the_Moon%27_by_Bayard_and_Neuville_01.jpg/250px-%27Around_the_Moon%27_by_Bayard_and_Neuville_01.jpg"

        # - audios - #
        initChapitre 0 23 # 1er chapitre=0
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Autour_de_la_Lune_Chap"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1871_Une_ville_flottante(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Une ville flottante";            tag_title="$tag_album";
    tag_year=1871;                              dl_date="$tag_year.08.09";
    dl_editeur=" Journal des Débats"
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="4h48";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-une-ville-flottante.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Une_ville_flottante

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Verne_-_Une_ville_flottante%2C_1872.djvu/page1-250px-Verne_-_Une_ville_flottante%2C_1872.djvu.jpg"

        # - audios - #
        initChapitre 1 39
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Une_ville_flottante_Chap"
    
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1865_Les_Forceurs_de_blocus(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Les Forceurs de blocus";         tag_title="$tag_album";
    tag_year=1865;                              dl_date="$tag_year.10";
    dl_editeur="Musée des familles"
	dl_voix="Bernard";                          dl_source="www.litteratureaudio.com";
    dl_duree="2h09";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-les-forceurs-de-blocus.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Les_Forceurs_de_blocus

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Verne-Blok%C3%A1da.jpg/250px-Verne-Blok%C3%A1da.jpg"

        # - audios - #
        initChapitre 1 10
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_Forceurs_de_Blocus_Chap"
    
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1872_Le_Tour_du_monde_en_80_jours(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Le Tour du monde en 80 jours";           tag_title="$tag_album";
    tag_year=1872;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Damien Genevois";                          dl_source="www.litteratureaudio.com";
    dl_duree="6h43";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/jules-verne-le-tour-du-monde-en-80-jours.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Le_Tour_du_monde_en_quatre-vingts_jours

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Verne_Tour_du_Monde.jpg/250px-Verne_Tour_du_Monde.jpg"
        
        # - audios - #
        initChapitre 1 37
        local episodeUrl="http://www.litteratureaudio.net/mp3/Jules_Verne_-_Le_Tour_du_monde_en_80_jours_Chap"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1873_Les_Meridiens_et_le_calendrier(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="étude";
    tag_album="Les Méridiens et le calendrier "; tag_title="$tag_album";
    tag_year=1873;                              dl_date="$tag_year.040.04";
    dl_editeur="journal officiel"
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="0h12";
    tag_url="https://fr.wikipedia.org/wiki/Les_M%C3%A9ridiens_et_le_Calendrier"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Les_M%C3%A9ridiens_et_le_Calendrier
    
        # - cover - #
        #dlCover "" # pas de cover

        # - audios - #
        tag_postnote="À la suite de la publication du Tour du monde en quatre-vingts jours, la Société de géographie invite Jules Verne, lors de sa séance du 4 avril 1873, à faire une conférence"
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_les_meridiens_et_le_calendrier.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

Verne_1874_Une_fantaisie_du_Docteur_Ox(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Une fantaisie du Docteur Ox";    tag_title="$tag_album";
    tag_year=1874;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="ThéoTeX";                          dl_source="www.litteratureaudio.com";
    dl_duree="2h26";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-une-fantaisie-du-docteur-ox.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Une_fantaisie_du_docteur_Ox
    
        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/xbeffroi_de_quiquendone.jpg.pagespeed.ic.LG9rxyUjJ7.webp"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Verne-Ox.jpg/250px-Verne-Ox.jpg"

        # - audios - #
        initChapitre 1 17
        addChapitre ""  1; dl_postnote="Chapitres 1 2 3."
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_Une_fantaisie_du_docteur_Ox_01_ch_1_3.mp3" "$livreTitreNormalize-chapitreNo"
        addChapitre ""  4; dl_postnote="Chapitres 4 5 6."
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_Une_fantaisie_du_docteur_Ox_02_ch_4_6.mp3" "$livreTitreNormalize-chapitreNo"
        addChapitre ""  7; dl_postnote="Chapitres 7 8 9."
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_Une_fantaisie_du_docteur_Ox_03_ch_7_9.mp3" "$livreTitreNormalize-chapitreNo"
        addChapitre "" 10; dl_postnote="Chapitres 10 11 12."
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_Une_fantaisie_du_docteur_Ox_04_ch_10_12.mp3" "$livreTitreNormalize-chapitreNo"
        addChapitre "" 13; dl_postnote="Chapitre 13 14 15 16 17."
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_Une_fantaisie_du_docteur_Ox_05_ch_13_17.mp3" "$livreTitreNormalize-chapitreNo"
    }
    livreContent
    closeLivre
}

Verne_1874_Un_drame_dans_les_airs(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Vulgarisation scientifique";
    tag_album="Un drame dans les airs";         tag_title="$tag_album";
    tag_year=1874;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Cécile_S";                         dl_source="www.litteratureaudio.com";
    dl_duree="0h48";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-un-drame-dans-les-airs.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Un_drame_dans_les_airs
        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/xN0069016_JPEG_163_163DM.jpeg.jpg.pagespeed.ic.vmAuduhZgZ.webp"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/%27A_Drama_in_the_Air%27_by_%C3%89mile_Bayard_6.jpg/250px-%27A_Drama_in_the_Air%27_by_%C3%89mile_Bayard_6.jpg"

        # - audios - #
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_Un_Drame_dans_les_airs.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

Verne_1875_Une_ville_ideale(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Une ville ideale";               tag_title="$tag_album";
    tag_year=1875;                              dl_date="$tag_year.12.12";
    dl_editeur="Journal d'Amiens "
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="0h45";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-une-ville-ideale.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Une_ville_id%C3%A9ale
        # - cover - #
        dlCover ""

        # - audios - #
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_Une_ville_ideale.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

Verne_1875_L_Ile_mysterieuse(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="L'île mysterieuse";              tag_title="$tag_album";
    tag_year=1875;                              dl_date="$tag_year.01.01";
    dl_editeur="Magasin d’éducation et de récréation"
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="23h30";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-lile-mysterieuse.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/L%27%C3%8Ele_myst%C3%A9rieuse

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Ile_Mysterieuse_01.jpg/250px-Ile_Mysterieuse_01.jpg"

        # - audios - #
        partieNb=3

        # - partie 1- #
        partieNo=1
        touch "$livreTitreNormalizePartieChapitre-Les Naufragés de l'air"
        initChapitre 1 22
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_L_ile_mysterieuse_Partie${partieNo}_Chap"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

        # - partie 2 - #
        partieNo=2
        touch "$livreTitreNormalizePartieChapitre-L'abandonné"
        initChapitre 1 20
        touch "$livreTitreNormalize-P$partieNo-0-L_Abandonne.hr"
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_L_ile_mysterieuse_Partie${partieNo}_Chap"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

        # - partie 3 - #
        partieNo=3
        touch "$livreTitreNormalizePartieChapitre-le secret de l'île"
        initChapitre 1 20
        touch "$livreTitreNormalize-P$partieNo-0-Le_secret_de_l_Ile.hr"
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_L_ile_mysterieuse_Partie${partieNo}_Chap"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1876_Michel_Strogoff(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Michel Strogoff";                tag_title="$tag_album";
    tag_year=1876;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Damien Genevois";                  dl_source="www.litteratureaudio.com";
    dl_duree="11h47";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-michel-strogoff.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Michel_Strogoff
        # https://ar.21-bal.com/pravo/19170/index.html

        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/xJules_Verne_-_Michel_Strogoff_illustration.jpg.pagespeed.ic.bN2RoknMiw.jpg"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/%27Michael_Strogoff%27_by_Jules_F%C3%A9rat_01.jpg/250px-%27Michael_Strogoff%27_by_Jules_F%C3%A9rat_01.jpg" "$livreTitreNormalize-wiki"

        # - audios - #
        partieNb=2


        # - Partie 1 - #
        partieNo=1;
        initChapitre 1 17
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Michel_Strogoff_P1_Chap"

        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Une fête au Palais-Neuf"
        chapitreTitreTbl[2]="Russe et Tartares"
        chapitreTitreTbl[3]="Michel Strogoff"
        chapitreTitreTbl[4]="De Moscou à Nijni-Novgorod"
        chapitreTitreTbl[5]="Un arrêté en deux articless"
        chapitreTitreTbl[6]="Frère et soeur"
        chapitreTitreTbl[7]="En descendant le Volga"
        chapitreTitreTbl[8]="En remontant la Kama"
        chapitreTitreTbl[9]="En tarentass nuit et jour"

        chapitreTitreTbl[10]="Un orage dans les monts Ourals"
        chapitreTitreTbl[11]="Voyageurs en détresse"
        chapitreTitreTbl[12]="Une_provocation"
        chapitreTitreTbl[13]="Au-dessus de tout, le devoir"
        chapitreTitreTbl[14]="Mère et fils"
        chapitreTitreTbl[15]="Les marais de la Baraba"
        chapitreTitreTbl[16]="Un_dernier_effort"
        chapitreTitreTbl[17]="Versets et chansons"

        #touch "$livreTitreNormalize-partie$partieNo.hr"
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

        # - Partie 2 - #
        partieNo=2;
        initChapitre 1 15
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Michel_Strogoff_P2_Chap"

        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Un camp tartare"
        chapitreTitreTbl[2]="Une attitude d’Alcide Jolivet"
        chapitreTitreTbl[3]="Coup pour coup"
        chapitreTitreTbl[4]="L’entrée triomphale"
        chapitreTitreTbl[5]="« Regarde de tous tes yeux, regarde ! »"
        chapitreTitreTbl[6]="Un ami de grande route"
        chapitreTitreTbl[7]="Le passage de l’Yeniseï"
        chapitreTitreTbl[8]="Un lièvre qui traverse la route"
        chapitreTitreTbl[9]="Dans la steppe"

        chapitreTitreTbl[10]="Baïkal et Angara"
        chapitreTitreTbl[11]="Entre deux rives"
        chapitreTitreTbl[12]="Irkoutsk"
        chapitreTitreTbl[13]="Un courrier du czar"
        chapitreTitreTbl[14]="La nuit du 5 au 6 octobre"
        chapitreTitreTbl[15]="Conclusion"


        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

        # - partie2 - #
        #touch "$livreTitreNormalize-partie$partieNo.hr"
        local episodeUrl=""
        #dlwget "${episodeUrl}01.mp3 $livreTitreNormalize-partie2-chapitre-01-15-Un_camp_Tatare.mp3"
        #dlwget "${episodeUrl}02.mp3 $livreTitreNormalize-partie2-chapitre-02-15-Une_attitude_d_Alcide_Jolivet.mp3"
        #dlwget "${episodeUrl}03.mp3 $livreTitreNormalize-partie2-chapitre-03-15-Coups_pour_coups.mp3"
        #dlwget "${episodeUrl}04.mp3 $livreTitreNormalize-partie2-chapitre-04-15-L_entree_triomphale.mp3"
        #dlwget "${episodeUrl}05.mp3 $livreTitreNormalize-partie2-chapitre-05-15-Regarde_de_tout_tes_yeux.mp3"
        #dlwget "${episodeUrl}06.mp3 $livreTitreNormalize-partie2-chapitre-06-15-Un_ami_de_grande_route.mp3"
        #dlwget "${episodeUrl}07.mp3 $livreTitreNormalize-partie2-chapitre-07-15-Le_passage_de_.mp3"
        #dlwget "${episodeUrl}08.mp3 $livreTitreNormalize-partie2-chapitre-08-15-Un_lievre_qui_traverse_la_route.mp3"
        #dlwget "${episodeUrl}09.mp3 $livreTitreNormalize-partie2-chapitre-09-15-Dans_la_steppe.mp3"
        #dlwget "${episodeUrl}10.mp3 $livreTitreNormalize-partie2-chapitre-10-15-Baikal_et_Angara.mp3"
        #dlwget "${episodeUrl}11.mp3 $livreTitreNormalize-partie2-chapitre-11-15-Entre_deux_rives.mp3"
        #dlwget "${episodeUrl}12.mp3 $livreTitreNormalize-partie2-chapitre-12-15-Irkousk.mp3"
        #dlwget "${episodeUrl}13.mp3 $livreTitreNormalize-partie2-chapitre-13-15-Un_courrier_du_Tsar.mp3"
        #dlwget "${episodeUrl}14.mp3 $livreTitreNormalize-partie2-chapitre-14-15-La_nuit_du_5_au_6_octobre.mp3"
        #dlwget "${episodeUrl}15.mp3 $livreTitreNormalize-partie2-chapitre-15-15-Conclusion.mp3"
    }
    livreContent
    closeLivre
}

Verne_1877_Hector_Servadac(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Hector Servadac";                tag_title="$tag_album";
    tag_year=1877;                              dl_date="$tag_year.01.01";
    dl_editeur="Magasin d'éducation et de récréation"
	dl_voix="Vincent de l'Épine";               dl_source="www.litteratureaudio.com";
    dl_duree="14h06";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-hector-servadac.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Hector_Servadac
        # https://www.atramenta.net/lire/oeuvre28613-chapitre-1.html (titres des chapitres et texte)
    
        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/xHector_Servadac.jpg.pagespeed.ic.nTIeYN15qT.jpg"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/%27Off_on_a_Comet%27_by_Paul_Philippoteaux_002.jpg/250px-%27Off_on_a_Comet%27_by_Paul_Philippoteaux_002.jpg" "$livreTitreNormalize-wiki"

        # - audios - #
        partieNb=2

        # partie 1 #
        partieNo=1
        initChapitre 1 24
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_Hector_Servadac_part0${partieNo}_chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Le comte : « Voici ma carte. » Le capitaine : « Voici la mienne. »"
        chapitreTitreTbl[2]="Dans lequel on photographie physiquement et moralement le capitaine Servadac et son ordonnance Ben-Zouf"
        chapitreTitreTbl[3]="Où l’on verra que l’inspiration poétique du capitaine Servadac est interrompue par un choc malencontreux"
        chapitreTitreTbl[4]="Qui permet au lecteur de multiplier à l’infini les points d’exclamation et d’interrogation !"
        chapitreTitreTbl[5]="Dans lequel il est parlé de quelques modifications apportées à l’ordre physique, sans qu’on puisse en indiquer la cause"
        chapitreTitreTbl[6]="Qui engage le lecteur à suivre le capitaine Servadac pendant sa première excursion sur son nouveau domaine"
        chapitreTitreTbl[7]="Dans lequel Ben-Zouf croit devoir se plaindre de la négligence du gouverneur général à son égard"
        chapitreTitreTbl[8]="Où il est question de Vénus et de Mercure, qui menacent de devenir des planètes d’achoppement"
        chapitreTitreTbl[9]="Dans lequel le capitaine Servadac pose une série de demandes qui restent sans réponses"

        chapitreTitreTbl[10]="Où, la lunette aux yeux, la sonde à la main, on cherche à retrouver quelques vestiges de la province d’Alger"
        chapitreTitreTbl[11]="Où le capitaine Servadac retrouve, épargné par la catastrophe, un îlot qui n’est qu’une tombe"
        chapitreTitreTbl[12]="Dans lequel, après avoir agi en marin, le lieutenant Procope s’en remet à la volonté de Dieu"
        chapitreTitreTbl[13]="Où il est question du brigadier Murphy, du major Oliphant, du caporal Pim, et d’un projectile qui se perd au-delà de l’horizon"
        chapitreTitreTbl[14]="Qui montre une certaine tension dans les relations internationales et aboutit à une déconvenue géographique"
        chapitreTitreTbl[15]="Dans lequel on discute pour arriver à découvrir une vérité dont on s’approche peut-être"
        chapitreTitreTbl[16]="Dans lequel on verra le capitaine Servadac tenir dans sa main tout ce qui reste d’un vaste continent"
        chapitreTitreTbl[17]="Qui pourrait sans inconvénient être très justement intitulé : du même aux mêmes"
        chapitreTitreTbl[18]="Qui traite de l’accueil fait au gouverneur général de l’île Gourbi et des événements qui se sont accomplis pendant son absence"
        chapitreTitreTbl[19]="Dans lequel le capitaine Servadac est reconnu gouverneur général de Gallia à l’unanimité des voix, y compris la sienne"

        chapitreTitreTbl[20]="Qui tend à prouver qu’en regardant bien, on finit toujours par apercevoir un feu à l’horizon"
        chapitreTitreTbl[21]="Où l’on verra quelle charmante surprise la nature fait, un beau soir, aux habitants de Gallia"
        chapitreTitreTbl[22]="Qui se termine par une petite expérience assez curieuse de physique amusante"
        chapitreTitreTbl[23]="Qui traite d’un événement de haute importance, lequel met en émoi toute la colonie Gallienne"
        chapitreTitreTbl[24]="Dans lequel le capitaine Servadac et le lieutenant Procope apprennent enfin le mot de cette énigme cosmographique"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done
       
        #  - partie 2 - #
        partieNo=2
        initChapitre 1 20
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_Hector_Servadac_part02_chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Dans lequel on présente sans cérémonie le trente-sixième habitant du sphéroïde gallien"
        chapitreTitreTbl[2]="Dont le dernier mot apprend au lecteur ce que, sans doute, il avait déjà deviné"
        chapitreTitreTbl[3]="Quelques variations sur le vieux thème si connu des comètes du monde solaire et autres"
        chapitreTitreTbl[4]="Dans lequel on verra Palmyrin Rosette tellement enchanté de son sort que cela donne beaucoup à réfléchir"
        chapitreTitreTbl[5]="Dans lequel l’élève Servadac est assez malmené par le professeur Palmyrin Rosette"
        chapitreTitreTbl[6]="Dans lequel on verra que Palmyrin Rosette est fondé à trouver insuffisant le matériel de la colonie"
        chapitreTitreTbl[7]="Où l’on verra qu’Isac trouve une magnifique occasion de prêter son argent à plus de dix-huit cents pour cent"
        chapitreTitreTbl[8]="Dans lequel le professeur et ses élèves jonglent avec les sextillions, les quintillions et autres multiples des milliards"
        chapitreTitreTbl[9]="Dans lequel il sera uniquement question de Jupiter, surnommé le grand troubleur de comètes"

        chapitreTitreTbl[10]="Dans lequel il sera nettement établi qu’il vaut mieux trafiquer sur la terre que sur Gallia"
        chapitreTitreTbl[11]="Dans lequel le monde savant de Gallia se lance, en idée, au milieu des infinis de l’espace"
        chapitreTitreTbl[12]="Comment on célébra le 1er janvier sur Gallia, et de quelle façon se termina ce jour de fête"
        chapitreTitreTbl[13]="Dans lequel le capitaine Servadac et ses compagnons font la seule chose qu’il y eut à faire"
        chapitreTitreTbl[14]="Qui prouve que les humains ne sont pas faits pour graviter à deux cent vingt millions de lieues du soleil"
        chapitreTitreTbl[15]="Où se fait le récit des premières et dernières relations qui s’établirent entre Palmyrin Rosette et Isac Hakhabut"
        chapitreTitreTbl[16]="Dans lequel le capitaine Servadac et Ben-Zouf partent et reviennent comme ils étaient partis"
        chapitreTitreTbl[17]="Qui traite de la grande question du retour à la terre et de la proposition hardie qui fut faite par le lieutenant Procope"
        chapitreTitreTbl[18]="Dans lequel on verra que les galliens se préparent à contempler d’un peu haut l’ensemble de leur astéroïde"
        chapitreTitreTbl[19]="Dans lequel on chiffre, minute par minute, les sensations et impressions des passagers de la nacelle"

        chapitreTitreTbl[20]="Qui, contrairement à toutes les règles du roman, ne se termine pas par le mariage du héros"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

    }
    livreContent
    closeLivre
}

Verne_1877_Les_Indes_noires(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Les Indes noires";               tag_title="$tag_album";
    tag_year=1877;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="6h28";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-les-indes-noires.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #
        dlCover ""

        # - audios - #
        initChapitre 1 22
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_Indes_noires_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Deux_lettres_contradictoires"
        chapitreTitreTbl[2]="Chemin_faisant"
        chapitreTitreTbl[3]="Le sous sol du royaume unis"
        chapitreTitreTbl[4]="La fosse d'Ochar"
        chapitreTitreTbl[5]="La famille Forf"
        chapitreTitreTbl[6]="Quelques phénomènes inexplicables"
        chapitreTitreTbl[7]="Une experience de Simon Ford"
        chapitreTitreTbl[8]="Un coup de dynamite"
        chapitreTitreTbl[9]="La nouvelle Aberfoil"

        chapitreTitreTbl[10]="Aller et retour"
        chapitreTitreTbl[11]="Les dames de feu"
        chapitreTitreTbl[12]="Les explois de Jack Rayan"
        chapitreTitreTbl[13]="Call City"
        chapitreTitreTbl[14]="Suspendu à un fil"
        chapitreTitreTbl[15]="Nel au cotage"
        chapitreTitreTbl[16]="Sur l'échelle oscillante"
        chapitreTitreTbl[17]="Un lévée de soleil"
        chapitreTitreTbl[18]="Du lac Lomon au lac Catherine"
        chapitreTitreTbl[19]="Une dernière menace"

        chapitreTitreTbl[20]="Le pénitent"
        chapitreTitreTbl[21]="Le mariage de Nel"
        chapitreTitreTbl[22]="La légende du vieux Silfax"


        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}"$chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done

    }
    livreContent
    closeLivre
}

Verne_1879_Les_Revoltes_de_la_Bounty(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Les Revoltes de la Bounty";      tag_title="$tag_album";
    tag_year=1879;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="René Depasse";                     dl_source="www.litteratureaudio.com";
    dl_duree="0h55";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-les-revoltes-de-la-bounty.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Les_R%C3%A9volt%C3%A9s_de_la_Bounty_(nouvelle)

        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/xdescendants-bounty.jpg.pagespeed.ic.hI1EV2utH-.jpg"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Mutineers_of_the_Bounty_by_Jules_Verne%2C_illustration_by_Leon_Bennett.jpg/250px-Mutineers_of_the_Bounty_by_Jules_Verne%2C_illustration_by_Leon_Bennett.jpg" "$livreTitreNormalize-wiki"

        # - audios - #
        dlwget "http://www.litteratureaudio.org/mp3/Verne_-_Les_revoltes_de_la_Bounty.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

Verne_1879_Les_Cinq_Cents_Millions_de_la_Begum(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Les Cinq Cents Millions de la Begum";    tag_title="$tag_album";
    tag_year=1879;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="4h59";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-les-cinq-cents-millions-de-la-begum.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # 
        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/%27The_Begum%27s_Fortune%27_by_L%C3%A9on_Benett_17.jpg/250px-%27The_Begum%27s_Fortune%27_by_L%C3%A9on_Benett_17.jpg"

        # - audios - #
        initChapitre 1 20
        local episodeUrl="http://www.litteratureaudio.net/mp3/Jules_Verne_-_Les_Cinq_Cents_Millions_de_la_Begum_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Où_Mr.Sharp_fait_son_entrée"
        chapitreTitreTbl[2]="Deux_copains"
        chapitreTitreTbl[3]="Un_fait_divers"
        chapitreTitreTbl[4]="Part_à_deux"
        chapitreTitreTbl[5]="La_Cité_de_l_acier"
        chapitreTitreTbl[6]="La_Cité_de_l_acier"
        chapitreTitreTbl[7]="Le_Puits_Albrecht"
        chapitreTitreTbl[8]="Le_Bloc_central"
        chapitreTitreTbl[9]="PPC"

        chapitreTitreTbl[10]="Un_article_de_l_Unsere_Centurie,revue_allemande"
        chapitreTitreTbl[11]="Un_dîner_chez_le_docteur_Sarrasin"
        chapitreTitreTbl[12]="Le_conseil"
        chapitreTitreTbl[13]="Marcel_Bruckmann_au_professeur_Schultze"
        chapitreTitreTbl[14]="Branle-bas_de_combat"
        chapitreTitreTbl[15]="La_Bourse_de_San_Francisco"
        chapitreTitreTbl[16]="Deux_français_contre_une_ville"
        chapitreTitreTbl[17]="Explications_à_coups_de_fusil"
        chapitreTitreTbl[18]="L_Amande_du_noyau"
        chapitreTitreTbl[19]="Une_affaire_de_famille"

        chapitreTitreTbl[20]="Conclusion"


        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
	closeLivre
}

Verne_1879_Les_Tribulations_d_un_Chinois_en_Chine(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Les Tribulations d un Chinois en Chine"; tag_title="$tag_album";
    tag_year=1879;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Carole Bassani Adibzadeh";                 dl_source="www.litteratureaudio.com";
    dl_duree="6h42";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-les-tribulations-dun-chinois-en-chine.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"

    livreContent(){
        # https://fr.wikipedia.org/wiki/Les_Tribulations_d%27un_Chinois_en_Chine_(roman)

        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/xVerne_-_Les_Tribulations_d-un_Chinois_en_Chine.jpg.pagespeed.ic.Q4neCche8s.jpg"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/%27Tribulations_of_a_Chinaman_in_China%27_by_L%C3%A9on_Benett_43.jpg/250px-%27Tribulations_of_a_Chinaman_in_China%27_by_L%C3%A9on_Benett_43.jpg" "$livreTitreNormalize-wiki"

        # - audios - #
        initChapitre 1 22
        
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_Tribulations_d_un_Chinois_en_Chine_Chap"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "" $ chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1879_La_maison_a_vapeur(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="La maison à vapeur";                     tag_title="$tag_album";
    tag_year=1879;                                      dl_date="$tag_year.12.01";
    dl_editeur="Magasin d'éducation et de récréation "
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="14h00";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-la-maison-a-vapeur.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/La_Maison_%C3%A0_vapeur

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/a/a2/%27The_Steam_House%27_by_L%C3%A9on_Benett_018.jpg"

        # - audios - #
        # les cahpitres sont titrés
        partieNb=2

        # partie 1 #
        partieNo=1
        initChapitre 1 16
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_La_maison_a_vapeur_Partie${partieNo}_Chap"
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

        # partie 2 #
        partieNo=2
        initChapitre 1 14
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_La_maison_a_vapeur_Partie${partieNo}_Chap"
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1882_Le_Rayon_vert(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Le Rayon vert";                          tag_title="$tag_album";
    tag_year=1882;                                      dl_date="$tag_year.05.17";
    dl_editeur="Hetzel"
	dl_voix="Christine Setrin";                         dl_source="www.litteratureaudio.com";
    dl_duree="6h59";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-le-rayon-vert.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #
        #dlCover "http://www.litteratureaudio.com/img/xJules_Verne_-_Le_Rayon_vert.jpg.pagespeed.ic.jh_v1GTup3.jpg" # moins bonne qualité
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Verne-Paprsek-fronti.jpg/250px-Verne-Paprsek-fronti.jpg"

        # - audios - #
        initChapitre 1 23
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Le_Rayon_vert_Chap"

        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Le_frere_Sam_et_le_frere_Sib"
        chapitreTitreTbl[2]="Helena_Campbell"
        chapitreTitreTbl[3]="L_article_du_Morning_Post"
        chapitreTitreTbl[4]="En_descendant_la_Clyde"
        chapitreTitreTbl[5]="D_un_bateau_a_l_autre"
        chapitreTitreTbl[6]="Le_gouffre_du_Corryvrekan"
        chapitreTitreTbl[7]="Aristobulus_Ursiclos"
        chapitreTitreTbl[8]="Un_nuage_a_l_horizon"
        chapitreTitreTbl[9]="Propo_de_dame_Bess"

        chapitreTitreTbl[10]="Une_partie_de_crockett"
        chapitreTitreTbl[11]="Olivier_Sinclair"
        chapitreTitreTbl[12]="Nouveaux_projets"
        chapitreTitreTbl[13]="Les_magnificences_de_la_mer"
        chapitreTitreTbl[14]="La_vie_a_Iona"
        chapitreTitreTbl[15]="Les_ruines_d_Iona"
        chapitreTitreTbl[16]="Deux_coups_de_fusil"
        chapitreTitreTbl[17]="À_bord_de_la_Clorinda"
        chapitreTitreTbl[18]="Staffa"
        chapitreTitreTbl[19]="La_grotte_de_Fingal"

        chapitreTitreTbl[20]="Pour_miss_Campbell"
        chapitreTitreTbl[21]="Toute_une_tempete_dans_une_grotte"
        chapitreTitreTbl[22]="Le_Rayon-Vert"
        chapitreTitreTbl[23]="Conclusion"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1881_L_Etoile_du_sud(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="L'étoile du sud";                        tag_title="$tag_album";
    tag_year=1881;                                      dl_date="$tag_year.03.18";
    dl_editeur="Magasin d'éducation et de récréation"
	dl_voix="Bernard";                                  dl_source="www.litteratureaudio.com";
    dl_duree="8h07";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-letoile-du-sud.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/L%27%C3%89toile_du_sud
        # https://fr.wikisource.org/wiki/L'Etoile_du_sud

        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/xJules_Verne_-_L_Etoile_du_sud.jpg.pagespeed.ic.u3_0u77uc8.jpg"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/%27The_Vanished_Diamond%27_by_L%C3%A9on_Benett_01.jpg/250px-%27The_Vanished_Diamond%27_by_L%C3%A9on_Benett_01.jpg" "$livreTitreNormalize-wiki"

        # - audios - #
        initChapitre 1 24
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_L_Etoile_du_sud_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Renversants_ces_francais"
        chapitreTitreTbl[2]="Au_champs_des_diamants"
        chapitreTitreTbl[3]="Un_peu_de_science_enseignee_de_bonne_amitie"
        chapitreTitreTbl[4]="Vandergaart_Kopje"
        chapitreTitreTbl[5]="Premiere_exploitation"
        chapitreTitreTbl[6]="Moeurs_du_camp"
        chapitreTitreTbl[7]="L_eboulement"
        chapitreTitreTbl[8]="La_grande_experience"
        chapitreTitreTbl[9]="Une_surprise"

        chapitreTitreTbl[10]="Ou_John_Watktins_reflechit"
        chapitreTitreTbl[11]="L_Etoile_du_Sud"
        chapitreTitreTbl[12]="Preparatifs_de_depart"
        chapitreTitreTbl[13]="A_travers_le_Transvaal"
        chapitreTitreTbl[14]="Au_Nord_du_Limpopo"
        chapitreTitreTbl[15]="un_complot"
        chapitreTitreTbl[16]="Trahison"
        chapitreTitreTbl[17]="Un_steeple-chase_africain"
        chapitreTitreTbl[18]="L_autruche_qui_parle"
        chapitreTitreTbl[19]="La_grotte_merveilleuse"

        chapitreTitreTbl[20]="Le_retour"
        chapitreTitreTbl[21]="Justice_venitienne"
        chapitreTitreTbl[22]="une_mine_d_un_nouveau_genre"
        chapitreTitreTbl[23]="La_statue_du_commandeur"
        chapitreTitreTbl[24]="Une_etoile_qui_file"


        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1884_Frritt_Flacc(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Frritt-Flacc";                           tag_title="$tag_album";
    tag_year=1884;                                      dl_date="$tag_year";
    dl_editeur="Figaro illustré"
	dl_voix="René Depasse";                             dl_source="www.litteratureaudio.com";
    dl_duree="0h20";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-frritt-flacc.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Frritt-Flacc
        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/xFrritt_flacc.jpg.pagespeed.ic.dwpFWQMHfG.jpg"

        # - audios - #
        dlwget "http://www.litteratureaudio.org/mp3/Verne_-_Frritt_flacc.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

Verne_1885_lepave_du_cynthia(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="L'Épave du Cynthia";                     tag_title="$tag_album";
    tag_year=1885;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Brigitte84";                               dl_source="www.litteratureaudio.com";
    dl_duree="8h53";
    dl_postnote=" Un roman cosigné par Jules Verne et André Laurie."
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-et-laurie-andre-lepave-du-cynthia.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/L%27%C3%89pave_du_Cynthia
        # https://gallica.bnf.fr/ark:/12148/bpt6k65282627/f10.item

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/HetzelEpaveCynthia.jpg/520px-HetzelEpaveCynthia.jpg"
        dlCover "http://www.litteratureaudio.com/img/xepave_du_Cynthia_-_George_Roux.jpg.pagespeed.ic.KdGwgB83zO.webp"

        # - audios - #
        initChapitre 1 22
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_et_Andre_Laurie_-_L_epave_du_Cynthia_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="L'ami de M. Malarius"
        chapitreTitreTbl[2]=" Chez un pêcheur de Noroë"
        chapitreTitreTbl[3]="Les idées de maaster Hersebom"
        chapitreTitreTbl[4]=" A Stockholm"
        chapitreTitreTbl[5]="Tretten yule dage"
        chapitreTitreTbl[6]="La décision d'Érik"
        chapitreTitreTbl[7]="L'opinion de Vanda"
        chapitreTitreTbl[8]="Patrick O'Donoghan"
        chapitreTitreTbl[9]="Cinq cents livres sterling de récompense"

        chapitreTitreTbl[10]="Tudor Brown, esquire"
        chapitreTitreTbl[11]="On nous écrit de la Véga"
        chapitreTitreTbl[12]="Passagers imprévus"
        chapitreTitreTbl[13]="Appuyons au sud-ouest"
        chapitreTitreTbl[14]="La Basse-Froide"
        chapitreTitreTbl[15]="Le chemin le plus court"
        chapitreTitreTbl[16]="De Serdze-Kamen à Ljakow"
        chapitreTitreTbl[17]="Enfin!"
        chapitreTitreTbl[18]="Coups de canon"
        chapitreTitreTbl[19]="Coups de fusil"

        chapitreTitreTbl[20]="La fin du périple"
        chapitreTitreTbl[21]="Une lettre de Paris"
        chapitreTitreTbl[22]="Le Val-Féray. - Conclusion"
    
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done    
    }
    livreContent
    closeLivre
}

Verne_1886_Robur_le_Conquerant(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Robur le Conquerant";                    tag_title="$tag_album";
    tag_year=1886;                                      dl_date="$tag_year.06.29";
    dl_editeur="Journal des débats politiques et littéraires"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="7h00";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-robur-le-conquerant.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Robur_le_Conqu%C3%A9rant
        # http://www.jules-verne.co.uk/french-robur-le-conquerant/

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Robur_1.jpg/250px-Robur_1.jpg"

        # - audios - #
        initChapitre 1 18
        local episodeUrl="www.litteratureaudio.org/mp3/Jules_Verne_-_Robur_le_Conquerant_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Ou_le_monde_savant_et_le_monde_ignorant_sont_aussi_bien_embarasses_l_un_que_l_autre"
        chapitreTitreTbl[2]="Dans_lequel_les_membres_du_Weldon"
        chapitreTitreTbl[3]="Dans_lequel_un_nouveau_personnage_n_a_pas_besoin_d_etre_presente_car_il_se_presente_lui_meme"
        chapitreTitreTbl[4]="Dans_lequel_a_propos_du_valet_Frycollin,l_auteur_essaie_de_rehabiliter_la_lune"
        chapitreTitreTbl[5]="Dans_lequel_une_suspension_d_hostilites_est_consentie_entre_le_president_et_le_secretaire_du_Weldon-institure"
        chapitreTitreTbl[6]="Les_ingenieurs,les_mecaniciens_et_autres_savants_feraient_bien_de__passer"
        chapitreTitreTbl[7]="Dans_lequel_uncle_Prudent__et_Phil_Evans_refusenet_encore__de_se_laisser_convaincre"
        chapitreTitreTbl[8]="Ou_l_on_verra_que_Robur_se_decide_a_repondre_a_l_importante_question_qui_lui_a_ete_pose"
        chapitreTitreTbl[9]="Dans_lequel_l_Albatros_a_franchit_pres_de_dix_milles_kilometres,qui_se_termine_par_un_bond_prodigieux"

        chapitreTitreTbl[10]="Dans_lequel_on_verra_comment_et_pourquoi_la_valet_Frycollin_fut_mis_a_la_remorque"
        chapitreTitreTbl[11]="Dans_lequel_la_colere_de_uncle_Prudent_croit_comme_le_carre_de_la_vitesse"
        chapitreTitreTbl[12]="Dans_lequel_l_ingenieur_Robur_agit_comme_s_il_voulait_concourir_pour_un_des_prix_Monthyon"
        chapitreTitreTbl[13]="Dans_lequel_uncle_Prudent_et_Phil_Evans_traversent_tout_un_ocean_sans_avoir_le_mal_de_mer"
        chapitreTitreTbl[14]="Dans_lequel_l_Albatros_fait_ce_qu_on_ne_pourrait_sans_doute_jamais_faire"
        chapitreTitreTbl[15]="Dans_lequel_il_se_passent_des_choses_qui_meritent_vraiment_la_peine_d_etre_racontes"
        chapitreTitreTbl[16]="Qui_laissera_le_lecteur_dans_une_indecision_peut-etre_regrettable"
        chapitreTitreTbl[17]="Dans_lequel_on_revient_deux_mois_en_arriere_et_ou_l_onsaute_neuf_mois_en_avant"
        chapitreTitreTbl[18]="Qui_termine_cette_veridique_histoire_de_l-Albatros_sans_la_terminer"


        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
            #dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1887_Gil_Braltar(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                         tag_genre="Roman";
    tag_album="Gil Braltar ";               tag_title="$tag_album";
    tag_year=1887;                          dl_date="$tag_year.01.02";
    dl_editeur="Petit Journal"
	dl_voix="Bernard";                     dl_source="www.litteratureaudio.com";
    dl_duree="0h15";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-gil-braltar.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Gil_Braltar

        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/xGil_Braltar.jpg.pagespeed.ic.vF5Dyemlce.webp"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Gil_Braltar_by_Jules_Verne.jpg/250px-Gil_Braltar_by_Jules_Verne.jpg"

        # - audios - #
        initChapitre 1 1
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_Gil_Braltar.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent

    closeLivre
}

Verne_1888_Deux_Ans_de_vacances(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Deux ans de vacances";                   tag_title="$tag_album";
    tag_year=1888;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Christine Setrin";                         dl_source="www.litteratureaudio.com";
    dl_duree="13h55";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-deux-ans-de-vacances.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-deux-ans-de-vacances.html
        # https://fr.wikipedia.org/wiki/Deux_ans_de_vacances
        # https://fr.wikisource.org/wiki/Deux_Ans_de_vacances

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Dwalat_01.jpg/250px-Dwalat_01.jpg"

        # - audios - #

        # - Preface - #
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Deux_Ans_de_vacances_Chapitre_00_Preface.mp3"
        dlwget "$episodeUrl" "$livreTitreNormalize--chapitre-00-$chapitreNb-Preface.mp3"

        initChapitre 1 30
        #local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Deux_Ans_de_vacances_Chapitre_0"
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Deux_Ans_de_vacances_Chapitre_"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="La_tempete"
        chapitreTitreTbl[2]="Au_millieu_du_ressac"
        chapitreTitreTbl[3]="La_pension_Chairman_à_Auckland"
        chapitreTitreTbl[4]="Première_exploration_du_littoral"
        chapitreTitreTbl[5]="Ile_ou_continent"
        chapitreTitreTbl[6]="Discussion"
        chapitreTitreTbl[7]="Le_bois_de_bouleaux"
        chapitreTitreTbl[8]="Reconnaissance_dans_l_ouest_du_lac"
        chapitreTitreTbl[9]="Visite_à_la_caverne"

        chapitreTitreTbl[10]="Récit_de_l_exploration"
        chapitreTitreTbl[11]="Premières_dispositions_à_l_intérieur_de_French-den"
        chapitreTitreTbl[12]="Agrandissement_de_French-den"
        chapitreTitreTbl[13]="Le_programme_d_études"
        chapitreTitreTbl[14]="Derniers_coups_de_l_hiver"
        chapitreTitreTbl[15]="Route_a_suivre_pour_le_retour"
        chapitreTitreTbl[16]="Briant_inquiet_de_Jacques"
        chapitreTitreTbl[17]="Preparatifs_en_vue_du_prochain_hiver"
        chapitreTitreTbl[18]="Le_marais_salant"
        chapitreTitreTbl[19]="Le_mât_de_signaux"

        chapitreTitreTbl[20]="Une_halte_à_la_pointe_sud_du_lac"
        chapitreTitreTbl[21]="Exploration_de_Deception-bay"
        chapitreTitreTbl[22]="Une_idée_de_Briant"
        chapitreTitreTbl[23]="La_situation_telle_qu_elle_est"
        chapitreTitreTbl[24]="Premier_essai"
        chapitreTitreTbl[25]="La_chaloupe_du_Severn"
        chapitreTitreTbl[26]="Kate_et_le_master"
        chapitreTitreTbl[27]="Le_détroit_de_Magellan"
        chapitreTitreTbl[28]="Interrogatoire_de_Forbes"
        chapitreTitreTbl[29]="Réaction"

        chapitreTitreTbl[30]="Conclusion"
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1889_Sans_dessus_dessous(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Sans dessus dessous";                    tag_title="$tag_album";
    tag_year=1889;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="TheoTeX";                                  dl_source="www.litteratureaudio.com";
    dl_duree="5h09";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-sans-dessus-dessous.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Sans_dessus_dessous
        # https://m.21-bal.com/pravo/6446/index.html : titre des chapitre et textes (manque un chapitre, sont dans le désordre a partie du 15, mais contient 2 chapitres de documentation)

        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/xmaston_hook.jpg.pagespeed.ic.UiKtNnNPtw.jpg"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/%27The_Purchase_of_the_North_Pole%27_by_George_Roux_31.jpg/250px-%27The_Purchase_of_the_North_Pole%27_by_George_Roux_31.jpg" "$livreTitreNormalize-wiki"

        # - audios - #
        initChapitre  21
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Sans_dessus_dessous_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Ou la North Polar practical association lance un document à travers les 2 mondes"
        chapitreTitreTbl[2]="Dans lequel les délégués anglais, hollandais, suédois, danois et russe se présentent au lecteur"
        chapitreTitreTbl[3]="Dans lequel se fait l’adjudication des régions du pôle arctique"
        chapitreTitreTbl[4]="Dans lequel reparaissent de vieilles connaissances de nos jeunes lecteurs"
        chapitreTitreTbl[5]="Et d’abord, peut-on admettre qu’il y ait des houillères près du Pôle nord"
        chapitreTitreTbl[6]="Dans lequel est interrompue une conversation téléphonique entre Mrs Scorbitt et J.-T. Maston"
        chapitreTitreTbl[7]="Dans lequel le président Barbicane n’en dit pas plus qu’il ne lui convient d’en dire"
        chapitreTitreTbl[8]=" « Comme dans Jupiter ? » a dit le président du Gun-Club"
        chapitreTitreTbl[9]="Dans lequel on sent apparaître un Deux ex Machina d’origine française"

        chapitreTitreTbl[10]=" Dans lequel diverses inquiétudes commencent à se faire jour"
        chapitreTitreTbl[11]="Ce qui se trouve dans le carnet de J.-T. Maston, et ce qui ne s’y trouve plus"
        chapitreTitreTbl[12]="Dans lequel J.-T. Maston continue héroïquement à se taire"
        chapitreTitreTbl[13]="La fin duquel J.-T. Maston fait une réponse véritablement épique"
        chapitreTitreTbl[14]="Très court, mais dans lequel l’x prend une valeur géographique"

        chapitreTitreTbl[15]="Qui contient quelques détails intéressants pour les habitants du sphéroïde terrestre"
        chapitreTitreTbl[16]="Dans lequel le chœur des mécontents va crescendo et rinforzando"
        chapitreTitreTbl[17]="Ce qui s’est fait au Kilimandjaro pendant huit mois de cette année mémorablee"
        chapitreTitreTbl[18]="Dans lequel les populations du Wamasai attendent que le président Barbicane crie feu ! au capitaine Nicholl"
        chapitreTitreTbl[19]="Dans lequel J.-T. Maston regrette peut-être le temps où la foule voulait le lyncher"

        chapitreTitreTbl[20]="Qui termine cette curieuse histoire aussi véridique qu’invraisemblable"
        chapitreTitreTbl[21]="Très court, mais tout à fait rassurant pour l’avenir du monde"

        # chapitre textes
        chapitreTitreTbl[22]="Départ du boulet d’un point quelconque"
        chapitreTitreTbl[23]="Départ du boulet dans le cas considéré"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done

        addChapitre "${chapitreTitreTbl[22]}" 22;
        echo "https://m.21-bal.com/pravo/6446/index.html?page=21" > "$livreTitreNormalizeChapitre.txt"

        addChapitre "${chapitreTitreTbl[23]}" 23;
        echo "https://m.21-bal.com/pravo/6446/index.html?page=22" > "$livreTitreNormalizeChapitre.txt"
    }
    livreContent
    closeLivre
}

Verne_1873_09_28_Vingt_quatre_minutes_en_ballon(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="24 Minutes en ballon";                   tag_title="$tag_album";
    tag_year=1873;                                      dl_date="$tag_year.09.28";
    dl_editeur="L’Écho de la Somme"
	dl_voix="René Depasse";                             dl_source="www.litteratureaudio.com";
    dl_duree="0h20";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-vingt-quatre-minutes-en-ballon.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/24_Minutes_en_ballon
    
        # - cover - #
        #dlCover "" # pas de cover

        # - audios - #
        dl_postnote ="Il s'agit du récit par Jules Verne de son unique ascension en ballon, effectuée avec Eugène Godard à bord du Météore le 28 septembre 1873."
        dlwget "http://www.litteratureaudio.org/mp3/Verne_-_Frritt_flacc.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

Verne_1890_Cesar_Cascabel(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Cesar Cascabel";                         tag_title="$tag_album";
    tag_year=1890;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Aurelien Ridon";                           dl_source="www.litteratureaudio.com";
    dl_duree="12h12";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-cesar-cascabel.html
        # http://jv.gilead.org.il/zydorczak/cas00.html

        # - cover - #
        dlCover ""

        # - audios - #
        partieNb=2
        
        # - partie 1- #
        partieNo=1;
        initChapitre 1 16
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Cesar_Cascabel_P${partieNo}_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Fortune_faite"
        chapitreTitreTbl[2]="Famille_Cascabel"
        chapitreTitreTbl[3]="La_Sierra_Nevada"
        chapitreTitreTbl[4]="Grande_détermination"
        chapitreTitreTbl[5]="En_route"
        chapitreTitreTbl[6]="Suite_du_voyage"
        chapitreTitreTbl[7]="A_travers_le_Caribou"
        chapitreTitreTbl[8]="Au_village_des_Coquins"
        chapitreTitreTbl[9]="On_ne_passe_pas"

        chapitreTitreTbl[10]="Kayette"
        chapitreTitreTbl[11]="Sitka"
        chapitreTitreTbl[12]="De_Sitka_au_fort_Youkon"
        chapitreTitreTbl[13]="Une_idee_de_Cornélia_Cascabel"
        chapitreTitreTbl[14]="Du_fort_Youkon_a_Port-Clarence"
        chapitreTitreTbl[15]="Port-Clarence"
        chapitreTitreTbl[16]="Adieux_au_nouveau_continent"


        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done    


        # - partie 2 - #
        partieNo=2;
        initChapitre 1 15
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Cesar_Cascabel_P${partieNo}_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Le_detroit_de_Behring"
        chapitreTitreTbl[2]="Entre_deux_courants"
        chapitreTitreTbl[3]="En_derive"
        chapitreTitreTbl[4]="Du_16_novembre_au_2_decembre"
        chapitreTitreTbl[5]="Les_iles_Liakhoff"
        chapitreTitreTbl[6]="Hivernage"
        chapitreTitreTbl[7]="Un_bon_tour_de_M.Cascabel"
        chapitreTitreTbl[8]="Le_pays_des_Iakoutes"
        chapitreTitreTbl[9]="Jusqu_a_l_Obi"

        chapitreTitreTbl[10]="Du_fleuve_Obi_aux_monts_Ourals"
        chapitreTitreTbl[11]="Les_monts_Ourals"
        chapitreTitreTbl[12]="Voyage_termine_et_qui_n_est_pas_fini"
        chapitreTitreTbl[13]="Une_longue_journee"
        chapitreTitreTbl[14]="Denouement_tres_applaudi_des_spectateurs"
        chapitreTitreTbl[15]="Conclusion"
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done
    }    
    livreContent
    closeLivre
}

Verne_1891_Ptit_Bonhomme(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="P’tit Bonhomme ";                        tag_title="$tag_album";
    tag_year=1891;                                      dl_date="$tag_year";
    dl_editeur="Magasin d'éducation et de récréation";
	dl_voix="Pomme";                                    dl_source="www.litteratureaudio.com";
    dl_duree="13h39";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-ptit-bonhomme.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/P%27tit-Bonhomme

        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/xJules_Verne_-_Ptit_Bonhomme.jpg.pagespeed.ic.Z8GL42DiCK.webp"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Verne-dobr%C3%A1%C4%8Dek1.jpg/250px-Verne-dobr%C3%A1%C4%8Dek1.jpg"
    
        # - audios - #
        partieNb=2

        # partie 1 ~
        partieTitre="Les premiers pas"
        partieNo=1
        touch "$livreTitreNormalize-p$partieNo-$partieTitre.partie"
        initChapitre 1 16
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_P_tit_Bonhomme_P1_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Au fond du Connaught"
        chapitreTitreTbl[2]="Marionnettes royales !"
        chapitreTitreTbl[3]="Ragged-school"
        chapitreTitreTbl[4]="L’enterrement d’une mouette"
        chapitreTitreTbl[5]="Encore la ragged-school"
        chapitreTitreTbl[6]="Limerick"
        chapitreTitreTbl[7]="Situation compromise"
        chapitreTitreTbl[8]="La ferme de Kerwan"
        chapitreTitreTbl[9]="La ferme de Kerwan (suite)"

        chapitreTitreTbl[10]="Ce qui s’est passé au Donégal"
        chapitreTitreTbl[11]="Prime à gagner"
        chapitreTitreTbl[12]="Le retour"
        chapitreTitreTbl[13]="Double baptême"
        chapitreTitreTbl[14]="Et il n’avait pas encore neuf ans"
        chapitreTitreTbl[15]="Mauvaise année"
        chapitreTitreTbl[16]="Éviction "


        addChapitre "${chapitreTitreTbl[1]}" 1;
        dl_postnote="chapitres 1,2,3";  # a mettre avantaddChapitre
        dlwget "${episodeUrl}1_a_3.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_2_3.mp3"

        addChapitre "${chapitreTitreTbl[4]}" 4;
        dl_postnote="chapitres 4,5,6";  setTagNoteLivre
        dlwget "${episodeUrl}4_a_6.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_5_6.mp3"

        addChapitre "${chapitreTitreTbl[7]}" 7;
        dl_postnote="chapitres 7,8";  setTagNoteLivre
        dlwget "${episodeUrl}7_et_8.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_7_8.mp3"

        addChapitre "${chapitreTitreTbl[9]}" 9;
        dl_postnote="chapitres 9,10,11";  setTagNoteLivre
        dlwget "${episodeUrl}9_a_11.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_10_11.mp3"

        addChapitre "${chapitreTitreTbl[12]}" 12;
        dl_postnote="chapitres 12,13";  setTagNoteLivre
        dlwget "${episodeUrl}12_et_13.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_12_13.mp3"

        addChapitre "${chapitreTitreTbl[14]}" 14;
        dl_postnote="chapitres 14,15";  setTagNoteLivre
        dlwget "${episodeUrl}14_et_15.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_14.mp3"

        addChapitre "${chapitreTitreTbl[16]}" 16;
        dlwget "${episodeUrl}16.mp3" "$livreTitreNormalizePartieChapitre.mp3"


        partieTitre="Dernières étapes"
        partieNo=2
        touch "$livreTitreNormalize-p$partieNo-$partieTitre.partie"

        initChapitre 1 15
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_P_tit_Bonhomme_P2_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Leurs Seigneuriest"
        chapitreTitreTbl[2]="Pendant quatre mois"
        chapitreTitreTbl[3]="À Trelingar-castle"
        chapitreTitreTbl[4]="Les lacs de Killarney"
        chapitreTitreTbl[5]="Chien de berger et chiens de chasse"
        chapitreTitreTbl[6]="Dix-huit ans à deux"
        chapitreTitreTbl[7]="Sept mois à Cork "
        chapitreTitreTbl[8]="Premier chauffeur"
        chapitreTitreTbl[9]="Une idée commerciale de Bob"

        chapitreTitreTbl[10]="À Dublin"
        chapitreTitreTbl[11]="Le bazar des Petites Poches"
        chapitreTitreTbl[12]="Comme on se retrouve"
        chapitreTitreTbl[13]="Changement de couleur et d’état"
        chapitreTitreTbl[14]="La mer de trois côtés "
        chapitreTitreTbl[15]="Et pourquoi pas ? "
    
        addChapitre "${chapitreTitreTbl[1]}" 1;
        dl_postnote="chapitres 1,2,3";  setTagNoteLivre
        dlwget "${episodeUrl}1_a_3.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_2_3.mp3"

        addChapitre "${chapitreTitreTbl[4]}" 4;
        dl_postnote="chapitres 4,5";  setTagNoteLivre
        dlwget "${episodeUrl}4_et_5.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_5.mp3"

        addChapitre "${chapitreTitreTbl[6]}" 6;
        dl_postnote="chapitres 6,7,8";  setTagNoteLivre
        dlwget "${episodeUrl}6_a_8.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_7_8.mp3"

        addChapitre "${chapitreTitreTbl[9]}" 9;
        dl_postnote="chapitres 9,10";  setTagNoteLivre
        dlwget "${episodeUrl}9_et10.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_10.mp3"

        addChapitre "${chapitreTitreTbl[11]}" 11;
        dl_postnote="chapitres 11,12";  setTagNoteLivre
        dlwget "${episodeUrl}11_et12.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_12.mp3"

        addChapitre "${chapitreTitreTbl[13]}" 13;
        dl_postnote="chapitres 13,14";  setTagNoteLivre
        dlwget "${episodeUrl}13_et14_V3.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_14.mp3"

        addChapitre "${chapitreTitreTbl[15]}" 15;
        dlwget "${episodeUrl}15.mp3" "$livreTitreNormalizePartieChapitre.mp3"
    }
    livreContent
    closeLivre
}

Verne_1892_Le_Chateau_des_Carpathes(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Le_Chateau_des_Carpathes";               tag_title="$tag_album";
    tag_year=1892;                                      dl_date="$tag_year.12.15";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="5h10";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-le-chateau-des-carpathes.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Le_Ch%C3%A2teau_des_Carpathes
        # https://fr.wikisource.org/wiki/Le_Ch%C3%A2teau_des_Carpathes

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/%27The_Carpathian_Castle%27_by_L%C3%A9on_Benett_33.jpg/220px-%27The_Carpathian_Castle%27_by_L%C3%A9on_Benett_33.jpg"
        # - audios - #
        initChapitre 1 18     
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Le_Chateau_des_Carpathes_Chap"
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done

    }
    livreContent
    closeLivre
}

Verne_1895_L_Ile_a_helice(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Science_fiction";
    tag_album="L'ïle à hélice";                         tag_title="$tag_album";
    tag_year=1895;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="11h50";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-lile-a-helice.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/L%27%C3%8Ele_%C3%A0_h%C3%A9lice
        # http://www.gutenberg.org/ebooks/17798

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/%27Propeller_Island%27_by_L%C3%A9on_Benett_79.jpg/250px-%27Propeller_Island%27_by_L%C3%A9on_Benett_79.jpg"

        # - audios - #
        partieNb=2;

        # - partie 1- #
        partieNo=1;
        initChapitre 1 14
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_L_ile_a_helice_Partie_${partieNo}_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Le_Quatuor_Concertant"
        chapitreTitreTbl[2]="Puissance_d_une_sonate_cacophonique"
        chapitreTitreTbl[3]="Un_loquace_cicérone"
        chapitreTitreTbl[4]="Le_Quatuor_Concertant_déconcerté"
        chapitreTitreTbl[5]="Standard-Island_et_Milliard-City"
        chapitreTitreTbl[6]="Invites...inviti"
        chapitreTitreTbl[7]="Cap_a_l_ouest"
        chapitreTitreTbl[8]="Navigation"
        chapitreTitreTbl[9]="L_archipel_des_Sandwich"

        chapitreTitreTbl[10]="Passage_de_la_ligne"
        chapitreTitreTbl[11]="Iles_Marquises"
        chapitreTitreTbl[12]="Trois_semaines_aux_Pomotou"
        chapitreTitreTbl[13]="Relache_a_Tahiti"
        chapitreTitreTbl[14]="De_fetes_en_fetes"
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
            #dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done


        # - partie 2- #
        partieNo=2;
        initChapitre 1 14
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_L_ile_a_helice_Partie_${partieNo}_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Aux_Iles_de_Cook"
        chapitreTitreTbl[2]="D_îles_en_iles"
        chapitreTitreTbl[3]="Concert_a_la_cour"
        chapitreTitreTbl[4]="Ultimatum_britannique"
        chapitreTitreTbl[5]="Le_Tabou_a_Tonga-Tabou"
        chapitreTitreTbl[6]="Une_collection_de_fauves"
        chapitreTitreTbl[7]="Battues"
        chapitreTitreTbl[8]="Fidji_et_Fidjiens"
        chapitreTitreTbl[9]="Un_casus_belli"

        chapitreTitreTbl[10]="Changement_de_proprietaires"
        chapitreTitreTbl[11]="Attaque_et_defense"
        chapitreTitreTbl[12]="Tribord_et_Babord_la_barre"
        chapitreTitreTbl[13]="Le_mot_de_la_situation_dit_par_Pinchinat"
        chapitreTitreTbl[14]="Denouement"
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
            #dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1897_Le_Sphinx_des_Glaces(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Le Sphinx des Glaces";                   tag_title="$tag_album";
    tag_year=1897;                                      dl_date="$tag_year.01.01";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="13h40";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-le-sphinx-des-glaces.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Le_Sphinx_des_glaces
        # https://fr.wikisource.org/wiki/Le_Sphinx_des_glaces

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Verne-Sphinx1.jpg/250px-Verne-Sphinx1.jpg"

        # - audios - #
        partieNo=2
    
        # - partie 1- #
        partieNo=1;
        initChapitre 1 16
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Le_sphinx_des_glaces_Partie${partieNo}_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Les_Iles_Kerguelen"
        chapitreTitreTbl[2]="La_goelette_Halbrane"
        chapitreTitreTbl[3]="Le_capitaine_Len_Guy"
        chapitreTitreTbl[4]="Des_iles_Kerguelen_a_l_ile_du_Prince-Edouard"
        chapitreTitreTbl[5]="Le_roman_d_Edgard_Poe"
        chapitreTitreTbl[6]="Comme_un_linceul_qui_s_entr_ouvre"
        chapitreTitreTbl[7]="Tristan_d_Acunha"
        chapitreTitreTbl[8]="En_direction_vers_les_Falklands"
        chapitreTitreTbl[9]="Mise_en_etat_de_l_Halbrane"

        chapitreTitreTbl[10]="Au_début_de_la_campagne"
        chapitreTitreTbl[11]="Des_Sandwich_au_cercle_polaire"
        chapitreTitreTbl[12]="Entre_le_cercle_polaire_et_la_banquise"
        chapitreTitreTbl[13]="Le_long_de_la_banquise"
        chapitreTitreTbl[14]="Une_voix_dans_un_reve"
        chapitreTitreTbl[15]="L_îlot_Bennet"
        chapitreTitreTbl[16]="L_Ile_Tsalal"
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" "$chapitreNu";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done    


        # - partie 2 - #
        partieNo=2;
        initChapitre 1 16
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Le_sphinx_des_glaces_Partie${partieNo}_Chap"
            local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Et Pym"
        chapitreTitreTbl[2]="Decision_prise"
        chapitreTitreTbl[3]="Le_groupe_disparu"
        chapitreTitreTbl[4]="Du_29_decembre_au_9_janvier"
        chapitreTitreTbl[5]="Une_embardee"
        chapitreTitreTbl[6]="Terre"
        chapitreTitreTbl[7]="L_ice-berg_culbute"
        chapitreTitreTbl[8]="Le_coup_de_grace"
        chapitreTitreTbl[9]="Que_faire"

        chapitreTitreTbl[10]="Hallucinations"
        chapitreTitreTbl[11]="Au_milieu_des_brumes"
        chapitreTitreTbl[12]="Campement"
        chapitreTitreTbl[13]="Dirk_Peters_a_la_mer"
        chapitreTitreTbl[14]="Onze_ans_en_quelques_pages"
        chapitreTitreTbl[15]="Le_Sphinx_des_glaces"
        chapitreTitreTbl[16]="Douze_sur_soixante-dix"
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1899_Le_Testament_d_un_excentrique(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Le Testament d'un excentrique";          tag_title="$tag_album";
    tag_year=1899;                                      dl_date="$tag_year.01.01";
    dl_editeur="Magasin d'Éducation et de Récréation"
	dl_voix="Pomme";                           dl_source="www.litteratureaudio.com";
    dl_duree="16h23";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-le-testament-dun-excentrique.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Le_Testament_d%27un_excentrique
        # https://fr.wikisource.org/wiki/Le_Testament_d%E2%80%99un_excentrique (titre des chapitres et textes)

        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/xJeu_de_l_oie.jpg.pagespeed.ic.lYVkzja0j-.webp"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Verne-TE1.jpg/250px-Verne-TE1.jpg"

        # - audios - #
        partieNb=2
        
        # - partie 1- #
        partieNo=1;
        initChapitre 1 15
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Le_Testament_d_un_Excentrique_P1_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Toute une ville en joie"
        chapitreTitreTbl[2]="William J. Hypperbone"
        chapitreTitreTbl[3]="Oakswoods"
        chapitreTitreTbl[4]="Les « Six »"
        chapitreTitreTbl[5]="Le Testament"
        chapitreTitreTbl[6]="La carte mise en circulation"
        chapitreTitreTbl[7]="Le premier partant"
        chapitreTitreTbl[8]="Tom Crabbe entraîné par John Milner"
        chapitreTitreTbl[9]="Un et un font deux"

        chapitreTitreTbl[10]="Un reporter en voyage"
        chapitreTitreTbl[11]="Les transes de Jovita Foley"
        chapitreTitreTbl[12]="La cinquième partenaire"
        chapitreTitreTbl[13]="Aventures du commodre Urrican"
        chapitreTitreTbl[14]="Suites des aventures du commodore Urrican"
        chapitreTitreTbl[15]="La situation au 27 mai"

        addChapitre "${chapitreTitreTbl[1]}" 1;
        dl_postnote="chapitres 1,2";  setTagNoteLivre
        dlwget "${episodeUrl}1_et_2.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_2.mp3"

        addChapitre "${chapitreTitreTbl[31]}" 3;
        dl_postnote="chapitres 3,4";  setTagNoteLivre
        dlwget "${episodeUrl}3_et_4.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_4.mp3"

        addChapitre "${chapitreTitreTbl[5]}" 5;
        dl_postnote="chapitres 5,6";  setTagNoteLivre
        dlwget "${episodeUrl}1_et_6.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_6.mp3"

        addChapitre "${chapitreTitreTbl[7]}" 7;
        dl_postnote="chapitres 7,8";  setTagNoteLivre
        dlwget "${episodeUrl}7_et_8.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_8.mp3"

        addChapitre "${chapitreTitreTbl[9]}" 10;
        dl_postnote="chapitres 9,10";  setTagNoteLivre
        dlwget "${episodeUrl}9_et_10.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_10.mp3"

        addChapitre "${chapitreTitreTbl[11]}" 11;
        dl_postnote="chapitres 11,12";  setTagNoteLivre
        dlwget "${episodeUrl}11_et_12.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_12.mp3"

        addChapitre "${chapitreTitreTbl[15]}" 15;
        dlwget "${episodeUrl}15.mp3" "$livreTitreNormalizePartieChapitre.mp3"

        # - partie 2 - #
        partieNo=2;
        initChapitre 1 15
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Le_Testament_d_un_Excentrique_P2_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Le Parc National"
        chapitreTitreTbl[2]="Pris l’un pour l’autre"
        chapitreTitreTbl[3]="À pas de tortue"
        chapitreTitreTbl[4]="Le pavillon vert"
        chapitreTitreTbl[5]="Les grottes du Kentucky "
        chapitreTitreTbl[6]="La vallée de la mort"
        chapitreTitreTbl[7]="À la maison de South Halstedt Street"
        chapitreTitreTbl[8]="Le coup du révérent Hunter"
        chapitreTitreTbl[9]="Deux cents dollars par jour"

        chapitreTitreTbl[10]="Les pérégrinations d’Harris T. Kymbale"
        chapitreTitreTbl[11]="La prison du Missouri"
        chapitreTitreTbl[12]="Sensationnel fait divers pour la Tribune"
        chapitreTitreTbl[13]="Les derniers coups du match Hypperbone"
        chapitreTitreTbl[14]="La cloche d’Oakswoods"
        chapitreTitreTbl[15]="Dernière excentricité"

        addChapitre "${chapitreTitreTbl[1]}" 1;
        dl_postnote="chapitres 1,2";  setTagNoteLivre
        dlwget "${episodeUrl}1_et_2.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_2.mp3"

        addChapitre "${chapitreTitreTbl[31]}" 3;
        dl_postnote="chapitres 3,4";  setTagNoteLivre
        dlwget "${episodeUrl}3_et_4.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_4.mp3"

        addChapitre "${chapitreTitreTbl[5]}" 5;
        dl_postnote="chapitres 5,6";  setTagNoteLivre
        dlwget "${episodeUrl}1_et_6.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_6.mp3"

        addChapitre "${chapitreTitreTbl[7]}" 7;
        dl_postnote="chapitres 7,8";  setTagNoteLivre
        dlwget "${episodeUrl}7_et_8.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_8.mp3"

        addChapitre "${chapitreTitreTbl[9]}" 10;
        dl_postnote="chapitres 9,10";  setTagNoteLivre
        dlwget "${episodeUrl}9_et_10.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_10.mp3"

        addChapitre "${chapitreTitreTbl[11]}" 11;
        dl_postnote="chapitres 11,12";  setTagNoteLivre
        dlwget "${episodeUrl}11_et_12.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_12.mp3"

        addChapitre "${chapitreTitreTbl[15]}" 15;
        dlwget "${episodeUrl}15.mp3" "$livreTitreNormalizePartieChapitre.mp3"
    }    
    livreContent
    closeLivre
}

Verne_1898_Le_Secret_de_Wilhelm_Storitz(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Le Secret de Wilhelm Storitz";           tag_title="$tag_album";
    tag_year=1898;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="DanielLuttringer";                         dl_source="www.litteratureaudio.com";
    dl_duree="5h36";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-le-secret-de-wilhelm-storitz.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Le_Secret_de_Wilhelm_Storitz

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/%27The_Secret_of_William_Storitz%27_by_George_Roux_02.jpg/250px-%27The_Secret_of_William_Storitz%27_by_George_Roux_02.jpg"

        # - audios - #
    
        initChapitre 1 19
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Le_Secret_de_Wilhelm_Storitz_Chap"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$dl_date-$livreTitreNormalize.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1901_Les_Histoires_de_Jean_Marie_Cabidoulin(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Les Histoires de Jean-Marie Cabidoulin ";tag_title="$tag_album";
    tag_year=1901;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Pomme";                                   dl_source="www.litteratureaudio.com";
    dl_duree="h";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-les-histoires-de-jean-marie-cabidoulin.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Les_Histoires_de_Jean-Marie_Cabidoulin

        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/xLes_Histoires_de_Cabidoulin.jpg.pagespeed.ic.eKSc560OHr.webp"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/%27The_Sea_Serpent%27_by_George_Roux_20.jpg/250px-%27The_Sea_Serpent%27_by_George_Roux_20.jpg"

        # - audios - #
        initChapitre 1 15
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_Histoires_de_Jean_Marie_Cabidoulin_Chap"
        for chapitreNo in 1 3 5 7 9 11
        do
            addChapitre "" $chapitreNo;
            local incChapitreNo=$((chapitreNo + 1))
            dl_postnote="chapitres $chapitreNo et $incChapitreNo"
            setTagNoteLivre
            dlwget "${episodeUrl}${chapitreNo}_et_${incChapitreNo}.mp3" "$livreTitreNormalizeChapitre-et_chapitre_$incChapitreNo.mp3"
        done    
        # 13-14-15
        addChapitre "" 13;
        dl_postnote="épisode 13 14 15"
        setTagNoteLivre
        dlwget "${episodeUrl}13_a_15.mp3" "$livreTitreNormalizeChapitre-et_chapitre_14_15.mp3"
    }
    livreContent
    closeLivre
}

Verne_1901_La_Chasse_au_meteore(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="La chasse au météore";                   tag_title="$tag_album";
    tag_year=1901;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Arthur";                                   dl_source="www.litteratureaudio.com";
    dl_duree="7h15";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-la-chasse-au-meteore.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/La_Chasse_au_m%C3%A9t%C3%A9ore
        # https://m.21-bal.com/law/9591/index.html

        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/x1280px-Van_Gogh_-_Starry_Night_-_Google_Art_Project.jpg.pagespeed.ic.2FWuaiqgTY.webp"

        # - audios - #
        initChapitre 1 21
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_La_Chasse_au_meteore_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Dans lequel le juge John Proth remplit un des plus agréables devoirs de sa charge avant de retourner à son jardin"
        chapitreTitreTbl[2]="Qui introduit le lecteur dans la maison de Dean Forsyth et le met en rapport avec son neveu, Francis Gordon, et sa bonne, Mitz"
        chapitreTitreTbl[3]="Où il est question du docteur Sydney Hudelson, de sa femme, Mrs Flora Hudelson, de miss Jenny et de miss Loo, leurs deux filles"
        chapitreTitreTbl[4]="Comment deux lettres envoyées, l’une à l’Observatoire de Pittsburg, l’autre à l’Observatoire de Cincinnati, furent classées dans le dossier des bolides"
        chapitreTitreTbl[5]="Dans lequel malgrè leur acharnement Mr Dean Forsyth et le docteur Hudelson n'ont que par les journaus des nouvelles de leur météore"
        chapitreTitreTbl[6]="Qui contient quelques variations plus ou moins fantaisistes sur les météores en général, et en particulier sur le bolide dont MM. Forsyth et Hudelson se disputent la découverte"
        chapitreTitreTbl[7]="Dans lequel on verra Mrs Hudelson très chagrine de l’attitude du docteur, et où l’on entendra la bonne Mitz rabrouer son maître d’une belle manière"
        chapitreTitreTbl[8]="Dans lequel des polémiques de presse aggravent la situation, et qui se termine par une constatation aussi certaine qu’inattendue"
        chapitreTitreTbl[9]="Dans lequel les journaux, le public, Mr Dean Forsyth et le docteur Hudelson font une orgie de mathématiques"

        chapitreTitreTbl[10]="Dans lequel il vient une idée et même deux idées à Zéphyrin Xirdal"
        chapitreTitreTbl[11]="Dans lequel Mr Dean Forsyth et le docteur Hudelson éprouvent une violente émotion"
        chapitreTitreTbl[12]="Où l’on voit Mrs Arcadia Stanfort attendre à son tour, non sans une vive impatience, et dans lequel Mr John Proth se déclare incompétent"
        chapitreTitreTbl[13]="Dans lequel on voit, comme l’a prévu le juge Proth, surgir le troisième larron, bientôt suivi d’un quatrième"
        chapitreTitreTbl[14]="Dans lequel la veuve Thibaut, en s’attaquant inconsidérément aux plus hauts problèmes de la mécanique céleste, cause de graves soucis au banquier Robert Lecœur"
        chapitreTitreTbl[15]="Où J. B. K. Lowenthal désigne le gagnant du gros lot"
        chapitreTitreTbl[16]="Dans lequel on voit nombre de curieux profiter de cette occasion d’aller au Groenland et d’assister à la chute de l’extraordinaire météore"
        chapitreTitreTbl[17]="Dans lequel le merveilleux bolide et un passager du « Mozik » rencontrent, celui-ci, un passager de l’« Oregon », et celui-là, le globe terrestre"
        chapitreTitreTbl[18]="Où, pour atteindre le bolide, M. de Schnack et ses nombreux complices commettent les crimes d’escalade et d’effraction"
        chapitreTitreTbl[19]="Dans lequel Zéphyrin Xirdal éprouve pour le bolide une aversion croissante, et ce qui s’ensuit"

        chapitreTitreTbl[20]="Qu’on lira peut-être avec regret, mais que son respect de la vérité historique a obligé l’auteur à écrire, tel que l’enregistreront un jour les annales astronomiques"
        chapitreTitreTbl[21]="Dernier chapitre, qui contient l’épilogue de cette histoire et dans lequel le dernier mot reste à Mr John Proth, juge à Whaston"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done    
    }
    livreContent
    closeLivre
}

Verne_1909_Les_Naufrages_du_Jonathan(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Les naufrages du Jonathan";              tag_title="$tag_album";
    tag_year=1909;                                      dl_date="$tag_year";
    dl_editeur="Magasin d'Éducation et de Récréation"
	dl_voix="Vincent de l’Épine";                       dl_source="www.litteratureaudio.com";
    dl_duree="17h16";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-les-naufrages-du-jonathan.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Les_Naufrag%C3%A9s_du_%C2%AB_Jonathan_%C2%BB
        # https://fr.wikisource.org/wiki/Les_Naufrag%C3%A9s_du_Jonathan (titre des chapitres et textes)

        # - cover - #
        dlCover "http://www.litteratureaudio.com/img/xLes_Naufrages_Du_Jonathan2.jpg.pagespeed.ic.tNkNHjbKdT.webp"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/Hetzel-Naufrages-Jonathan-1909.jpg/250px-Hetzel-Naufrages-Jonathan-1909.jpg"

        # - audios - #
        partieNb=3
        
        # - partie 1- #
        partieNo=1;
        initChapitre 1 5
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_Naufrages_Du_Jonathan_Part01_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Le Guanaque"
        chapitreTitreTbl[2]="Mystérieuse existence"
        chapitreTitreTbl[3]="La fin d'un pays libre"
        chapitreTitreTbl[4]="À la côte "
        chapitreTitreTbl[5]="Les naufragés"
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

        # - partie 2 - #
        partieNo=2;
        initChapitre 1 11
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_Naufrages_Du_Jonathan_Part02_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="À terre"
        chapitreTitreTbl[2]="La première loi"
        chapitreTitreTbl[3]="À la baie Scotchwell"
        chapitreTitreTbl[4]="Hivernage"
        chapitreTitreTbl[5]="Un navire en vue"
        chapitreTitreTbl[6]="Libres"
        chapitreTitreTbl[7]="La première enfance d'un peuple"
        chapitreTitreTbl[8]="Halg et Sirk"
        chapitreTitreTbl[9]="Le deuxième hiver"

        chapitreTitreTbl[10]="Du sang"
        chapitreTitreTbl[11]="Un chef"
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

        # - partie 3 - #
        partieNo=3;
        initChapitre 1 15
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_Naufrages_Du_Jonathan_Part03_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Premières mesures"
        chapitreTitreTbl[2]="La cité naissante"
        chapitreTitreTbl[3]="L'attentat"
        chapitreTitreTbl[4]="Dans les grottes"
        chapitreTitreTbl[5]="Un héros"
        chapitreTitreTbl[6]="Pendant dix-huit mois"
        chapitreTitreTbl[7]="L'invasion"
        chapitreTitreTbl[8]="Un traître"
        chapitreTitreTbl[9]="La patrie hostelienne "

        chapitreTitreTbl[10]="Cinq ans après"
        chapitreTitreTbl[11]="La fièvre de l'or"
        chapitreTitreTbl[12]="L'île au pillage"
        chapitreTitreTbl[13]="Une « journée »"
        chapitreTitreTbl[14]="L'abdication"
        chapitreTitreTbl[15]="Seul!"
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done
    }    

    livreContent
    closeLivre
}

Verne_1910_Le_Humbug_Moeurs_Americaines(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Nouvelle";
    tag_album="hier_et_demain";                         tag_title="Le_Humbug Moeurs Americaines";
    tag_year=1910;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="René Depasse";                             dl_source="www.litteratureaudio.com";
    dl_duree="1h15";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-le-humbug-moeurs-americaines.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # pas de lien wiki
    
    	# - cover - #
        dlCover "http://www.litteratureaudio.com/img/xJules_Verne_-_Le_Humbug.jpg.pagespeed.ic.1wDlJz89OH.webp"

        # - audios - #
        local episodeUrl="http://www.litteratureaudio.org/mp3/Verne_-_Humbug"
        dlwget "http://www.litteratureaudio.org/mp3/Verne_-_Humbug.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

Verne_1886_Aventures_de_la_famille_Raton(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="hier_et_demain";                         tag_title="Aventures de la famille Raton";
    tag_year=1886;                                      dl_date="$tag_year";
    dl_editeur="Figaro illustré"
	dl_voix="Domi";                                     dl_source="www.litteratureaudio.com";
    dl_duree="1h47";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-aventures-de-la-famille-raton.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"

    livreContent(){
        # https://fr.wikipedia.org/wiki/La_Famille_Raton
    
    	# - cover - #
        dlCover "http://www.litteratureaudio.com/img/xJules_Verne_-_La_Famille_Raton.jpg.pagespeed.ic.O3-f1eCMbS.jpg"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/%27Aventures_de_la_famille_Raton%27_by_Felicien_de_Myrbach_01.jpg/250px-%27Aventures_de_la_famille_Raton%27_by_Felicien_de_Myrbach_01.jpg" "$livreTitreNormalize-wiki"

        # - audios - #
        initChapitre 1 3
        addChapitre "" 1;dlwget "http://www.litteratureaudio.org/mp3/VERNE_Jules_Aventures_de_la_famille_Raton_fich01.mp3" "$livreTitreNormalize-1-3.mp3"
        addChapitre "" 2;dlwget "http://www.litteratureaudio.org/mp3/VERNE_Jules_Aventures_de_la_famille_Raton_fich02.mp3" "$livreTitreNormalize-2-3.mp3"
        addChapitre "" 3;dlwget "http://www.litteratureaudio.org/mp3/VERNE_Jules_Aventures_de_la_famille_Raton_fich03.mp3" "$livreTitreNormalize-3-3.mp3"
    }
    livreContent
    closeLivre
}

Verne_1893_Monsieur_Re_Dieze_et_mademoiselle_Mi_Bemol(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="hier_et_demain";                         tag_title="Monsieur Re-Dieze et mademoiselle Mi-Bemol";
    tag_year=1893;                                      dl_date="$tag_year.12.25";
    dl_editeur="Hetzel"
	dl_voix="René Depasse";                             dl_source="www.litteratureaudio.com";
    dl_duree="1h18";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-monsieur-re-dieze-et-mademoiselle-mi-bemol.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/M._R%C3%A9-Di%C3%A8ze_et_Mlle_Mi-B%C3%A9mol

        # - cover- #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Verne-Bemol.jpg/250px-Verne-Bemol.jpg"

        # - audios - #
        dlwget "http://www.litteratureaudio.net/mp3/Jules_Verne_-_Monsieur_Re_Diese_et_Mademoiselle_Mi_Bemol2.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

Verne_1910_La_Destinee_de_Jean_Morenas(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="hier_et_demain";                         tag_title="La Destinée de Jean Morenas";
    tag_year=1910;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="René Depasse";                             dl_source="www.litteratureaudio.com";
    dl_duree="1h30";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-la-destinee-de-jean-morenas.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"

    livreContent(){
        # https://fr.wikipedia.org/wiki/La_Destin%C3%A9e_de_Jean_Mor%C3%A9nas
        # https://www.atramenta.net/lire/oeuvre29989-chapitre-30.html
    
        # - cover- #
        dlCover "http://www.litteratureaudio.com/img/xJules_Verne_-_La_Destinee_de_Jean_Morenas.jpg.pagespeed.ic.y6wOkJ0sfk.jpg"

        # - audios - #
        partieNb=2
        initChapitre 1 9
        local episodeUrl="http://www.litteratureaudio.org/mp3/Verne_-_La_destinee_de_Jean_Morenas_01_Premiere_partie"
        dl_postnote="Ce récit a été totalement réécrit par Michel Verne, à partir d'une autre nouvelle de jeunesse de son père, Pierre-Jean, que ce dernier conçut vers 1852"
        dlwget "http://www.litteratureaudio.org/mp3/Verne_-_La_destinee_de_Jean_Morenas_01_Premiere_partie.mp3" "$livreTitreNormalize-P1-2.mp3"
        dlwget "http://www.litteratureaudio.org/mp3/Verne_-_La_destinee_de_Jean_Morenas_02_Deuxieme_partie.mp3" "$livreTitreNormalize-P2-2.mp3"
    }
    livreContent
    closeLivre
}

Verne_1910_La_Journee_d_un_journaliste_americain_en_2890(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="hier_et_demain";                         tag_title="La Journée d un journaliste américain en 2890";
    tag_year=1889;                                      dl_date="$tag_year.02";
    dl_editeur="The Forum"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="0h41";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-la-journee-dun-journaliste-americain-en-2890.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/La_Journ%C3%A9e_d%27un_journaliste_am%C3%A9ricain_en_2889

        # - cover- #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Verne-29-stoleti.JPG/250px-Verne-29-stoleti.JPG"

        # - audios - #

        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_La_journee_dun_journaliste_americain_en_2890.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

Verne_1910_L_Eternel_Adam(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Nouvelles";
    tag_album="hier_et_demain";                         tag_title="L_Eternel_Adam";
    tag_year=1910;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Bernard";                                 dl_source="www.litteratureaudio.com";
    dl_duree="1h28";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-leternel-adam.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/L%27%C3%89ternel_Adam
        # - cover- #
        dlCover "http://www.litteratureaudio.com/img/xJules_Verne_-_L_Eternel_Adam_2.jpg.pagespeed.ic.0b1X-2e2us.jpg"
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Verne-Adam.jpg/200px-Verne-Adam.jpg" "$livreTitreNormalize-wiki"

        # - audios - #
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_L_Eternel_Adam.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

hier_et_demain(){
    showDebug "\n$FUNCNAME($*)"
} #hier_et_demain

Verne_1889_Discours_d_inauguration_du_cirque_municipal_d_Amiens(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Discours_d_inauguration_du_cirque_municipal_d_Amiens";
    tag_title="$tag_album";
    tag_year=1889;                                      dl_date="$tag_year.06.26";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="0h25";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-discours-dinauguration-du-cirque-municipal-damiens.html"

    createLivre  "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Jules_Verne (une partie en parle)

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Cirque_Amiens_foule_d%C3%A9but_XXe.jpg/220px-Cirque_Amiens_foule_d%C3%A9but_XXe.jpg" # cirque d'amiens

        # - audios - #
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_Discours_dinauguration_du_cirque_municipal_damiens.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

Verne_1904_Maitre_du_Monde(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Maitre du monde";                        tag_title="$tag_album";
    tag_year=1904;                                      dl_date="$tag_year".07.01;
    dl_editeur="Magasin d'éducation et de récréation"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="5h37";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-maitre-du-monde.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Ma%C3%AEtre_du_Monde

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/9/91/%27Master_of_the_World%27_by_George_Roux_21.jpg"

        # - audios - #
        initChapitre 1 18
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Maitre_du_Monde_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]=""
        chapitreTitreTbl[2]=""
        chapitreTitreTbl[3]=""
        chapitreTitreTbl[4]=""
        chapitreTitreTbl[5]=""
        chapitreTitreTbl[6]=""
        chapitreTitreTbl[7]=""
        chapitreTitreTbl[8]=""
        chapitreTitreTbl[9]=""

        chapitreTitreTbl[10]=""
        chapitreTitreTbl[11]=""
        chapitreTitreTbl[12]=""
        chapitreTitreTbl[13]=""
        chapitreTitreTbl[14]=""
        chapitreTitreTbl[15]=""
        chapitreTitreTbl[16]=""
        chapitreTitreTbl[17]=""
        chapitreTitreTbl[18]=""
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            dl_postnote="Suite de Robur"
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done    

    }
    livreContent
    closeLivre
}
