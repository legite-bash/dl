#!/bin/bash
set -u

# - Monsieur_Phi - #
Monsieur_Phi(){
    showDebug "$FUNCNAME($*)"
    createCollection "/media/mediatheques/docutheques/videos/Philosophies/Monsieur_Phi"
    if [ $? -ne 0 ]; then return 1;fi
    SITE_URL="https://www.youtube.com/channel/UCqA8H22FwgBVcF3GJpp0MQw"

    setYoutubeFormat 18
    #Monsieur_Phi_2016
    #Monsieur_Phi_2017_01_09
    #Monsieur_Phi_2017_10
    #Monsieur_Phi_2017_11_12
    #Monsieur_Phi_2018_01_06
    #Monsieur_Phi_2018_07_12
    #Monsieur_Phi_2019_01_06
    Monsieur_Phi_2019_07

    closeCollection
}


Monsieur_Phi_2016(){
    albumActuel="2016"
    createAlbum "$albumActuel"

    codeYoutube="SR7DFabsU48"; setTitre "2016.08.03-Grain_de_philo-001-Philosophie-onanisme_intellectuel-1-2-PHILOSOPHIE_vs_SCIENCES"
    dlyt $codeYoutube $titre

    codeYoutube="H5VnO59U42o"; setTitre "2016.08.03-Grain_de_philo-001-Philosophie-onanisme_intellectuel-2-2-Un_residu_de_questions_insolubles"
    dlyt $codeYoutube $titre

    codeYoutube="ixbZNpgHjig"; setTitre "2016.08.11-Grain_de_philo-002-Esprit_et_matiere-1-5-Ou_Descarte_te_met_un_gros_doute"
    dlyt $codeYoutube $titre

    codeYoutube="LgoVqA0LphY"; setTitre "2016.08.14-Grain_de_philo-002-Esprit_et_matiere-2-5-Descartes-La_chose_qui_pense"
    dlyt $codeYoutube $titre

    codeYoutube="G2CNSB83Xu4"; setTitre "2016.08.19-Grain_de_philo-002-Esprit_et_matiere-3-5-La_telekinesie_pour_les_nuls"
    dlyt $codeYoutube $titre

    codeYoutube="nNkBxzQAGx4"; setTitre "2016.08.25-Grain_de_philo-002-Esprit_et_matiere-4-5-Malebranche_et_Leibniz-LA_TeLeKINeSIE_EXPLIQUeE"
    dlyt $codeYoutube $titre

    codeYoutube="qyDWSpX3xAk"; setTitre "2016.08.30-Grain_de_philo-002-Esprit_et_matiere-5-5-Materialisme_et_terminator"
    dlyt $codeYoutube $titre

    codeYoutube="AtTTn7KMIys"; setTitre "2016.09.08-Argument_frappant-001-Sommes-nous_des_simulations-L_argument_de_la_simulation_de_Nick_Bostrom"
    dlyt $codeYoutube $titre

    codeYoutube="6U9WBN0Sck8"; setTitre "2016.09.16-Grain_de_philo-003-Religion_et_morale-1-2-Si_Dieu_n'existe_pas-tout_est_permis"
    dlyt $codeYoutube $titre

    codeYoutube="Ffd55-hAaGE"; setTitre "2016.09.28-Grain_de_philo-003-Religion_et_morale-2-2-La_religion_empeche_t_elle_d_etre_moral"
    dlyt $codeYoutube $titre

    codeYoutube="mt4JXMcxlwI"; setTitre "2016.10.16-Argument_frappant-002-L_argument_du_violoniste-J.J.Thomson"
    dlyt $codeYoutube $titre

    codeYoutube="EvUZ5eToi10"; setTitre "2016.10.31-Grain_de_philo-004-La_NOVLANGUE_dans_1984_de_George_Orwell"
    dlyt $codeYoutube $titre

    codeYoutube="3FOrWMDL8CY"; setTitre "2016.10.13-Argument_frappant-003-La_loi_de_Bayes-1-2"
    dlyt $codeYoutube $titre

    codeYoutube="aU7EWwLtiOg"; setTitre "2016.12.07-Argument_frappant-003-La_loi_de_Bayes-2-2"
    dlyt $codeYoutube $titre

    codeYoutube="azNw7Vx1R8o"; setTitre "2016.12.23-Grain_de_philo-005-ADAM_SMITH-1-2-Le_paradoxe_de_la_veste_de_laine"
    dlyt $codeYoutube $titre

    codeYoutube="ZYlu7xxQogA"; setTitre "2017.01.11-Grain_de_philo-005-ADAM_SMITH-2-2-Division_invisible_et_main_invisible"
    dlyt $codeYoutube $titre

    closeAlbum
}

Monsieur_Phi_2017_01_09(){
    albumActuel="2017"
    createAlbum "$albumActuel"

    codeYoutube="u3lapME67VI"; setTitre "2017.02.11-Grain_de_philo-006-La_liberte_est_un_super_pouvoir-determinisme_et_liberte"
    dlyt $codeYoutube $titre

    codeYoutube="EZmvdkNjev8"; setTitre "2017.02.26-Grain_de_philo-007a-Identite_personnelle-1-2-Teleportation_trous_de_meemoire_et_responsabilite"
    dlyt $codeYoutube $titre

    codeYoutube="HPWQufM2sAg"; setTitre "2017.03.06-Grain_de_philo-007b-Identite_personnelle-2-2-Montez-vous_dans_le_teleporteur"
    dlyt $codeYoutube $titre

    codeYoutube="8SOGL5abq1Y"; setTitre "2017.02.11-Argument_frappant-004-Pour_e-penser_une_seconde_fois-La_paradoxe(du_condamne_mort"
    dlyt $codeYoutube $titre

    codeYoutube="_8DW9L-W73U"; setTitre "2017.05.04-Grain_de_philo-008-GYEONGBOKGUNG-Le_bateau_de_Thesee"
    dlyt $codeYoutube $titre

    codeYoutube="hI89r4LqaCc"; setTitre "2017.05.24-Grain_de_philo-009a-Le_principe_de_condorcet-1-2-Une_norme_democratique"
    dlyt $codeYoutube $titre

    codeYoutube="pwvJdaJRZ5Q"; setTitre "2017.06.05-Bac_philo-001-5_conseils_pour_ne_pas_rater_sa_dissertation"
    dlyt $codeYoutube $titre

    codeYoutube="aaIVuqSke58"; setTitre "2017.06.20-Annonce_FAQ_Tipeee_Blog-"
    dlyt $codeYoutube $titre

    codeYoutube="ZZb4TjvupkI"; setTitre "2017.02.11-Grain_de_philo-009b-Le_principe_de_condorcet-2-2-Macron_a_t_il_ete_mal_elu"
    dlyt $codeYoutube $titre

    codeYoutube="HAlayHY3Z8E"; setTitre "2017.07.14-Argument_frappant-005-Avantages_comparatifs_et_cout_d_opportunite"
    dlyt $codeYoutube $titre

    codeYoutube="e_mQ98SJu6A"; setTitre "2017.07.19-VOTEZ_pour_le_sujet_de_la_prochaine_video"
    dlyt $codeYoutube $titre

    codeYoutube="NUuL7g-r8GY"; setTitre "2017.07.23-Grain_de_philo-010-3_idees_recut_sur_la_philosophie"
    dlyt $codeYoutube $titre

    codeYoutube="4kRY6vp99iY"; setTitre "2017.08.04-Grain_de_philo-011a-Genealogie_de_la_morale-1-2-La_morale_des_winners"
    dlyt $codeYoutube $titre

    codeYoutube="XAaPkiiUTeI"; setTitre "2017.08.14-Grain_de_philo-011b-Genealogie_de_la_morale-2-2-NIETZSCHE_et_les_mechants"
    dlyt $codeYoutube $titre

    codeYoutube="KAG149QCduI"; setTitre "2017.08.15-Grain_de_philo-VOTEZ_pour_le_sujet_de_septembre"
    dlyt $codeYoutube $titre

    codeYoutube="7KmAKVaO-Xc"; setTitre "2017.08.20-Grain_de_philo-012-A_chacun_sa_morale-Relativisme_vs_realisme"
    dlyt $codeYoutube $titre

    codeYoutube="LocE6eog_z4"; setTitre "2017.08.28-Axiome-002-Le_paradoxe_de_la_Belle_au_bois_dormant"
    dlyt $codeYoutube $titre

    codeYoutube="AZBDMN5wZ-8"; setTitre "2017.09.04-Argument_frappant-006a-7_experiences_de_pensee-Science4All"
    dlyt $codeYoutube $titre

    codeYoutube="9-ILAnAW8us"; setTitre "2017.09.13-Grain_de_philo-013-Realisme_scientifique-1-2-Etes_vous_assis_sur_des_elections"
    dlyt $codeYoutube $titre

    codeYoutube="45OF-BBWmRs"; setTitre "2017.09.16-VOTEZ_pour_le_sujet_d_octobre"
    dlyt $codeYoutube $titre

    codeYoutube="y2l8lxi-9aY"; setTitre "2017.09.22-Argument_frappant-006b-7_experiences_de_pensee-Pour_un_peu_plus_d_utilitarisme"
    dlyt $codeYoutube $titre

    closeAlbum
}


Monsieur_Phi_2017_10(){
    albumActuel="2017"
    createAlbum "$albumActuel"

    codeYoutube="mPBC0OQKa2"; setTitre "2017.10.01-Axiome-003-Retour_sur_l_utilitarisme"
    dlyt $codeYoutube $titre

    codeYoutube="2DOYvDWZWwo"; setTitre "2017.10.06-Grain_de_philo-014-Demonstration_et_connaissance-1-3-Comment_demontrer_n_importe_quoi"
    dlyt $codeYoutube $titre

    codeYoutube="Gqu6Di353ok"; setTitre "2017.10.13-Grain_de_philo-014-Demonstration_et_connaissance-2-3-Apprenez_a_ne_rien_savoir"
    dlyt $codeYoutube $titre

    #codeYoutube=""; setTitre "2017.10.-Grain_de_philo-0-Demonstration_et_connaissance-3-3-"
    dlyt $codeYoutube $titre

    #codeYoutube=""; setTitre "2017.10.-Grain_de_philo-0-"
    dlyt $codeYoutube $titre

    codeYoutube="H31sBuq22cM"; setTitre "2017.10.17-VOTEZ_pour_le_sujet_de_novembre"
    dlyt $codeYoutube $titre

    codeYoutube="c86BtYw21RM"; setTitre "2017.10.25-Axiome-003-Le_paradoxe_de_Bertrand-extrait"
    dlyt $codeYoutube $titre

    closeAlbum
}


Monsieur_Phi_2017_11_12(){
    albumActuel="2017"
    createAlbum "$albumActuel"

    codeYoutube="alGQ-g2-FxM"; setTitre "2017.11.02-CERVEAU,CONSCIENCE,INCONSCIENT-1"
    dlyt $codeYoutube $titre

    codeYoutube="3hCffvguLTQ"; setTitre "2017.11.15-Grain_de_philo-016-CONSÉQUENTIALISME-Quel_est_le_but_de_la_morale"
    dlyt $codeYoutube $titre

    codeYoutube="ngi4E1_FDsE"; setTitre "2017.11.18-VOTEZ-série_et_philosophie"
    dlyt $codeYoutube $titre

    codeYoutube="J5dTXVbS3Vg"; setTitre "2017.11.25-Grain_de_philo-014-3-LE FONDATIONNALISME - Quelle base pour l'édifice des connaissances"
    dlyt $codeYoutube $titre

    codeYoutube="enZpq8jvFEs"; setTitre "2017.11.11-Grain_de_philo-014-4-L'AXIOMATIQUE - Les Éléments d'Euclide"
    dlyt $codeYoutube $titre

    codeYoutube="5ZMeRn_qae0"; setTitre "2017.12.18-VOTEZ pour une expérience de pensée"
    dlyt $codeYoutube $titre

    codeYoutube="j5EEHOokkaQ"; setTitre "2017.12.27-Grain_de_philo-014-5-LE PARADOXE DE LEWIS CARROLL"
    dlyt $codeYoutube $titre

    closeAlbum
}


Monsieur_Phi_2018_01_06(){
    albumActuel="2018"
    createAlbum "$albumActuel"

    codeYoutube="M1GFlVtk1yQ"; setTitre "2018.01.03-TOUT CE QUI N'EST POINT PROSE, EST VERS"
    dlyt $codeYoutube $titre

    codeYoutube="2FVwXaNWPQ0"; setTitre "2018.01.18-Argument_frappant-07-LA MACHINE a EXPeRIENCE de Robert Nozick"
    dlyt $codeYoutube $titre

    codeYoutube="iQ-AWuLk32c"; setTitre "2018.02.05-Grain_de_philo-014-6-LA RÈGLE DES RÈGLES"
    dlyt $codeYoutube $titre

    codeYoutube="XeZDt43Pij8"; setTitre "2018.02.13-Grain_de_philo-017-JE N'EXISTE PAS"
    dlyt $codeYoutube $titre

    codeYoutube="eN2-1J2RvO0"; setTitre "2018.03.01-Grain_de_philo-PopPhi-01-BLACK MIRROR, la série qui ne voulait pas qu'on la regarde"
    dlyt $codeYoutube $titre

    codeYoutube="Z1UwoMw9jZY"; setTitre "2018.03.12-Grain_de_philo-018-La mort"
    dlyt $codeYoutube $titre

    codeYoutube="t9-zeCYvY5c"; setTitre "2019.03.16-JEUX VIDÉO & PHILOSOPHIE"
    dlyt $codeYoutube $titre

    codeYoutube="2qLstbRHzmk"; setTitre "2018.03.22-Sciences vs. Philosophie-Stephen Hawking - LA PHILOSOPHIE EST MORTE"
    dlyt $codeYoutube $titre

    codeYoutube="wiYdEDDRQzE"; setTitre "2018.04.01-Argument_frappant-08-Le PARADOXE DU MENTEUR et le THÉORÈME DE TARSKI"
    dlyt $codeYoutube $titre

    codeYoutube="ar2bHjkL6XM"; setTitre "2018.04.14-Grain_de_philo-015-2-PROSOPAGNOSIE et DÉLIRE DES SOSIES"
    dlyt $codeYoutube $titre

    codeYoutube="nvDcEpG7WU4"; setTitre "2018.04.20-Grain_de_philo-Bac_philo-01- L'art des réponses longues et intelligentes aux questions brèves et stupides"
    dlyt $codeYoutube $titre

    codeYoutube="IDL587pZnNk"; setTitre "2018.05.01-PopPhi-02-LA LIBERTÉ EN JEU"
    dlyt $codeYoutube $titre

    codeYoutube="NVM3lMcVJ_Y"; setTitre "2018.06.06-Grain_de_philo-VOTE-série et philosophie-ANNONCES-Discord, conférences, live"
    dlyt $codeYoutube $titre

    codeYoutube="J1zCeYXMtNs"; setTitre "2018.05.10-Grain_de_philo-BAC PHILO-02-Peut-on-Faut-il-Doit-on"
    dlyt $codeYoutube $titre

    codeYoutube="frG47p887BE"; setTitre "2018.05.20-Conférence sur la SUPER-INTELLIGENCE"
    dlyt $codeYoutube $titre

    codeYoutube="0tFyva7zkZ8"; setTitre "2018.06.26-Bac_philo-Pourquoi"
    dlyt $codeYoutube $titre

    codeYoutube="ECipIfdJqyI"; setTitre "2018.06.01-Bac_philo-04-4 conseils pour l'ANALYSE DE SUJET"
    dlyt $codeYoutube $titre

    codeYoutube="J1TjSLiiIuw"; setTitre "-Grain_de_philo-Le plan"
    dlyt $codeYoutube $titre

    codeYoutube="dlznHTgArF0"; setTitre "2018.06.12-Bac_philo-06-Terminons par l'INTRO"
    dlyt $codeYoutube $titre

    codeYoutube="UUTvanQ5lTs"; setTitre "2018.06.14-Bac_philo-07-Conclusion"
    dlyt $codeYoutube $titre

    codeYoutube="m8pwDJlslsA"; setTitre "2018.06.26-Grain_de_philo-19-VOULONS-NOUS MOURIR"
    dlyt $codeYoutube $titre
    closeAlbum
}
 

Monsieur_Phi_2018_07_12(){
    albumActuel="2018"
    createAlbum "$albumActuel"

    codeYoutube="abjiHI8qYok"; setTitre "2018.07.06-Dixit-01-Le gros livre de Wittgenstein"
    dlyt $codeYoutube $titre
 
    codeYoutube="Pn-2GQbgQhY"; setTitre "2018.07.12-FOOTBALL_et_UTILITARISME-Qui DOIT gagner la coupe du monde"
    dlyt $codeYoutube $titre
 
    codeYoutube="b2_gqtr2HgA"; setTitre "2018.07.18-Grain_de_philo-19-FAQ de la mort"
    dlyt $codeYoutube $titre
 
    codeYoutube="NpUcdhbcmuI"; setTitre "2018.07.27-Dixit-02-John Stuart Mill-Il vaut mieux être Socrate insatisfait qu'un imbécile satisfait"
    dlyt $codeYoutube $titre
 
    codeYoutube="0sPxyll_Avo"; setTitre "2018.08.13-Grain_de_philo-20-De quoi la RÉALITÉ est-elle le nom "
    dlyt $codeYoutube $titre
 
    codeYoutube="EdK4VuPl3rA"; setTitre "2018.08.19-Grain_de_philo-Grain_de_philo-20-b-FAQ de la réalité"
    dlyt $codeYoutube $titre
 
    codeYoutube="HaVWbdlAiCQ"; setTitre "2018.09.01-Argument_frappant-09a-ÉTHIQUE ANIMALE-la probabilité d'une catastrophe"
    dlyt $codeYoutube $titre
 
    codeYoutube="fbpfMHKccMo"; setTitre "2018.09.13-Argument_frappant-09b-FAQ des animaux"
    dlyt $codeYoutube $titre
     
    codeYoutube="JefwhxJWmik"; setTitre "2018.10.01-L'argument de la Bugatti-Peter Singer et l'altruisme efficace"
    dlyt $codeYoutube $titre
     
    codeYoutube="LafrIPLgHCA"; setTitre "2018.10.14-Argument_frappant-09-2-L'incertitude sur le RÉALISME MORAL mène-t-elle au réalisme moral"
    dlyt $codeYoutube $titre
     
    codeYoutube="USgRKvEMyIw"; setTitre "2018.10.28-Argument_frappant-09-2b-FAQ de la méta-éthique"
    dlyt $codeYoutube $titre
    
    codeYoutube="aUz5gD2s"; setTitre "2018.11.11-Grain_de_philo-21-La VÉRITÉ nue"
    dlyt $codeYoutube $titre
    
    codeYoutube="SXLHijQeYok"; setTitre "2018.11.27-Grain_de_philo-22-La théorie peut-elle réfuter l'expérience"
    dlyt $codeYoutube $titre
    
    codeYoutube="hV3IYoHpQS4"; setTitre "2018.12.16-Argument_frappant-10-Pas de cadeaux pour Noel"
    dlyt $codeYoutube $titre
    
    codeYoutube="meNQnNqHjes"; setTitre "2018.12.25-Grain_de_philo-23-Merci Captain Ad Hoc"
    dlyt $codeYoutube $titre
    
    closeAlbum
}

Monsieur_Phi_2019_01_06(){
    #https://www.youtube.com/watch?v=2qxtdzbs2ag&index=1&list=PLqDeN75S4o4va_JT-aySud1iU3PzE9a3t
    albumActuel="2019"
    createAlbum "$albumActuel"

    codeYoutube="d1znpwDI9IU"; setTitre "2019.01.02-Lecture-Greg Egan-En apprenant à être moi"
    dlyt $codeYoutube $titre
    
    codeYoutube="5dS1D_X5xRg"; setTitre "2019.02.23-Grain_de_philo-24-La montre d'Einstein-sur le RÉALISME en sciences"
    dlyt $codeYoutube $titre

    codeYoutube="tPgmr-6A10M"; setTitre "2019.03.03-PopPhi-SF et philosophie de l'esprit : le cristal de Greg Egan"
    dlyt $codeYoutube $titre

    codeYoutube="8ArJN4CQs84"; setTitre "2019.03.15-Argument_frappant-11-1-Le PARADOXE DES DEUX ENFANTS-dédicace à Science4All "
    dlyt $codeYoutube $titre

    codeYoutube="y4wuaKmqY-g"; setTitre "2019.03.25-caféPhi-01-Tous zombies-François Kammerer et l'illusionnisme dans la philosophie de l'esprit"
    dlyt $codeYoutube $titre

    codeYoutube="vrfaqsb_0LM"; setTitre "2019.03.30-caféPhi-01-François Kammerer - Sommes-nous tous des zombies-Interview complète"
    dlyt $codeYoutube $titre

    codeYoutube="8RwnrNSqa_A"; setTitre "2019.04.06-Dixit-03-David Hume-Une affirmation extraordinaire requiert PLUS que des preuves extraordinaires"
    dlyt $codeYoutube $titre

    codeYoutube="NO_FM2FygUI"; setTitre "2019.04.20-Argument_frappant-11-2-RETOUR SUR LE PARADOXE DES DEUX ENFANTS-et le Monty Hall"
    dlyt $codeYoutube $titre

    codeYoutube="D2NMITS1YO4"; setTitre "2019.05.04-Lecture] Greg Egan-Des raisons d'être heureux"
    dlyt $codeYoutube $titre

    codeYoutube="ayyrtMdPyi4"; setTitre "2019.05.17-Grain_de_philo-25-Pourquoi se disputer sur le sens des mots"
    dlyt $codeYoutube $titre

    codeYoutube="U6EP2ecu2As"; setTitre "2019.06.02-Grain_de_philo-26-Definir-Definir"
    dlyt $codeYoutube $titre

    codeYoutube="YVLDNh-XDhg"; setTitre "2019.06.06-BacPhilo-Comment réviser sur YouTube"
    dlyt $codeYoutube $titre

    codeYoutube="FHTw_R9M8Mg"; setTitre "2019.06.17-Bac2019-La pluralité des cultures fait-elle obstacle à l'unité du genre humain"
    dlyt $codeYoutube $titre

    codeYoutube="F5G6jgm5g_8"; setTitre "2019.06.28-caféPhi-2-BAC PHILO-UNE LOTERIE-avec Frédéric Le Plaine"
    dlyt $codeYoutube $titre

    closeAlbum
}

Monsieur_Phi_2019_07(){
    albumActuel="2019"
    createAlbum "$albumActuel"
    
    codeYoutube="Z_Uo3Mf_Emc"; setTitre "2019.07.05-caféPhi-2-Philosophie au lycée-reforme et nouveau programme-avec Frédéric Le Plaine"
    dlyt $codeYoutube $titre

    closeAlbum
}
