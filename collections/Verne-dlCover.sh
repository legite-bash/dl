#!/bin/bash
set -u

# - Verne - #
echo ""


#SITE_URL="http://www.litteratureaudio.com/livres-audio-gratuits-mp3/tag/jules-verne"

# - references - #
#https://fr.wikipedia.org/wiki/
#http://www.litteratureaudio.com/livres-audio-gratuits-mp3/tag/jules-verne
#http://www.gutenberg.org/wiki/FR_Litt%C3%A9rature_%28Genre%29#Jules_Verne
#http://beq.ebooksgratuits.com/vents/verne.htm



Verne(){
    showDebug "\n$FUNCNAME($*)"
    #http://www.litteratureaudio.com/livres-audio-gratuits-mp3/tag/jules-verne
    createCollection "/media/docutheques/LivreAudios/Auteurs/"
    if [ $? -ne 0 ]; then return 1;fi

    createAuteur "Verne,Jules"
    if [ $? -ne 0 ]; then return 1;fi

    #setFormat 140 "mp3"
    #formatExt="mp3";

    tag_genre="Vocal";
    tag_genreNo=28; #28.Vocal

    tag_prenote="litteratureaudio.com";
    #setTag_collection  auteur,album(libreNom),titre(chapitre),year

    Verne_litteratureaudio_com_chronologie
    #Verne_litteratureaudio_com_Nouvelles
    #Verne_litteratureaudio_com_Cycles
    #Verne_litteratureaudio_com_Divers

    closeAuteur
}


###############
# Chronologie #
################
Verne_litteratureaudio_com_chronologie(){
    createRepertoire "Chronologie"
    if [ $? -ne 0 ]; then return 1;fi
    #formatExt="mp3";

    Verne_1863_Cinq_semaines_en_ballon
    #Verne_1863_A_propos_du_Geant
        #Les_Aventures_du_capitaine_Hatteras
    #Verne_1864_Voyage_au_centre_de_la_Terre
    #Verne_1865_De_la_Terre_a_la_Lune
    #Verne_1867_Les_Enfants_du_Capitaine_Grant
    #Verne_1869_Vingt_mille_lieues_sous_les_mers
    #Verne_1870_Autour_de_la_Lune
    #Verne_1871_Une_ville_flottante
        ##Les Forceurs de blocus
    ##Aventures de trois Russes et de trois Anglais dans l'Afrique australe
        ##Le Pays des fourrures
    #Verne_1873_Le_Tour_du_monde_en_80_jours
    #Verne_1873_Les_Meridiens_et_le_calendrier
    #Une_fantaisie_du_Docteur_Ox
    #Verne_1874_Un_drame_dans_les_airs
    #Verne_1875_Une_ville_ideale
    #Verne_1875_L_Ile_mysterieuse
        #Le Chancellor • Martin Paz
    #Michel_Strogoff
    #Verne_1877_Hector_Servadac
    #Verne_1877_Les_Indes_noires
        ##Un capitaine de quinze ans
    #Verne_1879_Les_Revoltes_de_la_Bounty
    #Verne_1879_Les_Cinq_Cents_Millions_de_la_Begum
    #Verne_1879_Les_Tribulations_d_un_Chinois_en_Chine
    #Verne_1880_La_maison_a_vapeur
        #La Jangada
        # L'École des Robinsons
    #Verne_1882_Le_Rayon_vert
    # Dix Heures en chasse
    # Kéraban-le-Têtu
    #Verne_1884_L_Etoile_du_sud
        #L'Archipel en feu
        #Mathias Sandorf
        #Un billet de loterie
    #Verne_1884_Frritt_Flacc
    #Verne_1885_lepave_du_cynthia
    #Verne_1886_Robur_le_Conquerant
        #Nord contre Sud
        #Le Chemin de France
        #Gil Braltar
    #Verne_1888_Deux_Ans_de_vacances
        #Famille-Sans-Nom
    #Verne_1889_Sans_dessus_dessous
    #Verne_1890_Cesar_Cascabel
        #Mistress Branican
        #Claudius Bombarnac
    #Verne_1891_Ptit_Bonhomme
        #Mirifiques Aventures de maître Antifer
    #Verne_1892_Le_Chateau_des_Carpathes
    #Verne_1895_L_Ile_a_helice
        #Face au drapeau
        #Clovis Dardentor
    #Verne_1897_Le_Sphinx_des_Glaces
        #Le Superbe Orénoque
        #Le Testament d'un excentrique
        #Seconde Patrie
        #Le Village aérien
    #Verne_1901_Les_Histoires_de_Jean_Marie_Cabidoulin
        #Les Frères Kip
        #Bourses de voyage
        #Un drame en Livonie
    #_1998_Le_Secret_de_Wilhelm_Storitz

    #Maitre_du_Monde
        #L'Invasion de la mer
        #Le Phare du bout du monde
        #Le Volcan d'or
        #L'Agence Thompson and Co
    
    #Verne_1901_La_Chasse_au_meteore 
        #Le Pilote du Danube
        #Les Naufragés du « Jonathan

        #La Chasse au météore
        #Le Pilote du Danube#
        #Les Naufragés du « Jonathan
        #Le Secret de Wilhelm Storitz#
    #hier_et_demain
        #L'Étonnante Aventure de la mission Barsac
        #Voyage d'études
  
    #Verne_litteratureaudio_com_Nouvelles

    #  L'Île à hélice • Face au drapeau • Clovis Dardentor
    #  Le Sphinx des glaces  • Le Superbe Orénoque • Le Testament d'un excentrique • Seconde Patrie • Le Village aérien
    #  • Les Histoires de Jean-Marie Cabidoulin • Les Frères Kip • Bourses de voyage • Un drame en Livonie
    #  Maître du monde • L'Invasion de la mer • Le Phare du bout du monde • Le Volcan d'or • L'Agence Thompson and Co
    #  La Chasse au météore • Le Pilote du Danube • Les Naufragés du « Jonathan » • Le Secret de Wilhelm Storitz • Hier et demain • L'Étonnante Aventure de la mission Barsac • Voyage d'études

    closeRepertoire #chronologie
}


#################
# - les livres- #
#################

Verne_1863_Cinq_semaines_en_ballon(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                         tag_genre="Roman";
    tag_album="Cinq semaines en ballon";    tag_title="$tag_album";
    tag_year=1863;                          dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                     dl_source="www.litteratureaudio.com";
    dl_duree="9h24";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # 
        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Cinq_Semaines_en_ballon_001.png/250px-Cinq_Semaines_en_ballon_001.png"
        return

        # - chapitres - #
        initChapitre 0 44
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Cinq_semaines_en_ballon_Chap"
        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}


Verne_1863_A_propos_du_Geant(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                         tag_genre="Roman";
    tag_album="A propos du Géant";          tag_title="$tag_album";
    tag_year=1863;                          dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                     dl_source="www.litteratureaudio.com";
    dl_duree="0h08";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        #http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-a-propos-du-geant.html
        # - cover - #

        # - audios - #
        initChapitre 1 1
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_A_propos_du_geant.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent

    closeLivre
}


Verne_1864_Voyage_au_centre_de_la_Terre(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                           tag_genre="Roman";
    tag_album="Voyage_au_centre_de_la_Terre"; tag_title="$tag_album";
    tag_year=1864;                            dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Damien Genevois";                dl_source="www.litteratureaudio.com";
    dl_duree="6h44";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/jules-verne-voyage-au-centre-de-la-terre.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #
        wget "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/%27Journey_to_the_Center_of_the_Earth%27_by_%C3%89douard_Riou_38.jpg/500px-%27Journey_to_the_Center_of_the_Earth%27_by_%C3%89douard_Riou_38.jpg" "$livreTitreNormalize.jpg"
        # - audios - #
        initChapitre 1 45
        local episodeUrl="http://www.litteratureaudio.net/mp3/Jules_Verne_-_Voyage_au_centre_de_la_Terre_Chap"

        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}


Verne_1865_De_la_Terre_a_la_Lune(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="De_la_Terre_a_la_Lune";          tag_title="$tag_album";
    tag_year=1865;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="6h05";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi
    
    livreContent(){
        # - cover - #

        # - audios - #
        initChapitre -1 28
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_De_la_Terre_a_la_Lune_Chap"

        local -a chapitreTitreTbl;
        chapitreTitreTbl[0]="preliminaire"
        chapitreTitreTbl[1]="Le_gun_club"
        chapitreTitreTbl[2]="Communication_du_president_Barbican"
        chapitreTitreTbl[3]="Effet_de_la_communication_Barbican"
        chapitreTitreTbl[4]="Réponse_de_l_observatoire_de_Cambridge"
        chapitreTitreTbl[5]="Le_roman_de_la_Lune"
        chapitreTitreTbl[6]="Ce_qui_n_est_pas_possible_d_ignorer_et_ce_qui_n_est_plus_permis_de_croire_dans_les_états-unis"
        chapitreTitreTbl[7]="L_hymne_du_boulet"
        chapitreTitreTbl[8]="L_histoire_du_canon"
        chapitreTitreTbl[9]="La_question_des_poudres"

        chapitreTitreTbl[10]="Un_ennemi_sur_25_millions_d_années"
        chapitreTitreTbl[11]="Floride_et_Texas"
        chapitreTitreTbl[12]="Orbis_et_Torbis"
        chapitreTitreTbl[13]="Stones_hill"
        chapitreTitreTbl[14]="Pioche_et_truelle"
        chapitreTitreTbl[15]="La_fete_de_la_fonte"
        chapitreTitreTbl[16]="La_Colombiade"
        chapitreTitreTbl[17]="Une_depeche_telegraphique"
        chapitreTitreTbl[18]="Le_passager_de_l_Atlanta"
        chapitreTitreTbl[19]="Un_meeting"

        chapitreTitreTbl[20]="Attaque_et_Riposte"
        chapitreTitreTbl[21]="Comment_un_francais_arrange_une_affaire"
        chapitreTitreTbl[22]="Le_nouveau_citoyen_des_etats"
        chapitreTitreTbl[23]="Le_wagon_projectile"
        chapitreTitreTbl[24]="Le_telecsope_des_montagnes_rocheuses"
        chapitreTitreTbl[25]="derniers_details"
        chapitreTitreTbl[26]="Feu"
        chapitreTitreTbl[27]="Temps_couvert"
        chapitreTitreTbl[28]="Un_nouvel_astre"

        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done

        return 

    }
    livreContent
    closeLivre
}


Verne_1867_Les_Enfants_du_Capitaine_Grant(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Les enfants du Capitaine Grant"; tag_title="$tag_album";
    tag_year=1867;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="23h27";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #

        # - audios - #
        partieNb=2;

        # - partie 1- #
        partieNo=1;
        initChapitre 0 26
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_enfants_du_Capitaine_Grant_Partie${partieNo}_Chap"
    
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Baland_s_Fish"
        chapitreTitreTbl[2]="Les_3_documents"
        chapitreTitreTbl[3]="Malcom_Kaseul"
        chapitreTitreTbl[4]="Une_proposotion_de_Lady_Glenarval"
        chapitreTitreTbl[5]="Le_depart_du_Duncun"
        chapitreTitreTbl[6]="Le_passager_de_la_cabine_numéro_6"
        chapitreTitreTbl[7]="D_ou_va_et_d_ou_viens_Jacques_Paganel"
        chapitreTitreTbl[8]="Un_brave_homme_de_plus_a_bord_du_Duncan"
        chapitreTitreTbl[9]="Le_detroit_de_Magelan"

        chapitreTitreTbl[10]="Le_37eme_parrallele"
        chapitreTitreTbl[11]="Traversé_du_Chili"
        chapitreTitreTbl[12]="A_12000_pieds_dans_les_airs"
        chapitreTitreTbl[13]="Descente_de_la_cordilliere"
        chapitreTitreTbl[14]="Le_coup_de_fusil_de_la_providence"
        chapitreTitreTbl[15]="L_espagnol_de_Jacques_Paganel"
        chapitreTitreTbl[16]="Le_Rio_Colorado"
        chapitreTitreTbl[17]="Les_Pompas"
        chapitreTitreTbl[18]="A_la_recherche_du_Negale"
        chapitreTitreTbl[19]="Les_Loups_Rouges"

        chapitreTitreTbl[20]="Les_plaines_argentines"
        chapitreTitreTbl[21]="Le_fort_independance"
        chapitreTitreTbl[22]="La_crue"
        chapitreTitreTbl[23]="Ou_l_on_mene_la_vie_des_oiseaux"
        chapitreTitreTbl[24]="Ou_l_on_continue_de_mener_la_vie_des_oiseaux"
        chapitreTitreTbl[25]="Entre_le_feu_et_l_eau"
        chapitreTitreTbl[26]="L_Atlantique"

        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

        # - partie 2 - #
        partieNo=2;
        initChapitre 0 22
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_enfants_du_Capitaine_Grant_Partie${partieNo}_Chap"

        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="A_bord"
        chapitreTitreTbl[2]="Tristan_Dacouma"
        chapitreTitreTbl[3]="L_ile_Amsterdam"
        chapitreTitreTbl[4]="Les_paris_de_Jacques_Paganel_et_du_major_McNap"
        chapitreTitreTbl[5]="Les_coleres_de_l_ocean_indien"
        chapitreTitreTbl[6]="Le_cap_Pernaut"
        chapitreTitreTbl[7]="Hertonne"
        chapitreTitreTbl[8]="Le_depart"
        chapitreTitreTbl[9]="La_province_de_Victoria"

        chapitreTitreTbl[10]=""
        chapitreTitreTbl[11]=""
        chapitreTitreTbl[12]=""
        chapitreTitreTbl[13]=""
        chapitreTitreTbl[14]=""
        chapitreTitreTbl[15]=""
        chapitreTitreTbl[16]=""
        chapitreTitreTbl[17]=""
        chapitreTitreTbl[18]=""
        chapitreTitreTbl[19]=""

        chapitreTitreTbl[20]=""
        chapitreTitreTbl[21]=""
        chapitreTitreTbl[22]=""


        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

        # - partie 3 - #
        partieNo=3;
        initChapitre 0 22
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_enfants_du_Capitaine_Grant_Partie${partieNo}_Chap"

        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]=""
        chapitreTitreTbl[2]=""
        chapitreTitreTbl[3]=""
        chapitreTitreTbl[4]=""
        chapitreTitreTbl[5]=""
        chapitreTitreTbl[6]=""
        chapitreTitreTbl[7]=""
        chapitreTitreTbl[8]=""
        chapitreTitreTbl[9]=""

        chapitreTitreTbl[10]=""
        chapitreTitreTbl[11]=""
        chapitreTitreTbl[12]=""
        chapitreTitreTbl[13]=""
        chapitreTitreTbl[14]=""
        chapitreTitreTbl[15]=""
        chapitreTitreTbl[16]=""
        chapitreTitreTbl[17]=""
        chapitreTitreTbl[18]=""
        chapitreTitreTbl[19]=""

        chapitreTitreTbl[20]=""
        chapitreTitreTbl[21]=""
        chapitreTitreTbl[22]="La derniere distraction de Jacques Paganel"


        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}


Verne_1869_Vingt_mille_lieues_sous_les_mers(){
	showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                 tag_genre="Roman";
    tag_album="Vingt_mille_lieues_sous_les_mers";   tag_title="$tag_album";
    tag_year=1869;                                  dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Damien Genevois";                      dl_source="www.litteratureaudio.com";
    dl_duree="15h02";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-vingt-mille-lieues-sous-les-mers.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #
        dlwget "http://www.litteratureaudio.com/img/170x220xJules_Verne_-_20000_lieues_sous_les_mers.jpg.pagespeed.ic.FmbAhzQhrV.jpg" "$livreTitreNormalize.jpg"

        # - audios - #
        partieNb=2

        # - Partie 1 - #
        partieNo=1;
        initChapitre 0 24
        local episodeUrl="http://www.litteratureaudio.net/mp3/Jules_Verne_-_20000_lieues_sous_les_mers_L${partieNo}_Chap"
                         #http://www.litteratureaudio.net/mp3/Jules_Verne_-_20000_lieues_sous_les_mers_L1_Chap01.mp3 

        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Un_écueil_fuyant"
        chapitreTitreTbl[2]="Le_pour_et_le_contre"
        chapitreTitreTbl[3]="Comme_il_plaira_à_monsieur"
        chapitreTitreTbl[4]="Ned_Land"
        chapitreTitreTbl[5]="À_l_aventure"
        chapitreTitreTbl[6]="À_toute_vapeur"
        chapitreTitreTbl[7]="Une_baleine_d_espèce_inconnue"
        chapitreTitreTbl[8]="Mobilis_in_mobile"
        chapitreTitreTbl[9]="Les_colères_de_Ned_Land"

        chapitreTitreTbl[10]="L_homme_des_eaux"
        chapitreTitreTbl[11]="Le_Nautilus"
        chapitreTitreTbl[12]="Tout_par_l_électricité"
        chapitreTitreTbl[13]="Quelques_chiffres"
        chapitreTitreTbl[14]="Le_Fleuve Noir"
        chapitreTitreTbl[15]="Une_invitation_par_lettre"
        chapitreTitreTbl[16]="Promenade_en_plaine"
        chapitreTitreTbl[17]="Une_forêt_sous-marine"
        chapitreTitreTbl[18]="Quatre_milles_lieues_sous_le_Pacifique"
        chapitreTitreTbl[19]="Vanikoro"

        chapitreTitreTbl[20]="Le_détroit_de_Torrès"
        chapitreTitreTbl[21]="Quelques_jours_à_terre"
        chapitreTitreTbl[22]="La_foudre_du_capitaine_Nemo"
        chapitreTitreTbl[23]="agri_somnia"
        chapitreTitreTbl[24]="Le_royaume_du_corail"

        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done


        # - Partie 2 - #
        partieNo=2;
        initChapitre 0 23

        local episodeUrl="http://www.litteratureaudio.net/mp3/Jules_Verne_-_20000_lieues_sous_les_mers_L${partieNo}_Chap"
                          #http://www.litteratureaudio.net/mp3/Jules_Verne_-_20000_lieues_sous_les_mers_L1_Chap01.mp3

        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="L_océan_Indien"
        chapitreTitreTbl[2]="Une_nouvelle_proposition_du_capitaine_Nemo"
        chapitreTitreTbl[3]="Une_perle_de_dix_millions"
        chapitreTitreTbl[4]="La_mer_Rouge"
        chapitreTitreTbl[5]="Arabian-Tunnel"
        chapitreTitreTbl[6]="L_Archipel_grec"
        chapitreTitreTbl[7]="La_Méditerranée_en_quarante-huit_heures"
        chapitreTitreTbl[8]="La_baie_de_Vigo"
        chapitreTitreTbl[9]="Un_continent_disparu"

        chapitreTitreTbl[10]="Les_houillères_sous-marines"
        chapitreTitreTbl[11]="La_mer_de_Sargasses"
        chapitreTitreTbl[12]="Cachalots_et_baleines"
        chapitreTitreTbl[13]="La_banquise"
        chapitreTitreTbl[14]="Le_pôle_Sud"
        chapitreTitreTbl[15]="Accident_ou_incident"
        chapitreTitreTbl[16]="Faute_d_air"
        chapitreTitreTbl[17]="Du_cap_Horn_à_l_Amazone"
        chapitreTitreTbl[18]="Les_poulpes"
        chapitreTitreTbl[19]="Le_Gulf-Stream"

        chapitreTitreTbl[20]="Par_47°24′_de_latitude_et_de_17°28′_de_longitude"
        chapitreTitreTbl[21]="Une_hécatombe"
        chapitreTitreTbl[22]="Les_dernières_paroles_du_capitaine_Nemo"
        chapitreTitreTbl[23]="Conclusion"

        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

        }
    livreContent
    closeLivre
}


Verne_1870_Autour_de_la_Lune(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Autour_de_la_Lune";              tag_title="$tag_album";
    tag_year=1870;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="23h27";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #

        # - audios - #
        initChapitre -1 23
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Autour_de_la_Lune_Chap"

        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            displayVar "chapitreNu" "$chapitreNu"
            addChapitre "";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}


Verne_1871_Une_ville_flottante(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Une ville flottante";            tag_title="$tag_album";
    tag_year=1871;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="4h48";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #

        # - audios - #
        initChapitre 0 39
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Une_ville_flottante_Chap"
    
        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            displayVar "chapitreNu" "$chapitreNu"
            addChapitre "";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}


Verne_1873_Le_Tour_du_monde_en_80_jours(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Le Tour du monde en 80 jours";           tag_title="$tag_album";
    tag_year=1872;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Damien Genevois";                          dl_source="www.litteratureaudio.com";
    dl_duree="6h43";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/jules-verne-le-tour-du-monde-en-80-jours.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Le_Tour_du_monde_en_quatre-vingts_jours

        # - cover - #
        dlwget "https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Verne_Tour_du_Monde.jpg/250px-Verne_Tour_du_Monde.jpg" "$livreTitreNormalize.jpg"
        
        # - audios - #
        initChapitre 0 37
        local episodeUrl="http://www.litteratureaudio.net/mp3/Jules_Verne_-_Le_Tour_du_monde_en_80_jours_Chap"

        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            displayVar "chapitreNu" "$chapitreNu"
            addChapitre "";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}


Verne_1873_Les_Meridiens_et_le_calendrier(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Les Meridiens et le calendrier"; tag_title="$tag_album";
    tag_year=1873;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="0h12";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #

        # - audios - #
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_les_meridiens_et_le_calendrier.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}


Une_fantaisie_du_Docteur_Ox(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Une fantaisie du Docteur Ox";    tag_title="$tag_album";
    tag_year=1874;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="ThéoTeX";                          dl_source="www.litteratureaudio.com";
    dl_duree="2h26";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-une-fantaisie-du-docteur-ox.html
        # - cover - #

        # - audios - #
        dlwget "" "$livreTitreNormalize.jpg"
    }
    livreContent
    closeLivre
}


Verne_1874_Un_drame_dans_les_airs(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Un drame dans les airs";         tag_title="$tag_album";
    tag_year=1874;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Cécile_S";                         dl_source="www.litteratureaudio.com";
    dl_duree="0h48";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #
        dlwget http://www.litteratureaudio.com/img/xN0069016_JPEG_163_163DM.jpeg.jpg.pagespeed.ic._TzybSbQIe.jpg "$livreTitreNormalize.jpg"

        # - audios - #
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_Un_Drame_dans_les_airs.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

Verne_1875_Une_ville_ideale(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Une ville ideale";               tag_title="$tag_album";
    tag_year=1875;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="0h45";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #

        # - audios - #
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_Une_ville_ideale.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}


Verne_1875_L_Ile_mysterieuse(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="L'île mysterieuse";              tag_title="$tag_album";
    tag_year=1875;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="23h30";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #

        # - audios - #
        partieNb=3

        # - partie 1- #
        partieNo=1
        initChapitre 0 22
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_L_ile_mysterieuse_Partie${partieNo}_Chap"

        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            displayVar "chapitreNu" "$chapitreNu"
            addChapitre "";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

        # - partie 2 - #
        partieNo=2
        initChapitre 0 20
        touch "$livreTitreNormalize-P$partieNo-0-L_Abandonne.hr"
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_L_ile_mysterieuse_Partie${partieNo}_Chap"

        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            displayVar "chapitreNu" "$chapitreNu"
            addChapitre "";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

        # - partie 3 - #
        partieNo=3
        initChapitre 0 20
        touch "$livreTitreNormalize-P$partieNo-0-Le_secret_de_l_Ile.hr"
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_L_ile_mysterieuse_Partie${partieNo}_Chap"

        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            displayVar "chapitreNu" "$chapitreNu"
            addChapitre "";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}


Michel_Strogoff(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Michel Strogoff";                tag_title="$tag_album";
    tag_year=1876;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Damien Genevois";                  dl_source="www.litteratureaudio.com";
    dl_duree="11h47";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #
        dlwget "http://www.litteratureaudio.com/img/xJules_Verne_-_Michel_Strogoff_illustration.jpg.pagespeed.ic.bN2RoknMiw.jpg" "$livreTitreNormalize.jpg"

        # - audios - #
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Michel_Strogoff_P1_Chap"
        partieNb=2
        # - partie1 - #
        #touch "$livreTitreNormalize-partie$partieNo.hr"
        dlwget "${episodeUrl}01.mp3" "$livreTitreNormalize-partie1-chapitre-01-17-une_fete_au_palai_neuf.mp3"
        dlwget "${episodeUrl}02.mp3" "$livreTitreNormalize-partie1-chapitre-02-17-Russe_et_Tartare.mp3"
        dlwget "${episodeUrl}03.mp3" "$livreTitreNormalize-partie1-chapitre-03-17-Michel_Strogoff.mp3"
        dlwget "${episodeUrl}04.mp3" "$livreTitreNormalize-partie1-chapitre-04-17-De_Moscou_a_Nivogorod.mp3"
        dlwget "${episodeUrl}05.mp3" "$livreTitreNormalize-partie1-chapitre-05-17-Un_arrété_en_deux_articles.mp3"
        dlwget "${episodeUrl}06.mp3" "$livreTitreNormalize-partie1-chapitre-06-17-Frère_et_soeur.mp3"
        dlwget "${episodeUrl}07.mp3" "$livreTitreNormalize-partie1-chapitre-07-17-En_descendant_le_Volga.mp3"
        dlwget "${episodeUrl}08.mp3" "$livreTitreNormalize-partie1-chapitre-08-17-En_remontant_la_Kama.mp3"
        dlwget "${episodeUrl}09.mp3" "$livreTitreNormalize-partie1-chapitre-09-17-En_tarentas_nuit_et_jours.mp3"
        dlwget "${episodeUrl}10.mp3" "$livreTitreNormalize-partie1-chapitre-10-17-Un_orage_dans_les_monts_Ourals.mp3"
        dlwget "${episodeUrl}11.mp3" "$livreTitreNormalize-partie1-chapitre-11-17-Voyageurs_en_détresse.mp3"
        dlwget "${episodeUrl}12.mp3" "$livreTitreNormalize-partie1-chapitre-12-17-Une_provocation.mp3"
        dlwget "${episodeUrl}13.mp3" "$livreTitreNormalize-partie1-chapitre-13-17-Au_dessus_de_tout,le_devoir.mp3"
        dlwget "${episodeUrl}14.mp3" "$livreTitreNormalize-partie1-chapitre-14-17-Mère_et_Fils.mp3"
        dlwget "${episodeUrl}15.mp3" "$livreTitreNormalize-partie1-chapitre-15-17-Les_marais_de_la_Baraba.mp3"
        dlwget "${episodeUrl}16.mp3" "$livreTitreNormalize-partie1-chapitre-16-17-Un_dernier_effort-1-2.mp3"
        dlwget "${episodeUrl}17.mp3" "$livreTitreNormalize-partie1-chapitre-17-17-Un_dernier_effort-2-2.mp3"

        # - partie2 - #
        #touch "$livreTitreNormalize-partie$partieNo.hr"
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Michel_Strogoff_P2_Chap"
        dlwget "${episodeUrl}01.mp3 $livreTitreNormalize-partie2-chapitre-01-15-Un_camp_Tatare.mp3"
        dlwget "${episodeUrl}02.mp3 $livreTitreNormalize-partie2-chapitre-02-15-Une_attitude_d_Alcide_Jolivet.mp3"
        dlwget "${episodeUrl}03.mp3 $livreTitreNormalize-partie2-chapitre-03-15-Coups_pour_coups.mp3"
        dlwget "${episodeUrl}04.mp3 $livreTitreNormalize-partie2-chapitre-04-15-L_entree_triomphale.mp3"
        dlwget "${episodeUrl}05.mp3 $livreTitreNormalize-partie2-chapitre-05-15-Regarde_de_tout_tes_yeux.mp3"
        dlwget "${episodeUrl}06.mp3 $livreTitreNormalize-partie2-chapitre-06-15-Un_ami_de_grande_route.mp3"
        dlwget "${episodeUrl}07.mp3 $livreTitreNormalize-partie2-chapitre-07-15-Le_passage_de_.mp3"
        dlwget "${episodeUrl}08.mp3 $livreTitreNormalize-partie2-chapitre-08-15-Un_lievre_qui_traverse_la_route.mp3"
        dlwget "${episodeUrl}09.mp3 $livreTitreNormalize-partie2-chapitre-09-15-Dans_la_steppe.mp3"
        dlwget "${episodeUrl}10.mp3 $livreTitreNormalize-partie2-chapitre-10-15-Baikal_et_Angara.mp3"
        dlwget "${episodeUrl}11.mp3 $livreTitreNormalize-partie2-chapitre-11-15-Entre_deux_rives.mp3"
        dlwget "${episodeUrl}12.mp3 $livreTitreNormalize-partie2-chapitre-12-15-Irkousk.mp3"
        dlwget "${episodeUrl}13.mp3 $livreTitreNormalize-partie2-chapitre-13-15-Un_courrier_du_Tsar.mp3"
        dlwget "${episodeUrl}14.mp3 $livreTitreNormalize-partie2-chapitre-14-15-La_nuit_du_5_au_6_octobre.mp3"
        dlwget "${episodeUrl}15.mp3 $livreTitreNormalize-partie2-chapitre-15-15-Conclusion.mp3"
    }
    livreContent
    closeLivre
}


Verne_1877_Hector_Servadac(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Hector Servadac";                tag_title="$tag_album";
    tag_year=1877;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Vincent de l'Épine";               dl_source="www.litteratureaudio.com";
    dl_duree="14h06";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-hector-servadac.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #
        dlwget "http://www.litteratureaudio.com/img/xHector_Servadac.jpg.pagespeed.ic.nTIeYN15qT.jpg" "$livreTitreNormalize.jpg"

        # - audios - #
        partieNb=2

        # partie 1 #
        partieNo=1
        initChapitre 0 24
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_Hector_Servadac_part0${partieNo}_chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Le_compte,voici_ma_carte.Le_capitaine,voici_la_mienne"
        chapitreTitreTbl[2]="Dans_lequel_on_photographie_physiquement_et_moralement_le_capitaine_Servadac_et_son_ordonnance_Bensoufe"
        chapitreTitreTbl[3]="Ou_l_on_verra_que_l_elan_poetique_du_capitaine_Servadac_et_interompu_par_un_choc_malencontreux"
        chapitreTitreTbl[4]="Qui_permet_au_lecteur_de_multiplier_par_l_infini_les_points_d_exclamations_et_d_interogations"
        chapitreTitreTbl[5]="Dans_lequel_il_est_parlé_de_quelques_modifications_apportés_à_l_ordre_physique_sans_qu_on_puisse_y_indiquer_la_cause"
        chapitreTitreTbl[6]=""
        chapitreTitreTbl[7]=""
        chapitreTitreTbl[8]=""
        chapitreTitreTbl[9]=""

        chapitreTitreTbl[10]=""
        chapitreTitreTbl[11]=""
        chapitreTitreTbl[12]=""
        chapitreTitreTbl[13]=""
        chapitreTitreTbl[14]=""
        chapitreTitreTbl[15]=""
        chapitreTitreTbl[16]=""
        chapitreTitreTbl[17]=""
        chapitreTitreTbl[18]=""
        chapitreTitreTbl[19]=""

        chapitreTitreTbl[20]=""
        chapitreTitreTbl[21]=""
        chapitreTitreTbl[22]=""
        chapitreTitreTbl[23]=""
        chapitreTitreTbl[24]=""

        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

       
        #  - partie 2 - #
        partieNo=2
        initChapitre 0 20
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_Hector_Servadac_part0${partieNo}_chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]=""
        chapitreTitreTbl[2]=""
        chapitreTitreTbl[3]=""
        chapitreTitreTbl[4]=""
        chapitreTitreTbl[5]=""
        chapitreTitreTbl[6]=""
        chapitreTitreTbl[7]=""
        chapitreTitreTbl[8]=""
        chapitreTitreTbl[9]=""

        chapitreTitreTbl[10]=""
        chapitreTitreTbl[11]=""
        chapitreTitreTbl[12]=""
        chapitreTitreTbl[13]=""
        chapitreTitreTbl[14]=""
        chapitreTitreTbl[15]=""
        chapitreTitreTbl[16]=""
        chapitreTitreTbl[17]=""
        chapitreTitreTbl[18]=""
        chapitreTitreTbl[19]=""

        chapitreTitreTbl[20]=""

        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_Hector_Servadac_part${partieNo}_chap"

        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

    }
    livreContent
    closeLivre
}


Verne_1877_Les_Indes_noires(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Les Indes noires";               tag_title="$tag_album";
    tag_year=1877;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                         dl_source="www.litteratureaudio.com";
    dl_duree="6h28";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-les-indes-noires.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #

        # - audios - #
        initChapitre 0 22
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_Indes_noires_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Deux_lettres_contradictoires"
        chapitreTitreTbl[2]="Chemin_faisant"
        chapitreTitreTbl[3]="Le sous sol du royaume unis"
        chapitreTitreTbl[4]="La fosse d'Ochar"
        chapitreTitreTbl[5]="La famille Forf"
        chapitreTitreTbl[6]="Quelques phénomènes inexplicables"
        chapitreTitreTbl[7]="Une experience de Simon Ford"
        chapitreTitreTbl[8]="Un coup de dynamite"
        chapitreTitreTbl[9]="La nouvelle Aberfoil"

        chapitreTitreTbl[10]="Aller et retour"
        chapitreTitreTbl[11]="Les dames de feu"
        chapitreTitreTbl[12]="Les explois de Jack Rayan"
        chapitreTitreTbl[13]="Call City"
        chapitreTitreTbl[14]="Suspendu à un fil"
        chapitreTitreTbl[15]="Nel au cotage"
        chapitreTitreTbl[16]="Sur l'échelle oscillante"
        chapitreTitreTbl[17]="Un lévée de soleil"
        chapitreTitreTbl[18]="Du lac Lomon au lac Catherine"
        chapitreTitreTbl[19]="Une dernière menace"

        chapitreTitreTbl[20]="Le pénitent"
        chapitreTitreTbl[21]="Le mariage de Nel"
        chapitreTitreTbl[22]="La légende du vieux Silfax"


        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done

    }
    livreContent
    closeLivre
}


Verne_1879_Les_Revoltes_de_la_Bounty(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                             tag_genre="Roman";
    tag_album="Les Revoltes de la Bounty";      tag_title="$tag_album";
    tag_year=1879;                              dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="René Depasse";                     dl_source="www.litteratureaudio.com";
    dl_duree="0h55";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #
        dlwget "http://www.litteratureaudio.com/img/xdescendants-bounty.jpg.pagespeed.ic.hI1EV2utH-.jpg" "$livreTitreNormalize.jpg"

        # - audios - #
        dlwget "http://www.litteratureaudio.org/mp3/Verne_-_Les_revoltes_de_la_Bounty.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}


Verne_1879_Les_Cinq_Cents_Millions_de_la_Begum(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Les Cinq Cents Millions de la Begum";    tag_title="$tag_album";
    tag_year=1879;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="4h59";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #

        # - audios - #
        initChapitre 0 20
        local episodeUrl="http://www.litteratureaudio.net/mp3/Jules_Verne_-_Les_Cinq_Cents_Millions_de_la_Begum_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Où_Mr.Sharp_fait_son_entrée"
        chapitreTitreTbl[2]="Deux_copains"
        chapitreTitreTbl[3]="Un_fait_divers"
        chapitreTitreTbl[4]="Part_à_deux"
        chapitreTitreTbl[5]="La_Cité_de_l_acier"
        chapitreTitreTbl[6]="La_Cité_de_l_acier"
        chapitreTitreTbl[7]="Le_Puits_Albrecht"
        chapitreTitreTbl[8]="Le_Bloc_central"
        chapitreTitreTbl[9]="PPC"

        chapitreTitreTbl[10]="Un_article_de_l_Unsere_Centurie,revue_allemande"
        chapitreTitreTbl[11]="Un_dîner_chez_le_docteur_Sarrasin"
        chapitreTitreTbl[12]="Le_conseil"
        chapitreTitreTbl[13]="Marcel_Bruckmann_au_professeur_Schultze"
        chapitreTitreTbl[14]="Branle-bas_de_combat"
        chapitreTitreTbl[15]="La_Bourse_de_San_Francisco"
        chapitreTitreTbl[16]="Deux_français_contre_une_ville"
        chapitreTitreTbl[17]="Explications_à_coups_de_fusil"
        chapitreTitreTbl[18]="L_Amande_du_noyau"
        chapitreTitreTbl[19]="Une_affaire_de_famille"

        chapitreTitreTbl[20]="Conclusion"


        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
	closeLivre
}


Verne_1879_Les_Tribulations_d_un_Chinois_en_Chine(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Les Tribulations d un Chinois en Chine"; tag_title="$tag_album";
    tag_year=1879;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Carole Bassani Adibzadeh";                 dl_source="www.litteratureaudio.com";
    dl_duree="6h42";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"

    livreContent(){
        # - cover - #
        dlwget http://www.litteratureaudio.com/img/xVerne_-_Les_Tribulations_d-un_Chinois_en_Chine.jpg.pagespeed.ic.Q4neCche8s.jpg "$livreTitreNormalize.jpg"

        # - audios - #
        initChapitre 0 22
        
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_Tribulations_d_un_Chinois_en_Chine_Chap"

        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1880_La_maison_a_vapeur(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="La maison à vapeur";                     tag_title="$tag_album";
    tag_year=1880;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="14h00";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-la-maison-a-vapeur.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/La_Maison_%C3%A0_vapeur

        # - cover - #
        wget "https://upload.wikimedia.org/wikipedia/commons/a/a2/%27The_Steam_House%27_by_L%C3%A9on_Benett_018.jpg"  "$livreTitreNormalize.jpg"
        # - audios - #
        # les cahpitres sont titrés
        partieNb=2

        # partie 1 #
        partieNo=1
        initChapitre 0 16
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_La_maison_a_vapeur_Partie${partieNo}_Chap"
        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done

        # partie 2 #
        partieNo=2
        initChapitre 0 14
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_La_maison_a_vapeur_Partie${partieNo}_Chap"
        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1882_Le_Rayon_vert(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Le Rayon vert";                          tag_title="$tag_album";
    tag_year=1882;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Christine Setrin";                         dl_source="www.litteratureaudio.com";
    dl_duree="6h59";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-le-rayon-vert.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #
        dlwget "http://www.litteratureaudio.com/img/xJules_Verne_-_Le_Rayon_vert.jpg.pagespeed.ic.jh_v1GTup3.jpg" "$livreTitreNormalize.jpg"

        # - audios - #
        initChapitre 0 23
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Le_Rayon_vert_Chap"

        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Le_frere_Sam_et_le_frere_Sib"
        chapitreTitreTbl[2]="Helena_Campbell"
        chapitreTitreTbl[3]="L_article_du_Morning_Post"
        chapitreTitreTbl[4]="En_descendant_la_Clyde"
        chapitreTitreTbl[5]="D_un_bateau_a_l_autre"
        chapitreTitreTbl[6]="Le_gouffre_du_Corryvrekan"
        chapitreTitreTbl[7]="Aristobulus_Ursiclos"
        chapitreTitreTbl[8]="Un_nuage_a_l_horizon"
        chapitreTitreTbl[9]="Propo_de_dame_Bess"

        chapitreTitreTbl[10]="Une_partie_de_crockett"
        chapitreTitreTbl[11]="Olivier_Sinclair"
        chapitreTitreTbl[12]="Nouveaux_projets"
        chapitreTitreTbl[13]="Les_magnificences_de_la_mer"
        chapitreTitreTbl[14]="La_vie_a_Iona"
        chapitreTitreTbl[15]="Les_ruines_d_Iona"
        chapitreTitreTbl[16]="Deux_coups_de_fusil"
        chapitreTitreTbl[17]="À_bord_de_la_Clorinda"
        chapitreTitreTbl[18]="Staffa"
        chapitreTitreTbl[19]="La_grotte_de_Fingal"

        chapitreTitreTbl[20]="Pour_miss_Campbell"
        chapitreTitreTbl[21]="Toute_une_tempete_dans_une_grotte"
        chapitreTitreTbl[22]="Le_Rayon-Vert"
        chapitreTitreTbl[23]="Conclusion"

        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1884_L_Etoile_du_sud(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="L'étoile du sud";                        tag_title="$tag_album";
    tag_year=1884;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Bernard";                                  dl_source="www.litteratureaudio.com";
    dl_duree="8h07";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        #https://fr.wikisource.org/wiki/L'Etoile_du_sud

        # - cover - #
        dlwget "http://www.litteratureaudio.com/img/xJules_Verne_-_L_Etoile_du_sud.jpg.pagespeed.ic.u3_0u77uc8.jpg" "$livreTitreNormalize.jpg"

        # - audios - #
        initChapitre 0 24
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_L_Etoile_du_sud_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Renversants_ces_francais"
        chapitreTitreTbl[2]="Au_champs_des_diamants"
        chapitreTitreTbl[3]="Un_peu_de_science_enseignee_de_bonne_amitie"
        chapitreTitreTbl[4]="Vandergaart_Kopje"
        chapitreTitreTbl[5]="Premiere_exploitation"
        chapitreTitreTbl[6]="Moeurs_du_camp"
        chapitreTitreTbl[7]="L_eboulement"
        chapitreTitreTbl[8]="La_grande_experience"
        chapitreTitreTbl[9]="Une_surprise"

        chapitreTitreTbl[10]="Ou_John_Watktins_reflechit"
        chapitreTitreTbl[11]="L_Etoile_du_Sud"
        chapitreTitreTbl[12]="Preparatifs_de_depart"
        chapitreTitreTbl[13]="A_travers_le_Transvaal"
        chapitreTitreTbl[14]="Au_Nord_du_Limpopo"
        chapitreTitreTbl[15]="un_complot"
        chapitreTitreTbl[16]="Trahison"
        chapitreTitreTbl[17]="Un_steeple-chase_africain"
        chapitreTitreTbl[18]="L_autruche_qui_parle"
        chapitreTitreTbl[19]="La_grotte_merveilleuse"

        chapitreTitreTbl[20]="Le_retour"
        chapitreTitreTbl[21]="Justice_venitienne"
        chapitreTitreTbl[22]="une_mine_d_un_nouveau_genre"
        chapitreTitreTbl[23]="La_statue_du_commandeur"
        chapitreTitreTbl[24]="Une_etoile_qui_file"


        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1884_Frritt_Flacc(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Frritt-Flacc";                        tag_title="$tag_album";
    tag_year=1884;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="René Depasse";                             dl_source="www.litteratureaudio.com";
    dl_duree="0h20";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover - #
        dlwget "http://www.litteratureaudio.com/img/xFrritt_flacc.jpg.pagespeed.ic.dwpFWQMHfG.jpg" "$livreTitreNormalize.jpg"

        # - audios - #
        dlwget "http://www.litteratureaudio.org/mp3/Verne_-_Frritt_flacc.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

Verne_1885_lepave_du_cynthia(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="L'Épave du Cynthia";                     tag_title="$tag_album";
    tag_year=1885;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Brigitte84";                               dl_source="www.litteratureaudio.com";
    dl_duree="8h53";
    dl_postnote=" Un roman cosigné par Jules Verne et André Laurie."
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-et-laurie-andre-lepave-du-cynthia.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        #https://fr.wikipedia.org/wiki/L%27%C3%89pave_du_Cynthia

        # - cover - #
        dlwget "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/HetzelEpaveCynthia.jpg/520px-HetzelEpaveCynthia.jpg" "$livreTitreNormalize.jpg"
        dlwget "http://www.litteratureaudio.com/img/xepave_du_Cynthia_-_George_Roux.jpg.pagespeed.ic.KdGwgB83zO.webp" "$livreTitreNormalize.webp"

        # - audios - #
        initChapitre 0 22
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_et_Andre_Laurie_-_L_epave_du_Cynthia_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]=""
        chapitreTitreTbl[2]=""
        chapitreTitreTbl[3]=""
        chapitreTitreTbl[4]=""
        chapitreTitreTbl[5]=""
        chapitreTitreTbl[6]=""
        chapitreTitreTbl[7]=""
        chapitreTitreTbl[8]=""
        chapitreTitreTbl[9]=""

        chapitreTitreTbl[10]=""
        chapitreTitreTbl[11]=""
        chapitreTitreTbl[12]=""
        chapitreTitreTbl[13]=""
        chapitreTitreTbl[14]=""
        chapitreTitreTbl[15]=""
        chapitreTitreTbl[16]=""
        chapitreTitreTbl[17]=""
        chapitreTitreTbl[18]=""
        chapitreTitreTbl[19]=""

        chapitreTitreTbl[20]=""
        chapitreTitreTbl[21]=""
        chapitreTitreTbl[21]=""
        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done    
    }
    livreContent
    closeLivre
}

Verne_1886_Robur_le_Conquerant(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Robur le Conquerant";                    tag_title="$tag_album";
    tag_year=1886;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="7h00";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        #http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-robur-le-conquerant.html
        #https://fr.wikipedia.org/wiki/Robur_le_Conqu%C3%A9rant
        #http://www.jules-verne.co.uk/french-robur-le-conquerant/

        # - cover - #

        # - audios - #
        initChapitre 0 18
        local episodeUrl="www.litteratureaudio.org/mp3/Jules_Verne_-_Robur_le_Conquerant_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Ou_le_monde_savant_et_le_monde_ignorant_sont_aussi_bien_embarasses_l_un_que_l_autre"
        chapitreTitreTbl[2]="Dans_lequel_les_membres_du_Weldon"
        chapitreTitreTbl[3]="Dans_lequel_un_nouveau_personnage_n_a_pas_besoin_d_etre_presente_car_il_se_presente_lui_meme"
        chapitreTitreTbl[4]="Dans_lequel_a_propos_du_valet_Frycollin,l_auteur_essaie_de_rehabiliter_la_lune"
        chapitreTitreTbl[5]="Dans_lequel_une_suspension_d_hostilites_est_consentie_entre_le_president_et_le_secretaire_du_Weldon-institure"
        chapitreTitreTbl[6]="Les_ingenieurs,les_mecaniciens_et_autres_savants_feraient_bien_de__passer"
        chapitreTitreTbl[7]="Dans_lequel_uncle_Prudent__et_Phil_Evans_refusenet_encore__de_se_laisser_convaincre"
        chapitreTitreTbl[8]="Ou_l_on_verra_que_Robur_se_decide_a_repondre_a_l_importante_question_qui_lui_a_ete_pose"
        chapitreTitreTbl[9]="Dans_lequel_l_Albatros_a_franchit_pres_de_dix_milles_kilometres,qui_se_termine_par_un_bond_prodigieux"

        chapitreTitreTbl[10]="Dans_lequel_on_verra_comment_et_pourquoi_la_valet_Frycollin_fut_mis_a_la_remorque"
        chapitreTitreTbl[11]="Dans_lequel_la_colere_de_uncle_Prudent_croit_comme_le_carre_de_la_vitesse"
        chapitreTitreTbl[12]="Dans_lequel_l_ingenieur_Robur_agit_comme_s_il_voulait_concourir_pour_un_des_prix_Monthyon"
        chapitreTitreTbl[13]="Dans_lequel_uncle_Prudent_et_Phil_Evans_traversent_tout_un_ocean_sans_avoir_le_mal_de_mer"
        chapitreTitreTbl[14]="Dans_lequel_l_Albatros_fait_ce_qu_on_ne_pourrait_sans_doute_jamais_faire"
        chapitreTitreTbl[15]="Dans_lequel_il_se_passent_des_choses_qui_meritent_vraiment_la_peine_d_etre_racontes"
        chapitreTitreTbl[16]="Qui_laissera_le_lecteur_dans_une_indecision_peut-etre_regrettable"
        chapitreTitreTbl[17]="Dans_lequel_on_revient_deux_mois_en_arriere_et_ou_l_onsaute_neuf_mois_en_avant"
        chapitreTitreTbl[18]="Qui_termine_cette_veridique_histoire_de_l-Albatros_sans_la_terminer"


        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            displayVar "chapitreNu" "$chapitreNu"
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
            #dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1888_Deux_Ans_de_vacances(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Deux ans de vacances";                   tag_title="$tag_album";
    tag_year=1888;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Christine Setrin";                         dl_source="www.litteratureaudio.com";
    dl_duree="13h55";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-deux-ans-de-vacances.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        #http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-deux-ans-de-vacances.html
        #https://fr.wikipedia.org/wiki/Deux_ans_de_vacances
        #https://fr.wikisource.org/wiki/Deux_Ans_de_vacances

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Dwalat_01.jpg/250px-Dwalat_01.jpg"

        # - audios - #

        # - Preface - #
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Deux_Ans_de_vacances_Chapitre_00_Preface.mp3"
        dlwget "$episodeUrl" "$livreTitreNormalize--chapitre-00-$chapitreNb-Preface.mp3"

        initChapitre 0 30
        #local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Deux_Ans_de_vacances_Chapitre_0"
         local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Deux_Ans_de_vacances_Chapitre_"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="La_tempete"
        chapitreTitreTbl[2]="Au_millieu_du_ressac"
        chapitreTitreTbl[3]="La_pension_Chairman_à_Auckland"
        chapitreTitreTbl[4]="Première_exploration_du_littoral"
        chapitreTitreTbl[5]="Ile_ou_continent"
        chapitreTitreTbl[6]="Discussion"
        chapitreTitreTbl[7]="Le_bois_de_bouleaux"
        chapitreTitreTbl[8]="Reconnaissance_dans_l_ouest_du_lac"
        chapitreTitreTbl[9]="Visite_à_la_caverne"

        chapitreTitreTbl[10]="Récit_de_l_exploration"
        chapitreTitreTbl[11]="Premières_dispositions_à_l_intérieur_de_French-den"
        chapitreTitreTbl[12]="Agrandissement_de_French-den"
        chapitreTitreTbl[13]="Le_programme_d_études"
        chapitreTitreTbl[14]="Derniers_coups_de_l_hiver"
        chapitreTitreTbl[15]="Route_a_suivre_pour_le_retour"
        chapitreTitreTbl[16]="Briant_inquiet_de_Jacques"
        chapitreTitreTbl[17]="Preparatifs_en_vue_du_prochain_hiver"
        chapitreTitreTbl[18]="Le_marais_salant"
        chapitreTitreTbl[19]="Le_mât_de_signaux"

        chapitreTitreTbl[20]="Une_halte_à_la_pointe_sud_du_lac"
        chapitreTitreTbl[21]="Exploration_de_Deception-bay"
        chapitreTitreTbl[22]="Une_idée_de_Briant"
        chapitreTitreTbl[23]="La_situation_telle_qu_elle_est"
        chapitreTitreTbl[24]="Premier_essai"
        chapitreTitreTbl[25]="La_chaloupe_du_Severn"
        chapitreTitreTbl[26]="Kate_et_le_master"
        chapitreTitreTbl[27]="Le_détroit_de_Magellan"
        chapitreTitreTbl[28]="Interrogatoire_de_Forbes"
        chapitreTitreTbl[29]="Réaction"

        chapitreTitreTbl[30]="Conclusion"
        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1889_Sans_dessus_dessous(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Sans dessus dessous";                    tag_title="$tag_album";
    tag_year=1889;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="TheoTeX";                                  dl_source="www.litteratureaudio.com";
    dl_duree="5h09";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){

        # - cover - #
        dlwget http://www.litteratureaudio.com/img/xmaston_hook.jpg.pagespeed.ic.UiKtNnNPtw.jpg "$livreTitreNormalize.jpg"

        # - audios - #
        initChapitre 0 21
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Sans_dessus_dessous_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Ou la North Polar practical association lance un document à travers les 2 mondes"
        chapitreTitreTbl[2]=""
        chapitreTitreTbl[3]=""
        chapitreTitreTbl[4]=""
        chapitreTitreTbl[5]=""
        chapitreTitreTbl[6]=""
        chapitreTitreTbl[7]=""
        chapitreTitreTbl[8]=""
        chapitreTitreTbl[9]=""

        chapitreTitreTbl[10]=""
        chapitreTitreTbl[11]=""
        chapitreTitreTbl[12]=""
        chapitreTitreTbl[13]=""
        chapitreTitreTbl[14]=""
        chapitreTitreTbl[15]=""
        chapitreTitreTbl[16]=""
        chapitreTitreTbl[17]=""
        chapitreTitreTbl[18]=""
        chapitreTitreTbl[19]=""

        chapitreTitreTbl[20]=""
        chapitreTitreTbl[21]=""


        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done

    }
    livreContent
    closeLivre
}

Verne_1890_Cesar_Cascabel(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Cesar Cascabel";                         tag_title="$tag_album";
    tag_year=1890;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Aurelien Ridon";                           dl_source="www.litteratureaudio.com";
    dl_duree="12h12";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        #http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-cesar-cascabel.html
        #http://jv.gilead.org.il/zydorczak/cas00.html

        # - cover - #

        # - audios - #
        partieNb=2
        
        # - partie 1- #
        partieNo=1;
        initChapitre 0 16
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Cesar_Cascabel_P${partieNo}_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Fortune_faite"
        chapitreTitreTbl[2]="Famille_Cascabel"
        chapitreTitreTbl[3]="La_Sierra_Nevada"
        chapitreTitreTbl[4]="Grande_détermination"
        chapitreTitreTbl[5]="En_route"
        chapitreTitreTbl[6]="Suite_du_voyage"
        chapitreTitreTbl[7]="A_travers_le_Caribou"
        chapitreTitreTbl[8]="Au_village_des_Coquins"
        chapitreTitreTbl[9]="On_ne_passe_pas"

        chapitreTitreTbl[10]="Kayette"
        chapitreTitreTbl[11]="Sitka"
        chapitreTitreTbl[12]="De_Sitka_au_fort_Youkon"
        chapitreTitreTbl[13]="Une_idee_de_Cornélia_Cascabel"
        chapitreTitreTbl[14]="Du_fort_Youkon_a_Port-Clarence"
        chapitreTitreTbl[15]="Port-Clarence"
        chapitreTitreTbl[16]="Adieux_au_nouveau_continent"


        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done    


        # - partie 2 - #
        partieNo=2;
        initChapitre 0 15
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Cesar_Cascabel_P${partieNo}_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Le_detroit_de_Behring"
        chapitreTitreTbl[2]="Entre_deux_courants"
        chapitreTitreTbl[3]="En_derive"
        chapitreTitreTbl[4]="Du_16_novembre_au_2_decembre"
        chapitreTitreTbl[5]="Les_iles_Liakhoff"
        chapitreTitreTbl[6]="Hivernage"
        chapitreTitreTbl[7]="Un_bon_tour_de_M.Cascabel"
        chapitreTitreTbl[8]="Le_pays_des_Iakoutes"
        chapitreTitreTbl[9]="Jusqu_a_l_Obi"

        chapitreTitreTbl[10]="Du_fleuve_Obi_aux_monts_Ourals"
        chapitreTitreTbl[11]="Les_monts_Ourals"
        chapitreTitreTbl[12]="Voyage_termine_et_qui_n_est_pas_fini"
        chapitreTitreTbl[13]="Une_longue_journee"
        chapitreTitreTbl[14]="Denouement_tres_applaudi_des_spectateurs"
        chapitreTitreTbl[15]="Conclusion"
        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
        done
    }    
    livreContent
    closeLivre
}

Verne_1891_Ptit_Bonhomme (){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="P’tit Bonhomme";                         tag_title="$tag_album";
    tag_year=1891;                                      dl_date="$tag_year";
    dl_editeur="Hetzel";
	dl_voix="Pomme";                                    dl_source="www.litteratureaudio.com";
    dl_duree="13h39";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-ptit-bonhomme.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/P%27tit-Bonhomme

        # - cover - #
        dlwget "" "$livreTitreNormalize.jpg"
        dlwget "" "$livreTitreNormalize.webp"
    
        # - audios - #
        partieNb=2

        partieNo=1
        initChapitre 1 16
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_P_tit_Bonhomme_P1_Chap"

        addChapitre "" 1;
        dl_postnote="chapitres 1,2,3";  setTagNoteLivre
        dlwget "${episodeUrl}1_a_3.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_2_3.mp3"

        addChapitre "" 4;
        dl_postnote="chapitres 4,5,6";  setTagNoteLivre
        dlwget "${episodeUrl}4_a_6.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_5_6.mp3"

        addChapitre "" 7;
        dl_postnote="chapitres 7,8";  setTagNoteLivre
        dlwget "${episodeUrl}7_et_8.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_7_8.mp3"

        addChapitre "" 9;
        dl_postnote="chapitres 9,10,11";  setTagNoteLivre
        dlwget "${episodeUrl}9_a_11.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_10_11.mp3"

        addChapitre "" 12;
        dl_postnote="chapitres 12,13";  setTagNoteLivre
        dlwget "${episodeUrl}12_et_13.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_12_13.mp3"

        addChapitre "" 14;
        dl_postnote="chapitres 14,15";  setTagNoteLivre
        dlwget "${episodeUrl}14_et_15.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_14.mp3"

        addChapitre "" 16;
        dlwget "${episodeUrl}16.mp3" "$livreTitreNormalizePartieChapitre.mp3"


        partieNo=2
        initChapitre 1 15
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_P_tit_Bonhomme_P2_Chap"

        addChapitre "" 1;
        dl_postnote="chapitres 1,2,3";  setTagNoteLivre
        dlwget "${episodeUrl}1_a_3.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_2_3.mp3"

        addChapitre "" 4;
        dl_postnote="chapitres 4,5";  setTagNoteLivre
        dlwget "${episodeUrl}4_a_5.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_5.mp3"

        addChapitre "" 6;
        dl_postnote="chapitres 6,7,8";  setTagNoteLivre
        dlwget "${episodeUrl}7_a_8.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_7_8.mp3"

        addChapitre "" 9;
        dl_postnote="chapitres 9,10";  setTagNoteLivre
        dlwget "${episodeUrl}9_et10.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_10.mp3"

        addChapitre "" 11;
        dl_postnote="chapitres 11,12";  setTagNoteLivre
        dlwget "${episodeUrl}11_et12.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_12.mp3"

        addChapitre "" 13;
        dl_postnote="chapitres 13,14";  setTagNoteLivre
        dlwget "${episodeUrl}13_et14V3.mp3" "$livreTitreNormalizePartieChapitre-et_chapitre_14.mp3"

        addChapitre "" 15;
        dlwget "${episodeUrl}15.mp3" "$livreTitreNormalizePartieChapitre.mp3"

    }
    livreContent
    closeLivre
}

Verne_1892_Le_Chateau_des_Carpathes(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Le_Chateau_des_Carpathes";               tag_title="$tag_album";
    tag_year=1892;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="5h10";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        #https://fr.wikipedia.org/wiki/Le_Ch%C3%A2teau_des_Carpathes
        #https://fr.wikisource.org/wiki/Le_Ch%C3%A2teau_des_Carpathes

        # - cover - #

        # - audios - #
        initChapitre 0 18     
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Le_Chateau_des_Carpathes_Chap"
        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done

    }
    livreContent
    closeLivre
}

Verne_1895_L_Ile_a_helice(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="L'ïle à hélice";                         tag_title="$tag_album";
    tag_year=1895;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="11h50";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        #http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-lile-a-helice.html
        #http://www.gutenberg.org/ebooks/17798

        # - cover - #

        # - audios - #
        partieNb=2;

        # - partie 1- #
        partieNo=1;
        initChapitre 0 14
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_L_ile_a_helice_Partie_${partieNo}_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Le_Quatuor_Concertant"
        chapitreTitreTbl[2]="Puissance_d_une_sonate_cacophonique"
        chapitreTitreTbl[3]="Un_loquace_cicérone"
        chapitreTitreTbl[4]="Le_Quatuor_Concertant_déconcerté"
        chapitreTitreTbl[5]="Standard-Island_et_Milliard-City"
        chapitreTitreTbl[6]="Invites...inviti"
        chapitreTitreTbl[7]="Cap_a_l_ouest"
        chapitreTitreTbl[8]="Navigation"
        chapitreTitreTbl[9]="L_archipel_des_Sandwich"

        chapitreTitreTbl[10]="Passage_de_la_ligne"
        chapitreTitreTbl[11]="Iles_Marquises"
        chapitreTitreTbl[12]="Trois_semaines_aux_Pomotou"
        chapitreTitreTbl[13]="Relache_a_Tahiti"
        chapitreTitreTbl[14]="De_fetes_en_fetes"
        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            displayVar "chapitreNu" "$chapitreNu"
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
            #dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done


        # - partie 2- #
        partieNo=2;
        initChapitre 0 14
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_L_ile_a_helice_Partie_${partieNo}_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Aux_Iles_de_Cook"
        chapitreTitreTbl[2]="D_îles_en_iles"
        chapitreTitreTbl[3]="Concert_a_la_cour"
        chapitreTitreTbl[4]="Ultimatum_britannique"
        chapitreTitreTbl[5]="Le_Tabou_a_Tonga-Tabou"
        chapitreTitreTbl[6]="Une_collection_de_fauves"
        chapitreTitreTbl[7]="Battues"
        chapitreTitreTbl[8]="Fidji_et_Fidjiens"
        chapitreTitreTbl[9]="Un_casus_belli"

        chapitreTitreTbl[10]="Changement_de_proprietaires"
        chapitreTitreTbl[11]="Attaque_et_defense"
        chapitreTitreTbl[12]="Tribord_et_Babord_la_barre"
        chapitreTitreTbl[13]="Le_mot_de_la_situation_dit_par_Pinchinat"
        chapitreTitreTbl[14]="Denouement"
        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            displayVar "chapitreNu" "$chapitreNu"
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
            #dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

Verne_1897_Le_Sphinx_des_Glaces(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Le Sphinx des Glaces";                   tag_title="$tag_album";
    tag_year=1897;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="13h40";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-le-sphinx-des-glaces.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        #http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-le-sphinx-des-glaces.html
        #https://fr.wikipedia.org/wiki/Le_Sphinx_des_glaces
        #https://fr.wikisource.org/wiki/Le_Sphinx_des_glaces

        # - cover - #
        #dlwget "" "$livreTitreNormalize.jpg"

        # - audios - #
        partieNo=2
    
        # - partie 1- #
        partieNo=1;
        initChapitre 0 16
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Le_sphinx_des_glaces_Partie${partieNo}_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Les_Iles_Kerguelen"
        chapitreTitreTbl[2]="La_goelette_Halbrane"
        chapitreTitreTbl[3]="Le_capitaine_Len_Guy"
        chapitreTitreTbl[4]="Des_iles_Kerguelen_a_l_ile_du_Prince-Edouard"
        chapitreTitreTbl[5]="Le_roman_d_Edgard_Poe"
        chapitreTitreTbl[6]="Comme_un_linceul_qui_s_entr_ouvre"
        chapitreTitreTbl[7]="Tristan_d_Acunha"
        chapitreTitreTbl[8]="En_direction_vers_les_Falklands"
        chapitreTitreTbl[9]="Mise_en_etat_de_l_Halbrane"

        chapitreTitreTbl[10]="Au_début_de_la_campagne"
        chapitreTitreTbl[11]="Des_Sandwich_au_cercle_polaire"
        chapitreTitreTbl[12]="Entre_le_cercle_polaire_et_la_banquise"
        chapitreTitreTbl[13]="Le_long_de_la_banquise"
        chapitreTitreTbl[14]="Une_voix_dans_un_reve"
        chapitreTitreTbl[15]="L_îlot_Bennet"
        chapitreTitreTbl[16]="L_Ile_Tsalal"
        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            displayVar "chapitreNu" "$chapitreNu"
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
            #dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done    


        # - partie 2 - #
        partieNo=2;
        initChapitre 0 16
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Le_sphinx_des_glaces_Partie${partieNo}_Chap"
            local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Et Pym"
        chapitreTitreTbl[2]="Decision_prise"
        chapitreTitreTbl[3]="Le_groupe_disparu"
        chapitreTitreTbl[4]="Du_29_decembre_au_9_janvier"
        chapitreTitreTbl[5]="Une_embardee"
        chapitreTitreTbl[6]="Terre"
        chapitreTitreTbl[7]="L_ice-berg_culbute"
        chapitreTitreTbl[8]="Le_coup_de_grace"
        chapitreTitreTbl[9]="Que_faire"

        chapitreTitreTbl[10]="Hallucinations"
        chapitreTitreTbl[11]="Au_milieu_des_brumes"
        chapitreTitreTbl[12]="Campement"
        chapitreTitreTbl[13]="Dirk_Peters_a_la_mer"
        chapitreTitreTbl[14]="Onze_ans_en_quelques_pages"
        chapitreTitreTbl[15]="Le_Sphinx_des_glaces"
        chapitreTitreTbl[16]="Douze_sur_soixante-dix"
        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            displayVar "chapitreNu" "$chapitreNu"
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizePartieChapitre.mp3"
            #dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done
    }
    livreContent
    closeLivre
}

_1998_Le_Secret_de_Wilhelm_Storitz(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Le Secret de Wilhelm Storitz";           tag_title="$tag_album";
    tag_year=1898;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="";                                 dl_source="www.litteratureaudio.com";
    dl_duree="h";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){

        # - cover - #

        # - audios - #
        #http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-le-secret-de-wilhelm-storitz.html
        echo "Vide"
    }
    livreContent
    closeLivre
}


Verne_1901_Les_Histoires_de_Jean_Marie_Cabidoulin(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Les Histoires de Jean-Marie Cabidoulin ";tag_title="$tag_album";
    tag_year=1901;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Pomme";                                   dl_source="www.litteratureaudio.com";
    dl_duree="h";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-les-histoires-de-jean-marie-cabidoulin.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Les_Histoires_de_Jean-Marie_Cabidoulin

        # - cover - #

        # - audios - #
        initChapitre 0 15
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Les_Histoires_de_Jean_Marie_Cabidoulin_Chap"
        for chapitreNo in 1 3 5 7 9 11
        do
            addChapitre "" $chapitreNo;
            local incChapitreNo=$((chapitreNo + 1))
            dl_postnote="chapitres $chapitreNo et $incChapitreNo"
            setTagNoteLivre
            dlwget "${episodeUrl}${chapitreNo}_et_${incChapitreNo}.mp3" "$livreTitreNormalizeChapitre-et_chapitre_$incChapitreNo.mp3"
        done    
        # 13-14-15
        addChapitre "" 13;
        dl_postnote="épisode 13 14 15"
        setTagNoteLivre
        dlwget "${episodeUrl}13_a_15.mp3" "$livreTitreNormalizeChapitre-et_chapitre_14_15.mp3"
    }
    livreContent
    closeLivre
}
Verne_1901_La_Chasse_au_meteore(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="La chasse au météore";                   tag_title="$tag_album";
    tag_year=1901;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Arthur";                                   dl_source="www.litteratureaudio.com";
    dl_duree="7h15";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-la-chasse-au-meteore.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        #https://fr.wikipedia.org/wiki/La_Chasse_au_m%C3%A9t%C3%A9ore

        # - cover - #

        # - audios - #
        initChapitre 0 21
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_La_Chasse_au_meteore_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]="Dans lequel le juge John Cross remplit l'un des plus agréable devoir de sa charge avant de retourner à son jardin"
        chapitreTitreTbl[2]=""
        chapitreTitreTbl[3]=""
        chapitreTitreTbl[4]=""
        chapitreTitreTbl[5]=""
        chapitreTitreTbl[6]=""
        chapitreTitreTbl[7]=""
        chapitreTitreTbl[8]=""
        chapitreTitreTbl[9]=""

        chapitreTitreTbl[10]=""
        chapitreTitreTbl[11]=""
        chapitreTitreTbl[12]=""
        chapitreTitreTbl[13]=""
        chapitreTitreTbl[14]=""
        chapitreTitreTbl[15]=""
        chapitreTitreTbl[16]=""
        chapitreTitreTbl[17]=""
        chapitreTitreTbl[18]=""
        chapitreTitreTbl[19]=""

        chapitreTitreTbl[20]=""
        chapitreTitreTbl[21]=""
        for ((chapitreNu=$chapitreNo+1; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}";
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done    
    }
    livreContent
    closeLivre
}

Verne_1910_Le_Humbug_Moeurs_Americaines(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="hier_et_demain";                         tag_title="Le_Humbug Moeurs Americaines";
    tag_year=1910;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="René Depasse";                             dl_source="www.litteratureaudio.com";
    dl_duree="1h15";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
    	# - cover - #
        dlwget "" "$livreTitreNormalize.jpg"

        # - audios - #
        local episodeUrl="http://www.litteratureaudio.org/mp3/Verne_-_Humbug"
        #wget 
    }
    livreContent
    closeLivre
}

Verne_1886_Aventures_de_la_famille_Raton(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="hier_et_demain";                         tag_title="Aventures de la famille Raton";
    tag_year=1886;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Domi";                                     dl_source="www.litteratureaudio.com";
    dl_duree="1h47";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-aventures-de-la-famille-raton.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"

    livreContent(){
    	# - cover - #
        dlwget "http://www.litteratureaudio.com/img/xJules_Verne_-_La_Famille_Raton.jpg.pagespeed.ic.O3-f1eCMbS.jpg" "$livreTitreNormalize.jpg"

        # - audios - #
        addChapitre "" 1;dlwget "http://www.litteratureaudio.org/mp3/VERNE_Jules_Aventures_de_la_famille_Raton_fich01.mp3" "$livreTitreNormalize.mp3"
        addChapitre "" 2;dlwget "http://www.litteratureaudio.org/mp3/VERNE_Jules_Aventures_de_la_famille_Raton_fich02.mp3" "$livreTitreNormalize.mp3"
        addChapitre "" 2;dlwget "http://www.litteratureaudio.org/mp3/VERNE_Jules_Aventures_de_la_famille_Raton_fich03.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

Verne_1910_Monsieur_Re_Dieze_et_mademoiselle_Mi_Bemol(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="hier_et_demain";                         tag_title="Monsieur Re-Dieze et mademoiselle Mi-Bemol";
    tag_year=1910;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="René Depasse";                             dl_source="www.litteratureaudio.com";
    dl_duree="1h18";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-monsieur-re-dieze-et-mademoiselle-mi-bemol.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover- #
        dlwget "http://www.litteratureaudio.com/img/xAristide_Cavaille_Coll_-_Orgue_Saint_Sulpice.jpg.pagespeed.ic.FVXiu5rkpb.jpg" "$livreTitreNormalize.jpg"

        # - audios - #
        dlwget "http://www.litteratureaudio.net/mp3/Jules_Verne_-_Monsieur_Re_Diese_et_Mademoiselle_Mi_Bemol2.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}


Verne_1910_La_Destinee_de_Jean_Morenas(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="hier_et_demain";                         tag_title="La Destinée de Jean Morenas";
    tag_year=1910;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="René Depasse";                             dl_source="www.litteratureaudio.com";
    dl_duree="1h30";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"

    livreContent(){
        # - cover- #
        dlwget "http://www.litteratureaudio.com/img/xJules_Verne_-_La_Destinee_de_Jean_Morenas.jpg.pagespeed.ic.y6wOkJ0sfk.jpg" "$livreTitreNormalize.jpg"

        # - audios - #
        local episodeUrl="http://www.litteratureaudio.org/mp3/Verne_-_La_destinee_de_Jean_Morenas_01_Premiere_partie" 
        dlwget "${episodeUrl}.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}


Verne_1910_La_Journee_d_un_journaliste_americain_en_2890(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="hier_et_demain";                         tag_title="La Journée d un journaliste américain en 2890";
    tag_year=1910;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="0h41";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-la-journee-dun-journaliste-americain-en-2890.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover- #

        # - audios - #

        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_La_journee_dun_journaliste_americain_en_2890.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

Verne_1910_L_Eternel_Adam(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="hier_et_demain";                         tag_title="L_Eternel_Adam";
    tag_year=1910;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Bernard";                                 dl_source="www.litteratureaudio.com";
    dl_duree="1h28";
    tag_url=""

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # - cover- #
        dlwget "http://www.litteratureaudio.com/img/xJules_Verne_-_L_Eternel_Adam_2.jpg.pagespeed.ic.0b1X-2e2us.jpg" "$livreTitreNormalize.jpg"

        # - audios - #
        dlwget "http://www.litteratureaudio.org/mp3/Jules_Verne_-_L_Eternel_Adam.mp3" "$livreTitreNormalize.mp3"
    }
    livreContent
    closeLivre
}

hier_et_demain(){
    showDebug "\n$FUNCNAME($*)"
} #hier_et_demain


Verne__Discours_d_inauguration_du_cirque_municipal_d_Amiens(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Discours_d_inauguration_du_cirque_municipal_d_Amiens";                         tag_title="$tag_album";
    tag_year=0;                                         dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="0h25";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-discours-dinauguration-du-cirque-municipal-damiens.html"
    createLivre  "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
        dlwget "dlwget "" "$livreTitreNormalize.jpg"" $livreTitreNormalize-chapitre-1-1.mp3
    closeLivre






Maitre_du_Monde(){
    showDebug "\n$FUNCNAME($*)"
    tag_genreNo=28;                                     tag_genre="Roman";
    tag_album="Maitre du monde";                        tag_title="$tag_album";
    tag_year=1904;                                      dl_date="$tag_year";
    dl_editeur="Hetzel"
	dl_voix="Orangeno";                                 dl_source="www.litteratureaudio.com";
    dl_duree="5h37";
    tag_url="http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-maitre-du-monde.html"

    createLivre "$tag_title" "$dl_date-$tag_title-$tag_artist-$dl_source-$dl_voix-$dl_duree"
    if [ $? -ne 0 ]; then return 1;fi

    livreContent(){
        # https://fr.wikipedia.org/wiki/Ma%C3%AEtre_du_Monde

        # - cover - #
        dlCover "https://upload.wikimedia.org/wikipedia/commons/9/91/%27Master_of_the_World%27_by_George_Roux_21.jpg"

        # - audios - #
        initChapitre 1 18
        local episodeUrl="http://www.litteratureaudio.org/mp3/Jules_Verne_-_Maitre_du_Monde_Chap"
        local -a chapitreTitreTbl;
        chapitreTitreTbl[1]=""
        chapitreTitreTbl[2]=""
        chapitreTitreTbl[3]=""
        chapitreTitreTbl[4]=""
        chapitreTitreTbl[5]=""
        chapitreTitreTbl[6]=""
        chapitreTitreTbl[7]=""
        chapitreTitreTbl[8]=""
        chapitreTitreTbl[9]=""

        chapitreTitreTbl[10]=""
        chapitreTitreTbl[11]=""
        chapitreTitreTbl[12]=""
        chapitreTitreTbl[13]=""
        chapitreTitreTbl[14]=""
        chapitreTitreTbl[15]=""
        chapitreTitreTbl[16]=""
        chapitreTitreTbl[17]=""
        chapitreTitreTbl[18]=""
        for ((chapitreNu=$chapitreNo; chapitreNu<=$chapitreNb ; chapitreNu++))
        do
            addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
            dlwget "${episodeUrl}$clz$chapitreNo.mp3" "$livreTitreNormalizeChapitre.mp3"
        done    

    }
    livreContent
    closeLivre
}





Verne_litteratureaudio_com_Nouvelles(){
    showDebug "\n$FUNCNAME($*)"
    # - nouvelles - #
    createCollection "$collectionRoot"
    createAlbum "Nouvelles"

    # - nouvelle: hier et demain - #
    createAlbum "1910-Hier_et_demain"
    echo "creation par liens"
    local originalDir="$collectionRoot/Chronologie"

    lier "$originalDir/1910-Aventures_de_la_famille_Raton-www.litteratureaudio.com-Domi-1h47" \
        "1-Aventures_de_la_famille_Raton-www.litteratureaudio.com-Domi-1h47"
    
    lier "$originalDir/1910-Monsieur_Ré-Dièze_et_mademoiselle_Mi-Bémol-www.litteratureaudio.com-René_Depasse-1h18" \
        "2-Monsieur_Ré-Dièze_et_mademoiselle_Mi-Bémol-www.litteratureaudio.com-René_Depasse-1h18"

    lier "$originalDir/1910-La_Journée_d_un_journaliste_américain_en_2890-www.litteratureaudio.com-Orangeno-0h41" \
        "3-La_Destinée_de_Jean_Morénas-www.litteratureaudio.com-Renée_Depasse-1h30"
    
    lier "$originalDir/1910-L_Éternel_Adam-www.litteratureaudio.com-Bernard-1h28" \
        "6-L_Éternel_Adam-www.litteratureaudio.com-Bernard-1h28"

    lier "$originalDir/1910-Le_Humbug,Moeurs_Américaines-www.litteratureaudio.com-René_Depasse-1h15" \
        "7-Le_Humbug,Moeurs_Américaines-www.litteratureaudio.com-René_Depasse-1h15"

    closeAlbum #1910-Hier_et_demain

    closeAlbum #Nouvelles
} #Verne_Nouvelle


Verne_litteratureaudio_com_Cycles(){
    showDebug "\n$FUNCNAME($*)"

    # - cycles - #
    createCollection "$collectionRoot"
    createAlbum "Cycles"

    # - Cycle de la lune - #
    createAlbum "Cycle_de_la_Lune"
    local originalDir="$collectionRoot/Chronologie"

    local nom="1865-De_la_Terre_à_la_Lune-www.litteratureaudio.com-Orangeno-6h05"
    lier "$originalDir/$nom" "1-$nom"

    local nom="1870-Autour_de_la_Lune-www.litteratureaudio.com-Orangeno-6h50"
    lier "$originalDir/$nom" "2-$nom"

    local nom="1889-Sans_dessus_dessous-www.litteratureaudio.com-ThéoTeX-5h09"
    lier "$originalDir/$nom" "3-$nom"
    
    closeAlbum #Cycle de la lune

    closeAlbum #Cycles
}


Verne_litteratureaudio_com_Divers(){
    showDebug "\n$FUNCNAME($*)"

    #createCollection "$collectionRoot"
    #createAlbum "Divers"

    #createLivre "Vingt-quatre_minutes_en_ballon_-www.litteratureaudio.com-Orangeno-0h08"
        #dlwget "$livreTitreNormalize.jpg"
        #episodeUrl=""    dlwget "${episodeUrl}.mp3" $livreTitreNormalize-chapitre-1-1.mp3
    #closeLivre


    #closeAlbum

} #Verne_litteratureaudio_com_Cycles
