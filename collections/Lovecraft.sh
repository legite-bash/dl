#!/bin

# - lovecraft - #
echo "$collection"
collectionRoot=""

#SITE_URL="https://www.youtube.com/user/"
VIDEO_INFO_URL="https://www.youtube.com/watch?v=8RTZQZGXKx0"

# - references - #
#https://fr.wikipedia.org/wiki/Liste_d'%C5%93uvres_de_H._P._Lovecraft
#http://www.litteratureaudio.com/livres-audio-gratuits-mp3/tag/howard-phillips-lovecraft
#https://www.youtube.com/channel/UCbs2NP7U3utFvmP2rU1Sluw
#http://www.tierslivre.net/spip/spip.php?article4250


Lovecraft(){
    createCollection "/media/mediatheques/litteratures/livreAudios/Auteurs/"
    if [ $? -ne 0 ]; then return 1;fi

    createAuteur "Lovecraft,Howard_Phillips"
    setYoutubeFormat 140

    #Lovecraft_youtube
    Lovecraft_francois_Bon
    #LeClubLovecraftien
    #Lovecraft_litteratureaudio_com
    closeAuteur
}


Lovecraft_youtube(){
    createCollection "$collectionRoot"
    createAlbum "Chronologie"
    youtubeFormat 140
    #dlyt 8RTZQZGXKx0 1959-Le_Mystere_du_Cimetiere-rene_Depasse

    createLivre "1927-L_affaire_Charles_Dexter_Ward-youtube-HPL"
    dlyt 44Xma-Wkq3s -1-5-1h03m06s
    dlyt gPAtkhFXVow -2-5-1h05m30s
    dlyt T6WbhT1tcxE -3-5-1h04m35s
    dlyt bCUI4e981CU -4-5-1h04m27s
    dlyt CdhG7QIhyWY -5-5-

    createLivre "1926-L_appel_de_Cthulhu-youtube-HPL-1h39m17s"
    dlyt WQu_SVlHB5s $livreNom
    
    createLivre "1926-Je_suis_d_ailleurs-l_intrus-The_Outsider-youtube-HPL-28m58s"
    dlyt iI7-Dp7kBqg $livreNom

    createLivre "1930-L_Ombre_sur_Innsmouth-Shadow_Over_Innsmouth-youtube-HPL-3h31m53s"
    dlyt xcs3t6-qBnE $livreNom

    createLivre "1922.09-Le_Molosse-The_hound-youtube-HPL-23m33s"
    dlyt tgjfic0ipsQ $livreNom

    createLivre "1923.08-Les_Rats_dans_les_Murs-youtube-HPL-1h04m02s"
    dlyt tK-uYuvzXqU $livreNom

    createLivre "1921.01-La_Cite_sans_nom-youtube-HPL-41m28s"
    dlyt BjMOblYywbc $livreNom

    createLivre "Fungi_de_Yuggoth-youtube-HPL-33m40s"
    dlyt W0Swfv8AzZo $livreNom

    createLivre "1919-Souvenir-youtube-HPL-2m40s"
    dlyt diwS7kVQvwM $livreNom

    createLivre "La_Petite_Bouteille_de_Verre-youtube-HPL-3m50s"
    dlyt DUYEBzuGP50 $livreNom

    createLivre "1921.02-La_Quete_d_Iranon-youtube-HPL-18m13s"
    dlyt bZGsTOopadQ $livreNom

    createLivre "1933.08-Le_Monstre_sur_le_Seuil-The_Thing_on_the_Doorstep-youtube-HPL-1h01m09s"
    dlyt jm-xSst1--k $livreNom

    createLivre "1932.02-La_Maison_de_la_Sorciere-youtube-HPL-1h34m08s"
    dlyt xD0jlCg910Y $livreNom

    createLivre "1931-Les_montagnes_Hallucinees-youtube-HPL-3h45m24s"
    dlyt Y9II56EueG0 $livreNom

    createLivre "1920.11-Celephaïs-youtube-HPL-16m15s"
    dlyt WIPRhf7v9Pc $livreNom

    createLivre "1918-Polaris-youtube-HPL-11m32s"
    dlyt 9R3bMYK6WmI $livreNom

    createLivre "1920-La_malediction_de_Sarnath-youtube-HPL-17m02s"
    dlyt QKoF_HL69dM $livreNom

    createLivre "1920.06-Les_Chats_d_Ulthar-youtube-HPL-8m27s-17m28s"
    dlyt ZtSyTTerDns $livreNom

    createLivre "1919-Le_Bateau_Blanc-youtube-HPL-"
    dlyt h2p_r4YodBc $livreNom

    createLivre "1917.07-Dagon-youtube-HPL-16m31s"
    dlyt XvZENzWcBq4 $livreNom
    
    
    createLivre "1919-Demons_et_Merveilles-1_Le_Temoignage_de_Randolph_Carter-The_Statement_of_Randolph_Carter-21m01s"
    dlyt Xh6UEyGgFCk $livreNom

    createLivre "1926-Demons_et_Merveilles-2-La_Cle_d_Argent-The_Silver_Key-41h56"
    dlyt jzVmWK6ZvEY $livreNom

    createLivre "1932.10-Demons_et_Merveilles-3-A_Travers_Les_Portes_De_La_Cle_d_Argent-2h01m31s"
    dlyt g6SbKGxYRPA $livreNom

    createLivre "1926-Demons_et_Merveilles-4-A_la_Recherche_de_Kadath-partie-1-2h48m28s"
    dlyt aCbqO_uZxNI $livreNom

    createLivre "1926-Demons_et_Merveilles-5-A_la_Recherche_de_Kadath-partie-2-2h49m08s"
    dlyt qBtO3Pnj32o $livreNom


    createLivre "1922.06-Herbert_West_Reanimateur-youtube-HPL-1h23m15s"
    dlyt xgay0KHqbTM  $livreNom

    createLivre "1920-De_l_Au_Dela-youtube-HPL-22m04s"
    dlyt 0jg3liYIgEQ $livreNom

    createLivre "1920-Le_Temple-The_Temple-youtube-HPL-35h54s"
    dlyt WKx9QpFhyDE $livreNom

    createLivre "1920-L_Arbre-The_Tree-youtube-HPL-11m31s"
    dlyt heUMapfdOmk $livreNom

    createLivre "1921.08-Les_Autres_Dieux-The_Other_Gods-youtube-HPL-13m26s"
    dlyt R-AXD7s3rT4 $livreNom

    createLivre "1908-L_Alchimiste-The_Alchemist-youtube-HPL-23m11s"
    dlyt YTEYM5c17GU $livreNom

    createLivre "1920-La_Poesie_et_les_Dieux-Poetry_and_the_Gods-youtube-HPL-17m41s"
    dlyt VuD2BFVXgiA $livreNom

    createLivre "1919-La_Rue-The_Street-youtube-HPL-15m26s"
    dlyt Ir6M3pQwCp0 $livreNom

    createLivre "1925.08-Horreur_a_Red_Hook-The_Horror_at_Red_Hook-youtube-HPL-1h00m21s"
    dlyt ZoEqBZCq6Fo $livreNom

    createLivre "1919.09-La_Transition_de_Juan_Romero-The_Transition_of_Juan_Romero-youtube-HPL-17m06s"
    dlyt burKDKIxHCQ $livreNom

    createLivre "1927.11-La_Chose_Dans_La_Clarte_Lunaire-The_Thing_in_the_Moonlight-youtube-HPL-5m31s"
    dlyt zKj-Fkf1IZI $livreNom

    createLivre "1933-Le_Livre-The_Book-youtube-HPL-8m16s"
    dlyt ElMV_8CIuBI $livreNom

    createLivre "1927-Le_Descendant-The_Descendant-youtube-HPL-10m01s"
    dlyt rvRR9ntwoUE $livreNom

    createLivre "1922.03-Hypnos-Hypnos-youtube-HPL-20m47s"
    dlyt 5DEW_PLubnU $livreNom

    createLivre "1923.10-Le_Festival-The_Festival-youtube-HPL-24m46s"
    dlyt myfjalad5u8 $livreNom

    createLivre "1924.02-Prisonnier_des_Pharaons-Imprisoned_with_the_Pharaohs-Under_the_Pyramids-youtube-HPL-1h44"
    dlyt 853JSbAak44 $livreNom

    createLivre "1925.08-Lui-He-youtube-HPL-29m46s"
    dlyt F5vmIlAPNhs $livreNom

    createLivre "1926.11-L_etrange_Maison_Haute_dans_la_Brume-The_Strange_High_House_in_The_Mist-youtube-HPL-29m30s"
    dlyt M6rum1paYPY $livreNom

    createLivre "1936.01-Dans_les_Murs_d_Eryx-In_the_Walls_of_Eryx-youtube-HPL-1h20m09s"
    dlyt c4M6kHhgpGk $livreNom

    createLivre "1933-Le_Clergyman_Maudit-The_Evil_Clergyman-youtube-HPL-9m46s"
    dlyt z3M3zAfrzvo $livreNom

    createLivre "1904-1905-La_Bete_de_la_Caverne-The_Beast_in_the_Cave-youtube-HPL-14m42s"
    dlyt fwKrBjpohv0 $livreNom

    createLivre "1917.06-La_Tombe-The_Tomb-youtube-HPL-27m36s"
    dlyt gnKX3wx6tHA $livreNom

    createLivre "1928-L_Abomination de Dunwich-The_Dunwich_Horror-youtube-HPL-2h01m55s"
    dlyt OtMqQYvRpek $livreNom

    createLivre "1934-1935-Dans_l_abîme_du_Temps-The_Shadow_Out_of_Time-youtube-HPL-2h54m35s"
    dlyt fas0pfoANg0 $livreNom

    createLivre "1930.02-Celui_qui_Chuchotait_dans_les_Tenebres-The_Whisperer_in_Darkness-youtube-HPL-2h58m32s"
    dlyt rxSqEIkPSpY $livreNom

    createLivre "1927.03-La_Couleur_Tombee_du_Ciel-The_Colour_out_of_Space-youtube-HPL-1h25m20s"
    dlyt KlS9rMZzWbc $livreNom

    createLivre "1936.07-Night_Ocean-The_Night_Ocean-youtube-HPL-1h07m41s"
    dlyt 0DPHg6o8Hpw $livreNom

    createLivre "1920-Nyarlathotep-Nyarlathotep-youtube-HPL-8m40s"
    dlyt 0aPVqIOhz54 $livreNom

    createLivre "1920-Ex_Oblivione-Ex_Oblivione-youtube-HPL-4m50"
    dlyt FSy1KvuGXLc $livreNom

    createLivre "1922.06-Ce_qu_apporte_la_Lune-What_The_Moon_Brings-youtube-HPL-5m45s"
    dlyt XfEgSrpp_2M $livreNom

    createLivre "1927-Histoire_du_Necronomicon-History_of_the_Necronomicon-youtube-HPL-5m46s"
    dlyt ysze0TrdY5w $livreNom

    createLivre "1927-Ibid-Ibid-youtube-HPL-14m26s"
    dlyt rpSJsBscf2E $livreNom

    createLivre "Bataille_a_la_fin_du_siecle-youtube-HPL-9m26s"
    dlyt 7i49Sd7c39E $livreNom

    createLivre "1935-Cosmos_Effondres-Collapsing_Cosmoses-youtube-HPL-5m06s"
    dlyt mtpwZEq6hco $livreNom

    createLivre "1931-Le_Piege-The_Trap-youtube-HPL-56m52s"
    dlyt GpJUBBQfAhs $livreNom

    createLivre "1935-Le_defi_d_outre_espace-The_Challenge_from_Beyond-youtube-HPL-42m41s"
    dlyt KHHX4Yugb7Q $livreNom

    createLivre "1917-Quelques_souvenirs_sur_le_docteur_Johnson-A_Reminiscence_of_Dr_Samuel_Johnson-youtube-HPL-15m56s"
    dlyt nesjo7gy8Gk $livreNom

    createLivre "1927.11-Le_peuple_Ancien-The_Very_Old_Folk-youtube-HPL-20m51s"
    dlyt migZmhKjgsA $livreNom

    createLivre "1919-Douce_Ermengarde-Sweet_Ermengarde-youtube-HPL-20m56s"
    dlyt -wOM3uqmka0  $livreNom

    createLivre "1934-Les_Sortileges_d_Aphlar-The_Sorcery_of_Aphlar-youtube-HPL-7m16s"
    dlyt jfS5yUd3uAY  $livreNom

    createLivre "1927-L_affaire_Charles_Dexter_Ward-The_Case_of_Charles_Dexter_Ward-youtube-HPL-4h48m15s"
    dlyt b03t5UEjm3M  $livreNom

    createLivre "1919-Par_Dela_Le_Mur_Du_Sommeil-Beyond_the_Wall_of_Sleep-youtube-HPL-32m12s"
    dlyt EPxWzbsO7XQ  $livreNom

    createLivre "1935.11-Celui_qui_hantait_les_Tenebres-The_Haunter_of_the_Dark-youtube-HPL-56m00s"
    dlyt iWRACJASMlI  $livreNom

    createLivre "1921.12-La_Musique_d_Erich_Zann-The_Music_of_Erich_Zann-youtube-HPL-30m03s"
    dlyt -Weeyrz_17k  $livreNom
   
    createLivre "1923.09-L_indicible-L_Innomable-The_Unnamable-youtube-HPL-29m59s"
    dlyt YtoumX_UqGQ $livreNom

    createLivre "1926.03Air_Froid-Cool_Air-youtube-HPL-31m00s"
    dlyt UTVXAF4DfHQ $livreNom

    createLivre "1921.03-La_Tourbiere_hantee-The_Moon-Bog-youtube-HPL-27m05s"
    dlyt dvwwmvq4ono $livreNom

    createLivre "Arthur_Jermyn-youtube-HPL-32m00s"
    dlyt 6tVmfItwa9U $livreNom

    createLivre "Le_Modèle_de_Pickman-youtube-HPL-45m41s"
    dlyt d96ep6gwoUM $livreNom

    createLivre "La_Peur_qui_Rôde-youtube-HPL-1h08m19s"
    dlyt Clw94BHIHu0 $livreNom

    createLivre "Les_montagnes_hallucinées-Chapitre-1-youtube-HPL-Plisken_le_Herisson-Philippe_Bertin-27m08s"
    dlyt YWW6_I2e6Q4 $livreNom

    createLivre "L_Appel_de_Cthlhu-youtube-HPL-Lyre_audio-1h36m22s"
    dlyt W4x03IQiXpI $livreNom

    closeAlbum
    closeCollection
}


Lovecraft_francois_Bon(){
    createCollection "$collectionRoot"
    createAlbum "Chronologie"
    youtubeFormat 140
	local voix="Francois_Bon";
    local source="www.tierslivre.net"

    # - livre - #
    tag_album="l'intrus-The Outsider"
    tag_title="$tag_album"
    tag_year="1921"
    #tag_note="${tag_prenote}Lu par $voix. $source"
    local fileName="$tag_year-$tag_title-$tag_artist-$source-0h21"
    createLivre "$tag_title" "$fileName"
    dlyt "6uCu3UWZ-uQ" "$fileName"
    closeLivre

    createLivre "1923.09-L_indicible-L_Innomable-The_Unamable-youtube-Francois_Bon-27min"
    dlyt "b_N6aWmEV2Q" "$livreNom"
    closeLivre

    createLivre "1922-Le_Chien-youtube-Francois_Bon-27min"
    dlyt "h_-swErynYk"  "$livreNom"
    closeLivre

    createLivre "1919-Dagon-youtube-Francois_Bon-min"
    dlyt "yow3Es3BrB4" "$livreNom"
    closeLivre

    closeAlbum
    closeCollection
}


Lovecraft_litteratureaudio_com(){
    createAlbum "Chronologie"
    formatExt="mp3";
    # http://www.litteratureaudio.com/livres-audio-gratuits-mp3/tag/howard-phillips-lovecraft

    createLivre "1920-Le_Temple-The_Temple-www.litteratureaudio.com-Cyprien-40min"
    dlwget http://www.litteratureaudio.com/img/xInterieur_de_sous-marin_wikimedia_commons.jpg.pagespeed.ic.1u2o0R_Mtq.jpg $livreNom.jpg
    dlwget http://www.litteratureaudio.org/mp3/Howard_Phillips_Lovecraft_-_Le_Temple.mp3                           $livreNom.$formatExt

    createLivre "1921.01-La_Cite_sans_nom-www.litteratureaudio.com-Vincent_de_l_Epine-41min"
    dlwget http://www.litteratureaudio.com/img/xLa_Cite_Sans_Nom.jpg.pagespeed.ic.AchYCBQlg2.jpg   $livreNom.jpg
    dlwget http://www.litteratureaudio.org/mp3/Howard_Philips_Lovecraft_-_La_Cite_Sans_Nom.mp3     $livreNom.$formatExt

        
    createLivre "1919-Le_Temoignage_de_Randolph_Carter-The_Statement_of_Randolph_Carter-www.litteratureaudio.com-Vincent_de_l_Epine-22min"
    dlwget http://www.litteratureaudio.com/img/xLe_Temoignage_De_Randolph_Carter.jpg.pagespeed.ic.mLD6T1tZtX.jpg   $livreNom.jpg
    dlwget http://www.litteratureaudio.org/mp3/Howard_Philips_Lovecraft_-_Le_Temoignage_De_Randolph_Carter.mp3     $livreNom.$formatExt

    createLivre "1926-Je_suis_d_ailleurs-l_intrus-The_Outsider-www.litteratureaudio.com-Vincent_de_l_Epine-23min"
    dlwget http://www.litteratureaudio.com/img/xL_intrus2.jpg.pagespeed.ic.ZIrLV7nKRS.jpg $livreNom.jpg
    dlwget http://www.litteratureaudio.org/mp3/Howard_Philips_Lovecraft_-_L_intrus.mp3                             $livreNom.$formatExt


    setPrefixe "1930-L_Ombre_sur_Innsmouth-Shadow_Over_Innsmouth-www.litteratureaudio.com-Jean-Luc_Fischer-2h30"
    dlwget http://www.litteratureaudio.com/img/xH_P_Lovecraft_-_L_Ombre_sur_Innsmouth.jpg.pagespeed.ic.GLU9kSaooG.jpg $livreNom.jpg
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_-_L_Ombre_sur_Innsmouth_Ch_01.mp3                     $livreNom-chapitre-1-5.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_-_L_Ombre_sur_Innsmouth_Ch_02.mp3                     $livreNom-chapitre-2-5.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_-_L_Ombre_sur_Innsmouth_Ch_03.mp3                     $livreNom-chapitre-3-5.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_-_L_Ombre_sur_Innsmouth_Ch_04.mp3                     $livreNom-chapitre-4-5.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_-_L_Ombre_sur_Innsmouth_Ch_05.mp3                     $livreNom-chapitre-5-5.$formatExt


    createLivre "1926-L_appel_de_Cthulhu-www.litteratureaudio.com-Jean-Luc_Fischer-1h40"
    dlwget http://www.litteratureaudio.com/img/xH_P_Lovecraft_-_L_Appel_de_Cthulhu.jpg.pagespeed.ic.t9pD87Zp1S.jpg $livreNom.jpg
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_-_L_Appel_de_Cthulhu_Ch_01.mp3 $livreNom-chapitre-1-3.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_-_L_Appel_de_Cthulhu_Ch_02.mp3 $livreNom-chapitre-2-3.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_-_L_Appel_de_Cthulhu_Ch_03.mp3 $livreNom-chapitre-3-3.$formatExt

    createLivre "1923.08-Les_Rats_dans_les_Murs-www.litteratureaudio.com-TLT-1h04"
    dlwget http://www.litteratureaudio.com/img/xH_P_Lovecraft_L_Affaire_CDW.jpg.pagespeed.ic.G7UcLM00Em.jpg        $livreNom.jpg
    dlwget http://www.litteratureaudio.org/mp3/Howard_Philips_Lovecraft_-_Les_rats_dans_les_murs.mp3               $livreNom.$formatExt

    createLivre "1933.08-La_Chose_sur_le_seuil-The_Thing_on_the_Doorstep-www.litteratureaudio.com-Vincent_de_l_Epine-1h12m"
    dlwget http://www.litteratureaudio.com/img/xLa_Chose_sur_le_Seuil.jpg.pagespeed.ic.L2QCKiEGYm.jpg              $livreNom.jpg
    dlwget http://www.litteratureaudio.org/mp3/Howard_Philips_Lovecraft_-_La_Chose_sur_le_seuil_Chap1.mp3          $livreNom-chapitre-1-7.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/Howard_Philips_Lovecraft_-_La_Chose_sur_le_seuil_Chap2.mp3          $livreNom-chapitre-2-7.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/Howard_Philips_Lovecraft_-_La_Chose_sur_le_seuil_Chap3.mp3          $livreNom-chapitre-3-7.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/Howard_Philips_Lovecraft_-_La_Chose_sur_le_seuil_Chap4.mp3          $livreNom-chapitre-4-7.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/Howard_Philips_Lovecraft_-_La_Chose_sur_le_seuil_Chap5.mp3          $livreNom-chapitre-5-7.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/Howard_Philips_Lovecraft_-_La_Chose_sur_le_seuil_Chap6.mp3          $livreNom-chapitre-6-7.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/Howard_Philips_Lovecraft_-_La_Chose_sur_le_seuil_Chap7.mp3          $livreNom-chapitre-7-7.$formatExt

    createLivre "Les_Champignons_De_Yugoth-www.litteratureaudio.com-Aymeric-39min"
    dlwget http://www.litteratureaudio.com/img/xHoward_Phillips_Lovecraft_-_Les_champignons_de_Yugoth.jpg.pagespeed.ic.yo0bxaQ1ry.jpg $livreNom.jpg
    dlwget http://www.litteratureaudio.org/mp3/Howard_Phillips_Lovecraft_-_Les_champignons_de_Yugoth.mp3           $livreNom.$formatExt

    createLivre "1922.09-Le_Molosse-The_hound-www.litteratureaudio.com-Vincent_de_l_Epine-24min"
    dlwget http://www.litteratureaudio.com/img/xLe_Molosse.jpg.pagespeed.ic.m4Inp8gvZX.jpg $livreNom.jpg
    dlwget http://www.litteratureaudio.org/mp3/Howard_Phillips_Lovecraft_-_Le_Molosse.mp3                           $livreNom.$formatExt

    createLivre "1896-La_Petite_Bouteille_de_Verre-The_Little_Glass_Bottle-www.litteratureaudio.com-Rene_Depasse-4min"
    dlwget http://www.litteratureaudio.com/livre-audio-gratuit-mp3/lovecraft-howard-phillips-la-petite-bouteille-de-verre.html $livreNom.jpg
    dlwget http://www.litteratureaudio.org/mp3/Lovecraft_-_La_pertite_bouteille_de_verre.mp3                       $livreNom.$formatExt

    createLivre "1927-L_affaire_Charles_Dexter_Ward-The_Case_of_Charles_Dexter_Ward-www.litteratureaudio.com-Jean-Luc_Fischer-5h30"
    #dlwget  $livreNom.jpg
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_I_1.mp3    $livreNom-chapitre-1-1.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_I_2.mp3    $livreNom-chapitre-1-2.$formatExt

    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_II_1.mp3   $livreNom-chapitre-2-1.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_II_2.mp3   $livreNom-chapitre-2-2.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_II_3.mp3   $livreNom-chapitre-2-3.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_II_4.mp3   $livreNom-chapitre-2-4.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_II_5.mp3   $livreNom-chapitre-2-5.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_II_6.mp3   $livreNom-chapitre-2-6.$formatExt

    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_III_1.mp3  $livreNom-chapitre-3-1.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_III_2.mp3  $livreNom-chapitre-3-2.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_III_3.mp3  $livreNom-chapitre-3-3.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_III_4.mp3  $livreNom-chapitre-3-4.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_III_5.mp3  $livreNom-chapitre-3-5.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_III_6.mp3  $livreNom-chapitre-3-6.$formatExt

    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_IV_1.mp3   $livreNom-chapitre-4-1.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_IV_2.mp3   $livreNom-chapitre-4-2.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_IV_3.mp3   $livreNom-chapitre-4-3.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_IV_4.mp3   $livreNom-chapitre-4-4.$formatExt

    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_V_1.mp3    $livreNom-chapitre-5-1.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_V_2.mp3    $livreNom-chapitre-5-2.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_V_3.mp3    $livreNom-chapitre-5-3.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_V_4.mp3    $livreNom-chapitre-5-4.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_V_5.mp3    $livreNom-chapitre-5-5.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_V_6.mp3    $livreNom-chapitre-5-6.$formatExt
    dlwget http://www.litteratureaudio.org/mp3/H_P_Lovecraft_L_Affaire_CDW_Ch_V_7.mp3    $livreNom-chapitre-5-7.$formatExt
    
    createLivre "1898-Le_Mystère_du_cimetière-La_Revanche_d_un_mort-The_Mystery_of_the_Grave-Yard-www.litteratureaudio.com-René_Depasse-12min"
    #dlwget  $livreNom.jpg
    dlwget http://www.litteratureaudio.org/mp3/Lovecraft_-_Le_mystere_du_cimetiere.mp3   $livreNom.$formatExt

    createLivre "mémoire-www.litteratureaudio.com-Martine_Leroy_Rambaud-7min"
    #dlwget  $livreNom.jpg
    dlwget http://www.litteratureaudio.org/mp3/Lovecraft_H_P_-_Memoire.mp3               $livreNom.$formatExt

    closeAlbum
    closeCollection
}

LeClubLovecraftien(){
    createCollection $collectionRoot
    createAlbum "Chronologie"
    #echo $PWD
    youtubeFormat 140

    #https://www.youtube.com/playlist?list=PLXNVVRi2Vb1DEfTvlWoTEfQIJavN4haGn


    createLivre "1939-Le_Masque_De_Chtulhu-1-Le_Retour_d-Hastur-The_Return_of_Hastur-youtube-HPL-Georges_Béjean-1h21m00s"
    dlyt j8J3uPOpJow $livreNom

    createLivre "1948-Le_Masque_De_Chtulhu-2-Les_Engoulevents_de_la_colline-The_Whippoorwills_in_the_hills-youtube-HPL-Georges_Béjean-Christiane_Deis-1h34m51s"
    dlyt Y_57p4IxuK4 $livreNom

    createLivre "1947-Le_Masque_De_Chtulhu-3-Quelque_chose_en_bois-Something_in_wood-youtube-HPL-Georges_Béjean-Christiane_Deis-39m39s"
    dlyt wUXz7J_MgT0 $livreNom

    createLivre "1940-Le_Masque_De_Chtulhu-4-Le_Pacte_des_Sandwin-The_Sandwin_compact-youtube-HPL--1h04m26s"
    dlyt BfyhzEkHoU4 $livreNom

    createLivre "1953-Le_Masque_De_Chtulhu-5-La_Maison_dans_la_Vallée-The_House_in_the_valley-youtube-HPL-Georges_Béjean-Christiane_Deis-1h15m36s"
    dlyt CTAQ4Be2dOI $livreNom

    createLivre "1957-Le_Masque_De_Chtulhu-6-Le_Sceau_de_R_Lyeh-The_seal_of_Rlyeh-youtube-HPL-Georges_Béjean-Christiane_Deis-1h19m36s"
    dlyt HIAI_2qLYTk $livreNom

    closeAlbum

    # - creation d'un repertoire specifique - #
    echo "Creation du repertoire Le_Masque_De_Chtulhu et ajout des liens"
    createAlbum "Le_Masque_De_Chtulhu"

    titre="1939-Le_Masque_De_Chtulhu-1-Le_Retour_d-Hastur-The_Return_of_Hastur-youtube-HPL-Georges_Béjean-1h21m00s"
    link  ../$titre.$formatExt ./$titre.$formatExt

    titre="1948-Le_Masque_De_Chtulhu-2-Les_Engoulevents_de_la_colline-The_Whippoorwills_in_the_hills-youtube-HPL-Georges_Béjean-Christiane_Deis-1h34m51s.$formatExt"
    link ../$titre.$formatExt ./$titre.$formatExt

    titre="1947-Le_Masque_De_Chtulhu-3-Quelque_chose_en_bois-Something_in_wood-youtube-HPL-Georges_Béjean-Christiane_Deis-39m39s"
    link ../$titre.$formatExt ./$titre.$formatExt

    titre="1940-Le_Masque_De_Chtulhu-4-Le_Pacte_des_Sandwin-The_Sandwin_compact-youtube-HPL--1h04m26s"
    link ../$titre.$formatExt ./$titre.$formatExt

    titre="1953-Le_Masque_De_Chtulhu-5-La_Maison_dans_la_Vallée-The_House_in_the_valley-youtube-HPL-Georges_Béjean-Christiane_Deis-1h15m36s"
    link ../$titre.$formatExt ./$titre.$formatExt

    titre="1957-Le_Masque_De_Chtulhu-6-Le_Sceau_de_R_Lyeh-The_seal_of_Rlyeh-youtube-HPL-Georges_Béjean-Christiane_Deis-1h19m36s"
    link ../$titre.$formatExt ./$titre.$formatExt

    closeAlbum
    closeCollection
}









